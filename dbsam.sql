-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 15, 2019 at 12:56 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbsam`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbappdefaults`
--

DROP TABLE IF EXISTS `tbappdefaults`;
CREATE TABLE IF NOT EXISTS `tbappdefaults` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_name` char(255) NOT NULL,
  `club_name` char(255) NOT NULL,
  `school_logo` char(255) DEFAULT NULL,
  `club_logo` char(255) DEFAULT NULL,
  `no_of_semesters` int(11) NOT NULL,
  `school_year` year(4) NOT NULL,
  `semester` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbappdefaults`
--

INSERT INTO `tbappdefaults` (`id`, `school_name`, `club_name`, `school_logo`, `club_logo`, `no_of_semesters`, `school_year`, `semester`) VALUES
(1, 'Notre Dame of Dadiangas University', 'ICPEP.se', 'NDDUseal.png', 'icpep.png', 2, 2019, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbattendance`
--

DROP TABLE IF EXISTS `tbattendance`;
CREATE TABLE IF NOT EXISTS `tbattendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `time_in` time NOT NULL,
  `time_out` time DEFAULT NULL,
  `remarks` char(200) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbevents`
--

DROP TABLE IF EXISTS `tbevents`;
CREATE TABLE IF NOT EXISTS `tbevents` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(250) NOT NULL,
  `venue` char(250) NOT NULL,
  `date` date NOT NULL,
  `time` time DEFAULT NULL,
  `grace_period` int(11) NOT NULL DEFAULT '15',
  `log_out_time` time DEFAULT NULL,
  `type` char(20) NOT NULL DEFAULT '0',
  `school_year` year(4) NOT NULL,
  `semester` int(2) NOT NULL,
  `enrolled_students` int(11) NOT NULL,
  `status` char(15) NOT NULL DEFAULT 'Unbegun',
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbevent_exemptions`
--

DROP TABLE IF EXISTS `tbevent_exemptions`;
CREATE TABLE IF NOT EXISTS `tbevent_exemptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `remarks` char(255) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tbevent_exemptions_ibfk_2` (`student_id`),
  KEY `tbevent_exemptions_ibfk_1` (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbfingerprints`
--

DROP TABLE IF EXISTS `tbfingerprints`;
CREATE TABLE IF NOT EXISTS `tbfingerprints` (
  `student_id` int(11) NOT NULL,
  `fingerprint` varchar(1000) NOT NULL,
  `position` int(11) NOT NULL,
  `fppath` char(255) NOT NULL,
  KEY `tbfingerprints_ibfk_1` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbfingerprints`
--

INSERT INTO `tbfingerprints` (`student_id`, `fingerprint`, `position`, `fppath`) VALUES (11120022, '<?xml version=\"1.0\" encoding=\"UTF-8\"?><Fid><Bytes>Rk1SACAyMAAA/gAz/v8AAAD8AUQAxQDFAQAAAFYlQHIAfQtegDAA8xZdgJIBJpRcQEcASxJZQLEA/p5YQGIBGnZYgJgA56VYQMUATq9WQBUBC3RWgLMBIJdWgE0Asm5VgGEA2xFVgG0AIhNUQJ4APwpUQGUAVA1UgFUBMhxUgN0APK5TQE0AWG5TgDcAvA9SQJcApgBRQEEAeBRQgHoA6ABQgIQAIQ1OgHYAjgtNQG0A5QhNgLEAw6ZMgCEBG3lMgO0AUVJLgIwAwwRKQIQA+qJKQNsBGZtIQKYAvV9HQJgATmU3QGEBOhI3AHIBNXIwAHgBKZUrAH0BPJApAAA=</Bytes><Format>1769473</Format><Version>1.0.0</Version></Fid>', 5, 'C:\\Student Attendance Manager\\Students\\11120022\\Fingerprints\\Left Thumb');

-- --------------------------------------------------------

--
-- Table structure for table `tbstudents`
--

DROP TABLE IF EXISTS `tbstudents`;
CREATE TABLE IF NOT EXISTS `tbstudents` (
  `student_id` int(15) NOT NULL,
  `firstname` char(50) NOT NULL,
  `middlename` char(50) NOT NULL,
  `lastname` char(50) NOT NULL,
  `sex` char(7) NOT NULL DEFAULT 'Male',
  `birthdate` date NOT NULL,
  `year_level` int(1) NOT NULL DEFAULT '1',
  `status` int(1) NOT NULL DEFAULT '1',
  `image` char(255) DEFAULT NULL,
  `remarks` char(255) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbstudents`
--

INSERT INTO `tbstudents` (`student_id`, `firstname`, `middlename`, `lastname`, `sex`, `birthdate`, `year_level`, `status`, `image`, `remarks`) VALUES (11120022, 'Steven Rufe', 'Cabreros', 'Padolina', 'Male', '1992-04-05', 9999999, 2, 'C:\\Student Attendance Manager\\Students\\11120022\\Photo\\Photo-11120022.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbusers`
--

DROP TABLE IF EXISTS `tbusers`;
CREATE TABLE IF NOT EXISTS `tbusers` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(20) NOT NULL,
  `password` char(255) NOT NULL DEFAULT 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855',
  `type` char(10) NOT NULL,
  `studentid` int(11) DEFAULT NULL,
  `Name` char(255) DEFAULT NULL,
  `Photo` char(255) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  KEY `studentid` (`studentid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbusers`
--

INSERT INTO `tbusers` (`user_id`, `username`, `password`, `type`, `studentid`, `Name`, `Photo`, `date_created`, `created_by`) VALUES
(1, 'stevemeister05', '6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118090', 'Creator', 11120022, NULL, NULL, '2019-10-30', NULL),
(2, 'user', 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 'Regular', NULL, 'Test User', NULL, '2019-10-30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_officers`
--

DROP TABLE IF EXISTS `tb_officers`;
CREATE TABLE IF NOT EXISTS `tb_officers` (
  `student_id` int(11) NOT NULL,
  `position` char(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `deleted` int(11) NOT NULL,
  KEY `tb_officers_ibfk_1` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_attendance`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_attendance`;
CREATE TABLE IF NOT EXISTS `view_attendance` (
`Attendance ID` int(11)
,`ID Number` int(15)
,`Name` varchar(101)
,`time_in` time
,`time_out` time
,`Event ID` int(11)
,`deleted` int(11)
,`Remarks` varchar(17)
,`Notes` char(200)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_attendance1`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_attendance1`;
CREATE TABLE IF NOT EXISTS `view_attendance1` (
`Attendance ID` int(11)
,`ID Number` int(15)
,`Name` varchar(101)
,`time_in` time
,`time_out` time
,`Event ID` int(11)
,`deleted` int(11)
,`Remarks` varchar(17)
,`Notes` char(200)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_events`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_events`;
CREATE TABLE IF NOT EXISTS `view_events` (
`event_id` int(11)
,`Event Name` char(250)
,`venue` char(250)
,`time` time
,`date` date
,`School Year` year(4)
,`semester` int(2)
,`status` char(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_fingerprints`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_fingerprints`;
CREATE TABLE IF NOT EXISTS `view_fingerprints` (
`student_id` int(15)
,`fingerprint` varchar(1000)
,`position` int(11)
,`fppath` char(255)
,`status` int(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_students`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_students`;
CREATE TABLE IF NOT EXISTS `view_students` (
`ID Number` int(15)
,`Name` varchar(101)
,`Year Level` int(1)
,`Sex` char(7)
,`Birthdate` date
,`Age` int(6)
,`Status` varchar(9)
,`Remarks` char(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_userphoto`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_userphoto`;
CREATE TABLE IF NOT EXISTS `view_userphoto` (
`user_id` int(11)
,`Photo` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_users`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_users`;
CREATE TABLE IF NOT EXISTS `view_users` (
`User ID` int(11)
,`username` char(20)
,`Name` varchar(255)
,`type` char(10)
,`Date Created` date
,`Created by` char(20)
);

-- --------------------------------------------------------

--
-- Structure for view `view_attendance`
--
DROP TABLE IF EXISTS `view_attendance`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_attendance`  AS  select `tbattendance`.`id` AS `Attendance ID`,`tbstudents`.`student_id` AS `ID Number`,concat(`tbstudents`.`firstname`,' ',`tbstudents`.`lastname`) AS `Name`,`tbattendance`.`time_in` AS `time_in`,`tbattendance`.`time_out` AS `time_out`,`tbattendance`.`event_id` AS `Event ID`,`tbattendance`.`deleted` AS `deleted`,if((`tbattendance`.`time_in` > addtime(`tbevents`.`time`,sec_to_time((`tbevents`.`grace_period` * 60)))),if((`tbattendance`.`time_out` < `tbevents`.`log_out_time`),'Late, Early Out',if(isnull(`tbattendance`.`time_out`),if((`tbevents`.`status` = 'Closed'),'Late, No Time Out','On-going'),'Late')),if((`tbattendance`.`time_out` < `tbevents`.`log_out_time`),'Early out',if(isnull(`tbattendance`.`time_out`),if((`tbevents`.`status` = 'Closed'),'No Time Out','On-going'),'Ok'))) AS `Remarks`,`tbattendance`.`remarks` AS `Notes` from ((`tbstudents` join `tbattendance` on((`tbstudents`.`student_id` = `tbattendance`.`student_id`))) join `tbevents` on((`tbattendance`.`event_id` = `tbevents`.`event_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_attendance1`
--
DROP TABLE IF EXISTS `view_attendance1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_attendance1`  AS  select `tbattendance`.`id` AS `Attendance ID`,`tbstudents`.`student_id` AS `ID Number`,concat(`tbstudents`.`firstname`,' ',`tbstudents`.`lastname`) AS `Name`,`tbattendance`.`time_in` AS `time_in`,`tbattendance`.`time_out` AS `time_out`,`tbattendance`.`event_id` AS `Event ID`,`tbattendance`.`deleted` AS `deleted`,if((`tbattendance`.`time_in` > addtime(`tbevents`.`time`,sec_to_time((`tbevents`.`grace_period` * 60)))),if((`tbattendance`.`time_out` < `tbevents`.`log_out_time`),'Late, Early Out',if(isnull(`tbattendance`.`time_out`),'Late, No Time Out','Late')),if((`tbattendance`.`time_out` < `tbevents`.`log_out_time`),'Early out',if(isnull(`tbattendance`.`time_out`),'No Time Out','Ok'))) AS `Remarks`,`tbattendance`.`remarks` AS `Notes` from ((`tbstudents` join `tbattendance` on((`tbstudents`.`student_id` = `tbattendance`.`student_id`))) join `tbevents` on((`tbattendance`.`event_id` = `tbevents`.`event_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_events`
--
DROP TABLE IF EXISTS `view_events`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_events`  AS  select `tbevents`.`event_id` AS `event_id`,`tbevents`.`title` AS `Event Name`,`tbevents`.`venue` AS `venue`,`tbevents`.`time` AS `time`,`tbevents`.`date` AS `date`,`tbevents`.`school_year` AS `School Year`,`tbevents`.`semester` AS `semester`,`tbevents`.`status` AS `status` from `tbevents` ;

-- --------------------------------------------------------

--
-- Structure for view `view_fingerprints`
--
DROP TABLE IF EXISTS `view_fingerprints`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_fingerprints`  AS  select `tbstudents`.`student_id` AS `student_id`,`tbfingerprints`.`fingerprint` AS `fingerprint`,`tbfingerprints`.`position` AS `position`,`tbfingerprints`.`fppath` AS `fppath`,`tbstudents`.`status` AS `status` from (`tbstudents` join `tbfingerprints` on((`tbstudents`.`student_id` = `tbfingerprints`.`student_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_students`
--
DROP TABLE IF EXISTS `view_students`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_students`  AS  select `tbstudents`.`student_id` AS `ID Number`,concat(`tbstudents`.`firstname`,' ',`tbstudents`.`lastname`) AS `Name`,`tbstudents`.`year_level` AS `Year Level`,`tbstudents`.`sex` AS `Sex`,`tbstudents`.`birthdate` AS `Birthdate`,((year(curdate()) - year(`tbstudents`.`birthdate`)) - (right(curdate(),5) < right(`tbstudents`.`birthdate`,5))) AS `Age`,(case `tbstudents`.`status` when 1 then 'Active' when 0 then 'Inactive' when 2 then 'Legendary' end) AS `Status`,`tbstudents`.`remarks` AS `Remarks` from `tbstudents` ;

-- --------------------------------------------------------

--
-- Structure for view `view_userphoto`
--
DROP TABLE IF EXISTS `view_userphoto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_userphoto`  AS  select `tbusers`.`user_id` AS `user_id`,if(isnull(`tbusers`.`studentid`),`tbusers`.`Photo`,`tbstudents`.`image`) AS `Photo` from (`tbusers` left join `tbstudents` on((`tbusers`.`studentid` = `tbstudents`.`student_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_users`
--
DROP TABLE IF EXISTS `view_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_users`  AS  select `tbusers`.`user_id` AS `User ID`,`tbusers`.`username` AS `username`,if(isnull(`tbusers`.`studentid`),`tbusers`.`Name`,concat(`tbstudents`.`firstname`,' ',`tbstudents`.`lastname`)) AS `Name`,`tbusers`.`type` AS `type`,`tbusers`.`date_created` AS `Date Created`,`b`.`username` AS `Created by` from ((`tbusers` left join `tbstudents` on((`tbusers`.`studentid` = `tbstudents`.`student_id`))) left join `tbusers` `b` on((`b`.`user_id` = `tbusers`.`created_by`))) order by `tbusers`.`user_id` ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbattendance`
--
ALTER TABLE `tbattendance`
  ADD CONSTRAINT `tbattendance_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `tbevents` (`event_id`),
  ADD CONSTRAINT `tbattendance_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `tbstudents` (`student_id`);

--
-- Constraints for table `tbevent_exemptions`
--
ALTER TABLE `tbevent_exemptions`
  ADD CONSTRAINT `tbevent_exemptions_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `tbevents` (`event_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbevent_exemptions_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `tbstudents` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbfingerprints`
--
ALTER TABLE `tbfingerprints`
  ADD CONSTRAINT `tbfingerprints_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `tbstudents` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbusers`
--
ALTER TABLE `tbusers`
  ADD CONSTRAINT `tbusers_ibfk_1` FOREIGN KEY (`studentid`) REFERENCES `tbstudents` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_officers`
--
ALTER TABLE `tb_officers`
  ADD CONSTRAINT `tb_officers_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `tbstudents` (`student_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
