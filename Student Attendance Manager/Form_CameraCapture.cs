﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Imaging.Filters;

namespace Student_Attendance_Manager
{
    public partial class Form_CameraCapture : Form
    {
        private FilterInfoCollection CaptureDevice; // list of webcam
        private VideoCaptureDevice camera;
        private PictureBox residentPhoto;
        public Form_CameraCapture(PictureBox residentPhoto)
        {
            InitializeComponent();
            CaptureDevice = new FilterInfoCollection(FilterCategory.VideoInputDevice);//constructor
                                                                                      //VideoCaptureDeviceForm vcdf = new VideoCaptureDeviceForm();
            this.residentPhoto = residentPhoto;
            foreach (FilterInfo Device in CaptureDevice)
            {
                comboBox1.Items.Add(Device.Name);
            }
            if (comboBox1.Items.Count > 0)
            {
                //vcdf.ShowDialog();
                comboBox1.SelectedIndex = 0; // default
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private void menuPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        void camera_NewFrame(object sender, NewFrameEventArgs eventArgs) // must be void so that it can be accessed everywhere.
                                                                         // New Frame Event Args is an constructor of a class
        {
            pictureBox1.Image = (Bitmap)eventArgs.Frame.Clone();// clone the bitmap
        }

        private void Form_CameraCapture_FormClosing(object sender, FormClosingEventArgs e)
        {
            stopCamera();
            camera = null;
        }

        private void stopCamera()
        {
            if (camera != null && camera.IsRunning == true)
            {
                camera.Stop();
            }
        }

        private void startCamera()
        {
            if (camera != null && camera.IsRunning == false)
            {
                camera.Start();
            }
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bNext_Click(object sender, EventArgs e)
        {
            switch (bNext.Text)
            {
                case "Capture":
                    stopCamera();
                    bNext.Text = "Recapture";
                    bConfirm.Enabled = true;
                    break;
                case "Recapture":
                    startCamera();
                    bNext.Text = "Capture";
                    bConfirm.Enabled = false;
                    break;
            }
        }

        private void bConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                residentPhoto.Image = pictureBox1.Image;
                residentPhoto.Tag = "Photo.jpg";
                residentPhoto.Image.Tag = "new";
            }
            catch(Exception err)
            {
                Console.WriteLine("Error message: " + err.Message + "\r\n\r\n" + err.StackTrace);
            }
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            stopCamera();
            camera = null;
            camera = new VideoCaptureDevice(CaptureDevice[comboBox1.SelectedIndex].MonikerString);// specified web cam and its filter moniker string
            comboBox2.Items.Clear();
            foreach (VideoCapabilities vc in camera.VideoCapabilities)
            {
                comboBox2.Items.Add(vc.FrameSize);
            }
            if (comboBox2.Items.Count > 0)
            {
                comboBox2.SelectedIndex = 0;
            }
            camera.NewFrame += new NewFrameEventHandler(camera_NewFrame);// click button event is fired,
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            stopCamera();
            camera.VideoResolution = camera.VideoCapabilities[comboBox2.SelectedIndex];
            startCamera();
        }
    }
}
