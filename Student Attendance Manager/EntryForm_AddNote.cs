﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class EntryForm_AddNote : Form
    {
        private DatabaseTableView dtv;
        private SQLParameter id;
        private MySQLDatabase db;
        public EntryForm_AddNote(DatabaseTableView dtv, SQLParameter id)
        {
            InitializeComponent();
            db = DatabaseManager.GetInstance();
            this.dtv = dtv;
            this.id = id;
        }

        #region ControlBoxEvents
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();
        private void menuPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void Label5_MouseHover(object sender, EventArgs e)
        {
            bClose.BackColor = Color.Firebrick;
        }

        private void BClose_MouseLeave(object sender, EventArgs e)
        {
            bClose.BackColor = Color.FromArgb(64, 64, 64);
        }
        #endregion

        private void bClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void bConfirm_Click(object sender, EventArgs e)
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter(dtv.SearchColumn, remarks.Text));
            SQLParameter[] prms1 = { id };
            if(db.Update(dtv.Name, prms, prms1))
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
