﻿namespace Student_Attendance_Manager
{
    partial class EntryForm_Student
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuPanel = new System.Windows.Forms.Panel();
            this.bClose = new System.Windows.Forms.Label();
            this.bFP = new System.Windows.Forms.Button();
            this.bPDetails = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.panelStudentInformation = new System.Windows.Forms.Panel();
            this.year_level = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.student_id = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.bBrowsePhoto = new System.Windows.Forms.Button();
            this.residentPhoto = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panelClientName = new System.Windows.Forms.Panel();
            this.birthdate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.sex = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lastName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.middleName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.firstName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panelFPs = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.pbFingerprint = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.bNext = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.menuPanel.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panelStudentInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.year_level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.residentPhoto)).BeginInit();
            this.panelClientName.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFingerprint)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPanel
            // 
            this.menuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.menuPanel.Controls.Add(this.bClose);
            this.menuPanel.Controls.Add(this.bFP);
            this.menuPanel.Controls.Add(this.bPDetails);
            this.menuPanel.Controls.Add(this.label1);
            this.menuPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.menuPanel.Location = new System.Drawing.Point(0, 0);
            this.menuPanel.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(842, 60);
            this.menuPanel.TabIndex = 101;
            this.menuPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuPanel_MouseDown);
            // 
            // bClose
            // 
            this.bClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bClose.ForeColor = System.Drawing.Color.Silver;
            this.bClose.Location = new System.Drawing.Point(782, 0);
            this.bClose.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(60, 60);
            this.bClose.TabIndex = 5;
            this.bClose.Text = "X";
            this.bClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bClose.Click += new System.EventHandler(this.bClose_Click);
            this.bClose.MouseLeave += new System.EventHandler(this.BClose_MouseLeave);
            this.bClose.MouseHover += new System.EventHandler(this.Label5_MouseHover);
            // 
            // bFP
            // 
            this.bFP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.bFP.Dock = System.Windows.Forms.DockStyle.Left;
            this.bFP.FlatAppearance.BorderSize = 0;
            this.bFP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bFP.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bFP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(200)))), ((int)(((byte)(252)))));
            this.bFP.Location = new System.Drawing.Point(270, 0);
            this.bFP.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.bFP.Name = "bFP";
            this.bFP.Size = new System.Drawing.Size(100, 60);
            this.bFP.TabIndex = 4;
            this.bFP.TabStop = false;
            this.bFP.Text = "Fingerprint";
            this.bFP.UseVisualStyleBackColor = false;
            this.bFP.Click += new System.EventHandler(this.bFP_Click);
            // 
            // bPDetails
            // 
            this.bPDetails.BackColor = System.Drawing.Color.White;
            this.bPDetails.Dock = System.Windows.Forms.DockStyle.Left;
            this.bPDetails.FlatAppearance.BorderSize = 0;
            this.bPDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bPDetails.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bPDetails.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bPDetails.Location = new System.Drawing.Point(170, 0);
            this.bPDetails.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.bPDetails.Name = "bPDetails";
            this.bPDetails.Size = new System.Drawing.Size(100, 60);
            this.bPDetails.TabIndex = 101;
            this.bPDetails.TabStop = false;
            this.bPDetails.Text = "Basic Information";
            this.bPDetails.UseVisualStyleBackColor = false;
            this.bPDetails.Click += new System.EventHandler(this.bPDetails_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Britannic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 60);
            this.label1.TabIndex = 0;
            this.label1.Text = "Student";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ItemSize = new System.Drawing.Size(0, 1);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.MaximumSize = new System.Drawing.Size(822, 514);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(822, 471);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 102;
            this.tabControl1.TabStop = false;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.panelStudentInformation);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.bBrowsePhoto);
            this.tabPage1.Controls.Add(this.residentPhoto);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.panelClientName);
            this.tabPage1.Location = new System.Drawing.Point(4, 5);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(30, 34, 30, 34);
            this.tabPage1.Size = new System.Drawing.Size(814, 462);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Enter += new System.EventHandler(this.tabPage1_Enter);
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.RoyalBlue;
            this.label14.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label14.ImageKey = "icons8-user-40.png";
            this.label14.Location = new System.Drawing.Point(12, 170);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(396, 31);
            this.label14.TabIndex = 40;
            this.label14.Text = "Student Information";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelStudentInformation
            // 
            this.panelStudentInformation.BackColor = System.Drawing.Color.White;
            this.panelStudentInformation.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelStudentInformation.Controls.Add(this.year_level);
            this.panelStudentInformation.Controls.Add(this.label8);
            this.panelStudentInformation.Controls.Add(this.label3);
            this.panelStudentInformation.Controls.Add(this.student_id);
            this.panelStudentInformation.Location = new System.Drawing.Point(12, 204);
            this.panelStudentInformation.Name = "panelStudentInformation";
            this.panelStudentInformation.Size = new System.Drawing.Size(396, 57);
            this.panelStudentInformation.TabIndex = 39;
            // 
            // year_level
            // 
            this.year_level.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.year_level.Location = new System.Drawing.Point(110, 27);
            this.year_level.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.year_level.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.year_level.Name = "year_level";
            this.year_level.Size = new System.Drawing.Size(281, 25);
            this.year_level.TabIndex = 39;
            this.year_level.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(1, 27);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.MinimumSize = new System.Drawing.Size(0, 25);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label8.Size = new System.Drawing.Size(109, 25);
            this.label8.TabIndex = 33;
            this.label8.Text = "Year Level";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(1, 1);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.MinimumSize = new System.Drawing.Size(0, 25);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label3.Size = new System.Drawing.Size(109, 25);
            this.label3.TabIndex = 31;
            this.label3.Text = "ID Number";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // student_id
            // 
            this.student_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.student_id.Location = new System.Drawing.Point(110, 1);
            this.student_id.MinimumSize = new System.Drawing.Size(4, 25);
            this.student_id.Name = "student_id";
            this.student_id.Size = new System.Drawing.Size(281, 25);
            this.student_id.TabIndex = 30;
            this.student_id.TextChanged += new System.EventHandler(this.student_id_TextChanged);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(512, 224);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 32);
            this.button2.TabIndex = 38;
            this.button2.TabStop = false;
            this.button2.Text = "Capture";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // bBrowsePhoto
            // 
            this.bBrowsePhoto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.bBrowsePhoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bBrowsePhoto.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bBrowsePhoto.ForeColor = System.Drawing.Color.White;
            this.bBrowsePhoto.Location = new System.Drawing.Point(623, 224);
            this.bBrowsePhoto.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.bBrowsePhoto.Name = "bBrowsePhoto";
            this.bBrowsePhoto.Size = new System.Drawing.Size(89, 32);
            this.bBrowsePhoto.TabIndex = 3;
            this.bBrowsePhoto.Text = "Browse";
            this.bBrowsePhoto.UseVisualStyleBackColor = false;
            this.bBrowsePhoto.Click += new System.EventHandler(this.bBrowsePhoto_Click);
            // 
            // residentPhoto
            // 
            this.residentPhoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.residentPhoto.Location = new System.Drawing.Point(512, 15);
            this.residentPhoto.Name = "residentPhoto";
            this.residentPhoto.Size = new System.Drawing.Size(200, 200);
            this.residentPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.residentPhoto.TabIndex = 37;
            this.residentPhoto.TabStop = false;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.RoyalBlue;
            this.label9.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label9.ImageKey = "icons8-user-40.png";
            this.label9.Location = new System.Drawing.Point(12, 15);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(396, 31);
            this.label9.TabIndex = 26;
            this.label9.Text = "Personal Information";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelClientName
            // 
            this.panelClientName.BackColor = System.Drawing.Color.White;
            this.panelClientName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelClientName.Controls.Add(this.birthdate);
            this.panelClientName.Controls.Add(this.label6);
            this.panelClientName.Controls.Add(this.sex);
            this.panelClientName.Controls.Add(this.label5);
            this.panelClientName.Controls.Add(this.lastName);
            this.panelClientName.Controls.Add(this.label4);
            this.panelClientName.Controls.Add(this.middleName);
            this.panelClientName.Controls.Add(this.label2);
            this.panelClientName.Controls.Add(this.firstName);
            this.panelClientName.Controls.Add(this.label7);
            this.panelClientName.Location = new System.Drawing.Point(12, 49);
            this.panelClientName.Name = "panelClientName";
            this.panelClientName.Size = new System.Drawing.Size(396, 109);
            this.panelClientName.TabIndex = 25;
            // 
            // birthdate
            // 
            this.birthdate.CustomFormat = "MM/dd/yy";
            this.birthdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.birthdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.birthdate.Location = new System.Drawing.Point(297, 79);
            this.birthdate.MinimumSize = new System.Drawing.Size(94, 25);
            this.birthdate.Name = "birthdate";
            this.birthdate.Size = new System.Drawing.Size(94, 25);
            this.birthdate.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(190, 79);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.MinimumSize = new System.Drawing.Size(0, 25);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label6.Size = new System.Drawing.Size(109, 25);
            this.label6.TabIndex = 30;
            this.label6.Text = "Birthdate";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sex
            // 
            this.sex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sex.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sex.FormattingEnabled = true;
            this.sex.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.sex.Location = new System.Drawing.Point(110, 79);
            this.sex.MinimumSize = new System.Drawing.Size(10, 0);
            this.sex.Name = "sex";
            this.sex.Size = new System.Drawing.Size(80, 25);
            this.sex.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(1, 79);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.MinimumSize = new System.Drawing.Size(0, 25);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label5.Size = new System.Drawing.Size(109, 25);
            this.label5.TabIndex = 28;
            this.label5.Text = "Sex";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lastName
            // 
            this.lastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lastName.Location = new System.Drawing.Point(110, 53);
            this.lastName.MinimumSize = new System.Drawing.Size(4, 25);
            this.lastName.Name = "lastName";
            this.lastName.Size = new System.Drawing.Size(281, 25);
            this.lastName.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(1, 53);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.MinimumSize = new System.Drawing.Size(0, 25);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label4.Size = new System.Drawing.Size(109, 25);
            this.label4.TabIndex = 27;
            this.label4.Text = "Last Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // middleName
            // 
            this.middleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.middleName.Location = new System.Drawing.Point(110, 27);
            this.middleName.MinimumSize = new System.Drawing.Size(4, 25);
            this.middleName.Name = "middleName";
            this.middleName.Size = new System.Drawing.Size(281, 25);
            this.middleName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(1, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 25);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label2.Size = new System.Drawing.Size(109, 25);
            this.label2.TabIndex = 25;
            this.label2.Text = "Middle Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // firstName
            // 
            this.firstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstName.Location = new System.Drawing.Point(110, 1);
            this.firstName.MinimumSize = new System.Drawing.Size(4, 25);
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(281, 25);
            this.firstName.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(1, 1);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.MinimumSize = new System.Drawing.Size(0, 25);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label7.Size = new System.Drawing.Size(109, 25);
            this.label7.TabIndex = 23;
            this.label7.Text = "First Name";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.White;
            this.tabPage3.Controls.Add(this.panelFPs);
            this.tabPage3.Controls.Add(this.btnCancel);
            this.tabPage3.Controls.Add(this.txtMessage);
            this.tabPage3.Controls.Add(this.pbFingerprint);
            this.tabPage3.Location = new System.Drawing.Point(4, 5);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.tabPage3.Size = new System.Drawing.Size(814, 462);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Enter += new System.EventHandler(this.tabPage3_Enter);
            // 
            // panelFPs
            // 
            this.panelFPs.BackColor = System.Drawing.Color.White;
            this.panelFPs.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFPs.Location = new System.Drawing.Point(4, 364);
            this.panelFPs.Name = "panelFPs";
            this.panelFPs.Size = new System.Drawing.Size(806, 92);
            this.panelFPs.TabIndex = 6;
            // 
            // btnCancel
            // 
            this.btnCancel.Enabled = false;
            this.btnCancel.Location = new System.Drawing.Point(523, 200);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(92, 33);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            // 
            // txtMessage
            // 
            this.txtMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtMessage.Location = new System.Drawing.Point(523, 6);
            this.txtMessage.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMessage.Size = new System.Drawing.Size(284, 173);
            this.txtMessage.TabIndex = 3;
            // 
            // pbFingerprint
            // 
            this.pbFingerprint.Location = new System.Drawing.Point(623, 191);
            this.pbFingerprint.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.pbFingerprint.Name = "pbFingerprint";
            this.pbFingerprint.Size = new System.Drawing.Size(184, 167);
            this.pbFingerprint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFingerprint.TabIndex = 4;
            this.pbFingerprint.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SteelBlue;
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 60);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panel2.Size = new System.Drawing.Size(842, 532);
            this.panel2.TabIndex = 103;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.bNext);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(10, 471);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(822, 51);
            this.panel4.TabIndex = 105;
            // 
            // bNext
            // 
            this.bNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.bNext.FlatAppearance.BorderSize = 0;
            this.bNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNext.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNext.ForeColor = System.Drawing.Color.White;
            this.bNext.Location = new System.Drawing.Point(704, 9);
            this.bNext.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.bNext.Name = "bNext";
            this.bNext.Size = new System.Drawing.Size(103, 33);
            this.bNext.TabIndex = 41;
            this.bNext.Text = "Next";
            this.bNext.UseVisualStyleBackColor = false;
            this.bNext.Click += new System.EventHandler(this.bNext_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tabControl1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(10, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(822, 471);
            this.panel3.TabIndex = 0;
            // 
            // EntryForm_Student
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(842, 592);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.menuPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EntryForm_Student";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EntryForm_Student";
            this.menuPanel.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panelStudentInformation.ResumeLayout(false);
            this.panelStudentInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.year_level)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.residentPhoto)).EndInit();
            this.panelClientName.ResumeLayout(false);
            this.panelClientName.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFingerprint)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel menuPanel;
        private System.Windows.Forms.Label bClose;
        private System.Windows.Forms.Button bFP;
        private System.Windows.Forms.Button bPDetails;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button bBrowsePhoto;
        private System.Windows.Forms.PictureBox residentPhoto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panelClientName;
        private System.Windows.Forms.TextBox lastName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox middleName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox firstName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panelFPs;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.PictureBox pbFingerprint;
        private System.Windows.Forms.TextBox student_id;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox sex;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker birthdate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panelStudentInformation;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button bNext;
        private System.Windows.Forms.NumericUpDown year_level;
    }
}