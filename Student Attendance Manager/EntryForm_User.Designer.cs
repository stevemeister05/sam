﻿namespace Student_Attendance_Manager
{
    partial class EntryForm_User
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuPanel = new System.Windows.Forms.Panel();
            this.bClose = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.bConfirm = new System.Windows.Forms.Button();
            this.panelPhoto = new System.Windows.Forms.Panel();
            this.bBrowse = new System.Windows.Forms.Button();
            this.bCapture = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.pbUser = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panelOthers = new System.Windows.Forms.Panel();
            this.name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panelStudent = new System.Windows.Forms.Panel();
            this.studentid = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.designation = new System.Windows.Forms.ComboBox();
            this.l222 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.unameStatus = new System.Windows.Forms.Label();
            this.panelUserInfo1 = new System.Windows.Forms.Panel();
            this.type = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.menuPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panelPhoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUser)).BeginInit();
            this.panel4.SuspendLayout();
            this.panelOthers.SuspendLayout();
            this.panelStudent.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelUserInfo1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPanel
            // 
            this.menuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.menuPanel.Controls.Add(this.bClose);
            this.menuPanel.Controls.Add(this.label1);
            this.menuPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.menuPanel.Location = new System.Drawing.Point(0, 0);
            this.menuPanel.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(380, 60);
            this.menuPanel.TabIndex = 103;
            this.menuPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuPanel_MouseDown);
            // 
            // bClose
            // 
            this.bClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bClose.ForeColor = System.Drawing.Color.Silver;
            this.bClose.Location = new System.Drawing.Point(320, 0);
            this.bClose.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(60, 60);
            this.bClose.TabIndex = 5;
            this.bClose.Text = "X";
            this.bClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bClose.Click += new System.EventHandler(this.bClose_Click);
            this.bClose.MouseLeave += new System.EventHandler(this.BClose_MouseLeave);
            this.bClose.MouseHover += new System.EventHandler(this.Label5_MouseHover);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Britannic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 60);
            this.label1.TabIndex = 0;
            this.label1.Text = "User";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 60);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.panel1.Size = new System.Drawing.Size(380, 487);
            this.panel1.TabIndex = 104;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.unameStatus);
            this.panel2.Controls.Add(this.panelUserInfo1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(5, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(21, 19, 24, 10);
            this.panel2.Size = new System.Drawing.Size(370, 482);
            this.panel2.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.panelPhoto);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(21, 189);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(325, 283);
            this.panel5.TabIndex = 45;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.bConfirm);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 216);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.panel7.Size = new System.Drawing.Size(325, 55);
            this.panel7.TabIndex = 37;
            // 
            // bConfirm
            // 
            this.bConfirm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.bConfirm.Dock = System.Windows.Forms.DockStyle.Right;
            this.bConfirm.FlatAppearance.BorderSize = 0;
            this.bConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bConfirm.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bConfirm.ForeColor = System.Drawing.Color.White;
            this.bConfirm.Location = new System.Drawing.Point(222, 20);
            this.bConfirm.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.bConfirm.Name = "bConfirm";
            this.bConfirm.Size = new System.Drawing.Size(103, 35);
            this.bConfirm.TabIndex = 42;
            this.bConfirm.Text = "Confirm";
            this.bConfirm.UseVisualStyleBackColor = false;
            this.bConfirm.Click += new System.EventHandler(this.bConfirm_Click);
            // 
            // panelPhoto
            // 
            this.panelPhoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelPhoto.Controls.Add(this.bBrowse);
            this.panelPhoto.Controls.Add(this.bCapture);
            this.panelPhoto.Controls.Add(this.label4);
            this.panelPhoto.Controls.Add(this.pbUser);
            this.panelPhoto.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPhoto.Location = new System.Drawing.Point(0, 0);
            this.panelPhoto.Name = "panelPhoto";
            this.panelPhoto.Size = new System.Drawing.Size(325, 216);
            this.panelPhoto.TabIndex = 36;
            this.panelPhoto.Visible = false;
            // 
            // bBrowse
            // 
            this.bBrowse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.bBrowse.FlatAppearance.BorderSize = 0;
            this.bBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bBrowse.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bBrowse.ForeColor = System.Drawing.Color.White;
            this.bBrowse.Location = new System.Drawing.Point(7, 149);
            this.bBrowse.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.bBrowse.Name = "bBrowse";
            this.bBrowse.Size = new System.Drawing.Size(96, 35);
            this.bBrowse.TabIndex = 44;
            this.bBrowse.Text = "Browse";
            this.bBrowse.UseVisualStyleBackColor = false;
            this.bBrowse.Click += new System.EventHandler(this.bBrowse_Click);
            // 
            // bCapture
            // 
            this.bCapture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.bCapture.FlatAppearance.BorderSize = 0;
            this.bCapture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bCapture.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bCapture.ForeColor = System.Drawing.Color.White;
            this.bCapture.Location = new System.Drawing.Point(7, 111);
            this.bCapture.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.bCapture.Name = "bCapture";
            this.bCapture.Size = new System.Drawing.Size(96, 35);
            this.bCapture.TabIndex = 43;
            this.bCapture.Text = "Capture";
            this.bCapture.UseVisualStyleBackColor = false;
            this.bCapture.Click += new System.EventHandler(this.bCapture_Click);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(1, 1);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.MinimumSize = new System.Drawing.Size(0, 25);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label4.Size = new System.Drawing.Size(109, 68);
            this.label4.TabIndex = 25;
            this.label4.Text = "Photo";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbUser
            // 
            this.pbUser.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbUser.Location = new System.Drawing.Point(110, 1);
            this.pbUser.Name = "pbUser";
            this.pbUser.Size = new System.Drawing.Size(211, 211);
            this.pbUser.TabIndex = 37;
            this.pbUser.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panelOthers);
            this.panel4.Controls.Add(this.panelStudent);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(21, 127);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(325, 62);
            this.panel4.TabIndex = 43;
            // 
            // panelOthers
            // 
            this.panelOthers.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelOthers.Controls.Add(this.name);
            this.panelOthers.Controls.Add(this.label5);
            this.panelOthers.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelOthers.Location = new System.Drawing.Point(0, 31);
            this.panelOthers.Name = "panelOthers";
            this.panelOthers.Size = new System.Drawing.Size(325, 31);
            this.panelOthers.TabIndex = 35;
            this.panelOthers.Visible = false;
            // 
            // name
            // 
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.Location = new System.Drawing.Point(110, 1);
            this.name.MinimumSize = new System.Drawing.Size(4, 25);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(211, 25);
            this.name.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(1, 1);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.MinimumSize = new System.Drawing.Size(0, 25);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label5.Size = new System.Drawing.Size(109, 25);
            this.label5.TabIndex = 25;
            this.label5.Text = "Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelStudent
            // 
            this.panelStudent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelStudent.Controls.Add(this.studentid);
            this.panelStudent.Controls.Add(this.label3);
            this.panelStudent.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelStudent.Location = new System.Drawing.Point(0, 0);
            this.panelStudent.Name = "panelStudent";
            this.panelStudent.Size = new System.Drawing.Size(325, 31);
            this.panelStudent.TabIndex = 34;
            // 
            // studentid
            // 
            this.studentid.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.studentid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentid.FormattingEnabled = true;
            this.studentid.Items.AddRange(new object[] {
            "Student",
            "Others"});
            this.studentid.Location = new System.Drawing.Point(110, 1);
            this.studentid.MinimumSize = new System.Drawing.Size(10, 0);
            this.studentid.Name = "studentid";
            this.studentid.Size = new System.Drawing.Size(211, 25);
            this.studentid.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(1, 1);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.MinimumSize = new System.Drawing.Size(0, 25);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label3.Size = new System.Drawing.Size(109, 25);
            this.label3.TabIndex = 27;
            this.label3.Text = "Student Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(21, 117);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(325, 10);
            this.panel8.TabIndex = 48;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.designation);
            this.panel3.Controls.Add(this.l222);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(21, 86);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(325, 31);
            this.panel3.TabIndex = 33;
            // 
            // designation
            // 
            this.designation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.designation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.designation.FormattingEnabled = true;
            this.designation.Items.AddRange(new object[] {
            "Student",
            "Others"});
            this.designation.Location = new System.Drawing.Point(110, 1);
            this.designation.MinimumSize = new System.Drawing.Size(10, 0);
            this.designation.Name = "designation";
            this.designation.Size = new System.Drawing.Size(211, 25);
            this.designation.TabIndex = 32;
            this.designation.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // l222
            // 
            this.l222.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.l222.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.l222.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l222.ForeColor = System.Drawing.Color.White;
            this.l222.Location = new System.Drawing.Point(1, 1);
            this.l222.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.l222.MinimumSize = new System.Drawing.Size(0, 25);
            this.l222.Name = "l222";
            this.l222.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.l222.Size = new System.Drawing.Size(109, 25);
            this.l222.TabIndex = 27;
            this.l222.Text = "Designation";
            this.l222.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(21, 76);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(325, 10);
            this.panel6.TabIndex = 47;
            // 
            // unameStatus
            // 
            this.unameStatus.AutoSize = true;
            this.unameStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unameStatus.ForeColor = System.Drawing.Color.Red;
            this.unameStatus.Location = new System.Drawing.Point(367, 47);
            this.unameStatus.Name = "unameStatus";
            this.unameStatus.Size = new System.Drawing.Size(11, 13);
            this.unameStatus.TabIndex = 44;
            this.unameStatus.Text = "!";
            // 
            // panelUserInfo1
            // 
            this.panelUserInfo1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelUserInfo1.Controls.Add(this.type);
            this.panelUserInfo1.Controls.Add(this.label2);
            this.panelUserInfo1.Controls.Add(this.username);
            this.panelUserInfo1.Controls.Add(this.label7);
            this.panelUserInfo1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelUserInfo1.Location = new System.Drawing.Point(21, 19);
            this.panelUserInfo1.Name = "panelUserInfo1";
            this.panelUserInfo1.Size = new System.Drawing.Size(325, 57);
            this.panelUserInfo1.TabIndex = 26;
            // 
            // type
            // 
            this.type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.type.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.type.FormattingEnabled = true;
            this.type.Items.AddRange(new object[] {
            "Admin",
            "Regular"});
            this.type.Location = new System.Drawing.Point(110, 27);
            this.type.MinimumSize = new System.Drawing.Size(10, 0);
            this.type.Name = "type";
            this.type.Size = new System.Drawing.Size(211, 25);
            this.type.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(1, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 25);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label2.Size = new System.Drawing.Size(109, 25);
            this.label2.TabIndex = 27;
            this.label2.Text = "Type";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username.Location = new System.Drawing.Point(110, 1);
            this.username.MinimumSize = new System.Drawing.Size(4, 25);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(211, 25);
            this.username.TabIndex = 24;
            this.username.TextChanged += new System.EventHandler(this.username_TextChanged);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(1, 1);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.MinimumSize = new System.Drawing.Size(0, 25);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label7.Size = new System.Drawing.Size(109, 25);
            this.label7.TabIndex = 25;
            this.label7.Text = "Username";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EntryForm_User
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(380, 547);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EntryForm_User";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EntryForm_User";
            this.menuPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panelPhoto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbUser)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panelOthers.ResumeLayout(false);
            this.panelOthers.PerformLayout();
            this.panelStudent.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panelUserInfo1.ResumeLayout(false);
            this.panelUserInfo1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel menuPanel;
        private System.Windows.Forms.Label bClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelUserInfo1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox type;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox designation;
        private System.Windows.Forms.Label l222;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panelOthers;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panelStudent;
        private System.Windows.Forms.ComboBox studentid;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label unameStatus;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panelPhoto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pbUser;
        private System.Windows.Forms.Button bConfirm;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button bCapture;
        private System.Windows.Forms.Button bBrowse;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel8;
    }
}