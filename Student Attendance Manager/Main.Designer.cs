﻿namespace Student_Attendance_Manager
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.bAttendance = new System.Windows.Forms.ToolStripMenuItem();
            this.bStudents = new System.Windows.Forms.ToolStripMenuItem();
            this.bEvents = new System.Windows.Forms.ToolStripMenuItem();
            this.bOfficers = new System.Windows.Forms.ToolStripMenuItem();
            this.bUsers = new System.Windows.Forms.ToolStripMenuItem();
            this.bExit = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.clubName = new System.Windows.Forms.Label();
            this.schoolName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.clublogo = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.schoolyear = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.sem = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.applabel = new System.Windows.Forms.Label();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.bUser = new System.Windows.Forms.ToolStripMenuItem();
            this.bChangePassword = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.content = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clublogo)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(64)))), ((int)(((byte)(82)))));
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.panel1.Size = new System.Drawing.Size(200, 650);
            this.panel1.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(64)))), ((int)(((byte)(82)))));
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(35, 35);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bAttendance,
            this.bEvents,
            this.bStudents,
            this.bOfficers,
            this.bUsers,
            this.bExit});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(0, 336);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.menuStrip1.Size = new System.Drawing.Size(200, 299);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // bAttendance
            // 
            this.bAttendance.AutoSize = false;
            this.bAttendance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bAttendance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
            this.bAttendance.Image = global::Student_Attendance_Manager.Properties.Resources.Attendance;
            this.bAttendance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bAttendance.Name = "bAttendance";
            this.bAttendance.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
            this.bAttendance.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bAttendance.Size = new System.Drawing.Size(200, 47);
            this.bAttendance.Text = "  Attendance";
            this.bAttendance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bAttendance.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // bStudents
            // 
            this.bStudents.AutoSize = false;
            this.bStudents.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bStudents.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
            this.bStudents.Image = global::Student_Attendance_Manager.Properties.Resources.students;
            this.bStudents.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bStudents.Name = "bStudents";
            this.bStudents.Padding = new System.Windows.Forms.Padding(0);
            this.bStudents.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bStudents.Size = new System.Drawing.Size(200, 47);
            this.bStudents.Text = "  Students";
            this.bStudents.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bStudents.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // bEvents
            // 
            this.bEvents.AutoSize = false;
            this.bEvents.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bEvents.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
            this.bEvents.Image = global::Student_Attendance_Manager.Properties.Resources.events;
            this.bEvents.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bEvents.Name = "bEvents";
            this.bEvents.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
            this.bEvents.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bEvents.Size = new System.Drawing.Size(200, 47);
            this.bEvents.Text = "  Events";
            this.bEvents.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bEvents.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // bOfficers
            // 
            this.bOfficers.AutoSize = false;
            this.bOfficers.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bOfficers.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
            this.bOfficers.Image = global::Student_Attendance_Manager.Properties.Resources.officers;
            this.bOfficers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bOfficers.Name = "bOfficers";
            this.bOfficers.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
            this.bOfficers.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bOfficers.Size = new System.Drawing.Size(200, 47);
            this.bOfficers.Text = "  Officers";
            this.bOfficers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bOfficers.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.bOfficers.Visible = false;
            // 
            // bUsers
            // 
            this.bUsers.AutoSize = false;
            this.bUsers.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bUsers.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
            this.bUsers.Image = global::Student_Attendance_Manager.Properties.Resources.users;
            this.bUsers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bUsers.Name = "bUsers";
            this.bUsers.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
            this.bUsers.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bUsers.Size = new System.Drawing.Size(200, 47);
            this.bUsers.Text = "  Users";
            this.bUsers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bUsers.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // bExit
            // 
            this.bExit.AutoSize = false;
            this.bExit.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
            this.bExit.Image = ((System.Drawing.Image)(resources.GetObject("bExit.Image")));
            this.bExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bExit.Name = "bExit";
            this.bExit.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
            this.bExit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bExit.Size = new System.Drawing.Size(200, 47);
            this.bExit.Text = "  Exit";
            this.bExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bExit.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(64)))), ((int)(((byte)(82)))));
            this.panel3.Controls.Add(this.clubName);
            this.panel3.Controls.Add(this.schoolName);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.clublogo);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 336);
            this.panel3.TabIndex = 4;
            // 
            // clubName
            // 
            this.clubName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.clubName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clubName.ForeColor = System.Drawing.Color.White;
            this.clubName.Location = new System.Drawing.Point(0, 199);
            this.clubName.Name = "clubName";
            this.clubName.Size = new System.Drawing.Size(200, 28);
            this.clubName.TabIndex = 4;
            this.clubName.Text = "ICPEP";
            this.clubName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // schoolName
            // 
            this.schoolName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(64)))), ((int)(((byte)(82)))));
            this.schoolName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.schoolName.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.schoolName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.schoolName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.schoolName.ForeColor = System.Drawing.Color.White;
            this.schoolName.Location = new System.Drawing.Point(0, 227);
            this.schoolName.Multiline = true;
            this.schoolName.Name = "schoolName";
            this.schoolName.ReadOnly = true;
            this.schoolName.Size = new System.Drawing.Size(200, 46);
            this.schoolName.TabIndex = 4;
            this.schoolName.TabStop = false;
            this.schoolName.Text = "Notre Dame of Dadiangas University";
            this.schoolName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Forte", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 60);
            this.label1.TabIndex = 4;
            this.label1.Text = "Stevemeister05";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // clublogo
            // 
            this.clublogo.ErrorImage = ((System.Drawing.Image)(resources.GetObject("clublogo.ErrorImage")));
            this.clublogo.Location = new System.Drawing.Point(35, 60);
            this.clublogo.Name = "clublogo";
            this.clublogo.Size = new System.Drawing.Size(130, 130);
            this.clublogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.clublogo.TabIndex = 4;
            this.clublogo.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.schoolyear);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 273);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 23);
            this.panel4.TabIndex = 0;
            // 
            // schoolyear
            // 
            this.schoolyear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.schoolyear.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.schoolyear.ForeColor = System.Drawing.Color.White;
            this.schoolyear.Location = new System.Drawing.Point(0, 0);
            this.schoolyear.Name = "schoolyear";
            this.schoolyear.Size = new System.Drawing.Size(200, 23);
            this.schoolyear.TabIndex = 6;
            this.schoolyear.Text = "School Year";
            this.schoolyear.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.sem);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 296);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(25, 0, 25, 0);
            this.panel5.Size = new System.Drawing.Size(200, 40);
            this.panel5.TabIndex = 5;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(171)))), ((int)(((byte)(189)))));
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(25, 28);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(150, 2);
            this.panel7.TabIndex = 7;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(25, 30);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(150, 10);
            this.panel6.TabIndex = 0;
            // 
            // sem
            // 
            this.sem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sem.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sem.ForeColor = System.Drawing.Color.White;
            this.sem.Location = new System.Drawing.Point(25, 0);
            this.sem.Name = "sem";
            this.sem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.sem.Size = new System.Drawing.Size(150, 40);
            this.sem.TabIndex = 6;
            this.sem.Text = "Semester";
            this.sem.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.applabel);
            this.panel2.Controls.Add(this.menuStrip2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(200, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(4);
            this.panel2.Size = new System.Drawing.Size(900, 61);
            this.panel2.TabIndex = 7;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form_Main_MouseDown);
            // 
            // applabel
            // 
            this.applabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.applabel.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.applabel.ForeColor = System.Drawing.Color.SteelBlue;
            this.applabel.Location = new System.Drawing.Point(4, 4);
            this.applabel.Name = "applabel";
            this.applabel.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.applabel.Size = new System.Drawing.Size(364, 53);
            this.applabel.TabIndex = 3;
            this.applabel.Text = "STUDENT ATTENDANCE MANAGER";
            this.applabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.Right;
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(25, 25);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bUser,
            this.settingsToolStripMenuItem,
            this.toolStripMenuItem3});
            this.menuStrip2.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip2.Location = new System.Drawing.Point(756, 4);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.ShowItemToolTips = true;
            this.menuStrip2.Size = new System.Drawing.Size(140, 53);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // bUser
            // 
            this.bUser.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.bUser.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bChangePassword,
            this.logoutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.bUser.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bUser.ForeColor = System.Drawing.Color.SteelBlue;
            this.bUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bUser.Name = "bUser";
            this.bUser.Size = new System.Drawing.Size(95, 49);
            this.bUser.Text = "User\'s Name";
            // 
            // bChangePassword
            // 
            this.bChangePassword.ForeColor = System.Drawing.Color.SteelBlue;
            this.bChangePassword.Name = "bChangePassword";
            this.bChangePassword.Size = new System.Drawing.Size(184, 22);
            this.bChangePassword.Text = "Change Password";
            this.bChangePassword.Click += new System.EventHandler(this.bChangePassword_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.ForeColor = System.Drawing.Color.SteelBlue;
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.ForeColor = System.Drawing.Color.SteelBlue;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.settingsToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.settingsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("settingsToolStripMenuItem.Image")));
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(37, 49);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.ToolTipText = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripMenuItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem3.Image")));
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(37, 49);
            this.toolStripMenuItem3.Text = "Settings";
            this.toolStripMenuItem3.ToolTipText = "SMS";
            this.toolStripMenuItem3.Visible = false;
            // 
            // content
            // 
            this.content.Dock = System.Windows.Forms.DockStyle.Fill;
            this.content.Location = new System.Drawing.Point(200, 61);
            this.content.Name = "content";
            this.content.Size = new System.Drawing.Size(900, 589);
            this.content.TabIndex = 9;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1100, 650);
            this.ControlBox = false;
            this.Controls.Add(this.content);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form_Main_MouseDown);
            this.panel1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clublogo)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem bStudents;
        private System.Windows.Forms.ToolStripMenuItem bEvents;
        private System.Windows.Forms.ToolStripMenuItem bAttendance;
        private System.Windows.Forms.ToolStripMenuItem bOfficers;
        private System.Windows.Forms.ToolStripMenuItem bUsers;
        private System.Windows.Forms.ToolStripMenuItem bExit;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label clubName;
        private System.Windows.Forms.TextBox schoolName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox clublogo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label applabel;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem bUser;
        private System.Windows.Forms.ToolStripMenuItem bChangePassword;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.Panel content;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label schoolyear;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label sem;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
    }
}