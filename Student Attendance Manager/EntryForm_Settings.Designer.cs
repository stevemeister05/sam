﻿namespace Student_Attendance_Manager
{
    partial class EntryForm_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuPanel = new System.Windows.Forms.Panel();
            this.bClose = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bConfirm = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panelGeneralSettings = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.appcaption = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.syslocation = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panelSchoolSettings = new System.Windows.Forms.Panel();
            this.semester = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.browseCL = new System.Windows.Forms.Button();
            this.browseSL = new System.Windows.Forms.Button();
            this.school_year = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.no_of_semesters = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.club_logo = new System.Windows.Forms.TextBox();
            this.school_logo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.club_name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.school_name = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.testresult = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panelDatabase = new System.Windows.Forms.Panel();
            this.dbmain = new System.Windows.Forms.TextBox();
            this.db_password = new System.Windows.Forms.TextBox();
            this.db_uid = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dbserver = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.menuPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panelGeneralSettings.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panelSchoolSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.semester)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.no_of_semesters)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panelDatabase.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPanel
            // 
            this.menuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.menuPanel.Controls.Add(this.bClose);
            this.menuPanel.Controls.Add(this.label1);
            this.menuPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.menuPanel.Location = new System.Drawing.Point(0, 0);
            this.menuPanel.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(454, 60);
            this.menuPanel.TabIndex = 103;
            this.menuPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.menuPanel_MouseDown);
            // 
            // bClose
            // 
            this.bClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.bClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bClose.ForeColor = System.Drawing.Color.Silver;
            this.bClose.Location = new System.Drawing.Point(394, 0);
            this.bClose.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(60, 60);
            this.bClose.TabIndex = 100;
            this.bClose.Text = "X";
            this.bClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bClose.Click += new System.EventHandler(this.bClose_Click);
            this.bClose.MouseLeave += new System.EventHandler(this.BClose_MouseLeave);
            this.bClose.MouseHover += new System.EventHandler(this.Label5_MouseHover);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Britannic Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 60);
            this.label1.TabIndex = 0;
            this.label1.Text = "Settings";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 60);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.panel1.Size = new System.Drawing.Size(454, 353);
            this.panel1.TabIndex = 104;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.bConfirm);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(5, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(5);
            this.panel2.Size = new System.Drawing.Size(444, 348);
            this.panel2.TabIndex = 0;
            // 
            // bConfirm
            // 
            this.bConfirm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.bConfirm.FlatAppearance.BorderSize = 0;
            this.bConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bConfirm.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bConfirm.ForeColor = System.Drawing.Color.White;
            this.bConfirm.Location = new System.Drawing.Point(322, 305);
            this.bConfirm.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.bConfirm.Name = "bConfirm";
            this.bConfirm.Size = new System.Drawing.Size(103, 33);
            this.bConfirm.TabIndex = 50;
            this.bConfirm.Text = "Confirm";
            this.bConfirm.UseVisualStyleBackColor = false;
            this.bConfirm.Click += new System.EventHandler(this.bConfirm_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tabControl1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(5, 5);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(434, 292);
            this.panel4.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ItemSize = new System.Drawing.Size(120, 25);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(434, 292);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panelGeneralSettings);
            this.tabPage1.ForeColor = System.Drawing.Color.SteelBlue;
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(10);
            this.tabPage1.Size = new System.Drawing.Size(426, 259);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panelGeneralSettings
            // 
            this.panelGeneralSettings.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelGeneralSettings.Controls.Add(this.button2);
            this.panelGeneralSettings.Controls.Add(this.appcaption);
            this.panelGeneralSettings.Controls.Add(this.label2);
            this.panelGeneralSettings.Controls.Add(this.syslocation);
            this.panelGeneralSettings.Controls.Add(this.label7);
            this.panelGeneralSettings.Location = new System.Drawing.Point(23, 25);
            this.panelGeneralSettings.Name = "panelGeneralSettings";
            this.panelGeneralSettings.Size = new System.Drawing.Size(380, 97);
            this.panelGeneralSettings.TabIndex = 27;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(344, 1);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(31, 25);
            this.button2.TabIndex = 32;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // appcaption
            // 
            this.appcaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appcaption.Location = new System.Drawing.Point(131, 27);
            this.appcaption.MinimumSize = new System.Drawing.Size(4, 25);
            this.appcaption.Multiline = true;
            this.appcaption.Name = "appcaption";
            this.appcaption.Size = new System.Drawing.Size(244, 63);
            this.appcaption.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(1, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MinimumSize = new System.Drawing.Size(0, 25);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label2.Size = new System.Drawing.Size(130, 63);
            this.label2.TabIndex = 27;
            this.label2.Text = "App Caption";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // syslocation
            // 
            this.syslocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.syslocation.Location = new System.Drawing.Point(131, 1);
            this.syslocation.MinimumSize = new System.Drawing.Size(4, 25);
            this.syslocation.Name = "syslocation";
            this.syslocation.Size = new System.Drawing.Size(211, 25);
            this.syslocation.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(1, 1);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.MinimumSize = new System.Drawing.Size(0, 25);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label7.Size = new System.Drawing.Size(130, 25);
            this.label7.TabIndex = 25;
            this.label7.Text = "App Files Location";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panelSchoolSettings);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(426, 259);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "School";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panelSchoolSettings
            // 
            this.panelSchoolSettings.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelSchoolSettings.Controls.Add(this.semester);
            this.panelSchoolSettings.Controls.Add(this.label14);
            this.panelSchoolSettings.Controls.Add(this.browseCL);
            this.panelSchoolSettings.Controls.Add(this.browseSL);
            this.panelSchoolSettings.Controls.Add(this.school_year);
            this.panelSchoolSettings.Controls.Add(this.label13);
            this.panelSchoolSettings.Controls.Add(this.no_of_semesters);
            this.panelSchoolSettings.Controls.Add(this.label9);
            this.panelSchoolSettings.Controls.Add(this.club_logo);
            this.panelSchoolSettings.Controls.Add(this.school_logo);
            this.panelSchoolSettings.Controls.Add(this.label6);
            this.panelSchoolSettings.Controls.Add(this.label5);
            this.panelSchoolSettings.Controls.Add(this.club_name);
            this.panelSchoolSettings.Controls.Add(this.label3);
            this.panelSchoolSettings.Controls.Add(this.school_name);
            this.panelSchoolSettings.Controls.Add(this.label4);
            this.panelSchoolSettings.Location = new System.Drawing.Point(24, 18);
            this.panelSchoolSettings.Name = "panelSchoolSettings";
            this.panelSchoolSettings.Size = new System.Drawing.Size(380, 188);
            this.panelSchoolSettings.TabIndex = 30;
            // 
            // semester
            // 
            this.semester.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.semester.Location = new System.Drawing.Point(131, 157);
            this.semester.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.semester.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.semester.Name = "semester";
            this.semester.Size = new System.Drawing.Size(244, 25);
            this.semester.TabIndex = 38;
            this.semester.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label14.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(1, 157);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.MinimumSize = new System.Drawing.Size(0, 25);
            this.label14.Name = "label14";
            this.label14.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label14.Size = new System.Drawing.Size(130, 25);
            this.label14.TabIndex = 39;
            this.label14.Text = "Semester";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // browseCL
            // 
            this.browseCL.Location = new System.Drawing.Point(344, 79);
            this.browseCL.Name = "browseCL";
            this.browseCL.Size = new System.Drawing.Size(31, 25);
            this.browseCL.TabIndex = 37;
            this.browseCL.Text = "...";
            this.browseCL.UseVisualStyleBackColor = true;
            this.browseCL.Click += new System.EventHandler(this.browseCL_Click);
            // 
            // browseSL
            // 
            this.browseSL.Location = new System.Drawing.Point(344, 53);
            this.browseSL.Name = "browseSL";
            this.browseSL.Size = new System.Drawing.Size(31, 25);
            this.browseSL.TabIndex = 31;
            this.browseSL.Text = "...";
            this.browseSL.UseVisualStyleBackColor = true;
            this.browseSL.Click += new System.EventHandler(this.browseSL_Click);
            // 
            // school_year
            // 
            this.school_year.CustomFormat = "yyyy";
            this.school_year.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.school_year.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.school_year.Location = new System.Drawing.Point(131, 131);
            this.school_year.Name = "school_year";
            this.school_year.ShowUpDown = true;
            this.school_year.Size = new System.Drawing.Size(244, 25);
            this.school_year.TabIndex = 16;
            this.school_year.Tag = "year";
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label13.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(1, 131);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.MinimumSize = new System.Drawing.Size(0, 25);
            this.label13.Name = "label13";
            this.label13.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label13.Size = new System.Drawing.Size(130, 25);
            this.label13.TabIndex = 36;
            this.label13.Text = "School Year";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // no_of_semesters
            // 
            this.no_of_semesters.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.no_of_semesters.Location = new System.Drawing.Point(131, 105);
            this.no_of_semesters.Name = "no_of_semesters";
            this.no_of_semesters.Size = new System.Drawing.Size(244, 25);
            this.no_of_semesters.TabIndex = 15;
            this.no_of_semesters.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.no_of_semesters.ValueChanged += new System.EventHandler(this.no_of_semesters_ValueChanged);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(1, 105);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.MinimumSize = new System.Drawing.Size(0, 25);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label9.Size = new System.Drawing.Size(130, 25);
            this.label9.TabIndex = 34;
            this.label9.Text = "No. of Semesters";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // club_logo
            // 
            this.club_logo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.club_logo.Location = new System.Drawing.Point(131, 79);
            this.club_logo.MinimumSize = new System.Drawing.Size(4, 25);
            this.club_logo.Name = "club_logo";
            this.club_logo.Size = new System.Drawing.Size(212, 25);
            this.club_logo.TabIndex = 14;
            // 
            // school_logo
            // 
            this.school_logo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.school_logo.Location = new System.Drawing.Point(131, 53);
            this.school_logo.MinimumSize = new System.Drawing.Size(4, 25);
            this.school_logo.Name = "school_logo";
            this.school_logo.Size = new System.Drawing.Size(212, 25);
            this.school_logo.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(1, 79);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.MinimumSize = new System.Drawing.Size(0, 25);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label6.Size = new System.Drawing.Size(130, 25);
            this.label6.TabIndex = 31;
            this.label6.Text = "Club Logo";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(1, 53);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.MinimumSize = new System.Drawing.Size(0, 25);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label5.Size = new System.Drawing.Size(130, 25);
            this.label5.TabIndex = 29;
            this.label5.Text = "School Logo";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // club_name
            // 
            this.club_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.club_name.Location = new System.Drawing.Point(131, 27);
            this.club_name.MinimumSize = new System.Drawing.Size(4, 25);
            this.club_name.Name = "club_name";
            this.club_name.Size = new System.Drawing.Size(244, 25);
            this.club_name.TabIndex = 12;
            this.club_name.Text = "ICPEP.se";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(1, 27);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.MinimumSize = new System.Drawing.Size(0, 25);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label3.Size = new System.Drawing.Size(130, 25);
            this.label3.TabIndex = 27;
            this.label3.Text = "Club Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // school_name
            // 
            this.school_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.school_name.Location = new System.Drawing.Point(131, 1);
            this.school_name.MinimumSize = new System.Drawing.Size(4, 25);
            this.school_name.Name = "school_name";
            this.school_name.Size = new System.Drawing.Size(244, 25);
            this.school_name.TabIndex = 11;
            this.school_name.Text = "Notre Dame of Dadiangas University";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(1, 1);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.MinimumSize = new System.Drawing.Size(0, 25);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label4.Size = new System.Drawing.Size(130, 25);
            this.label4.TabIndex = 25;
            this.label4.Text = "School Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.testresult);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.panelDatabase);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(426, 259);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Database";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // testresult
            // 
            this.testresult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testresult.Location = new System.Drawing.Point(23, 140);
            this.testresult.Name = "testresult";
            this.testresult.Size = new System.Drawing.Size(270, 33);
            this.testresult.TabIndex = 45;
            this.testresult.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(300, 140);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 33);
            this.button1.TabIndex = 35;
            this.button1.Text = "Test Connection";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panelDatabase
            // 
            this.panelDatabase.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelDatabase.Controls.Add(this.dbmain);
            this.panelDatabase.Controls.Add(this.db_password);
            this.panelDatabase.Controls.Add(this.db_uid);
            this.panelDatabase.Controls.Add(this.label8);
            this.panelDatabase.Controls.Add(this.label10);
            this.panelDatabase.Controls.Add(this.label11);
            this.panelDatabase.Controls.Add(this.dbserver);
            this.panelDatabase.Controls.Add(this.label12);
            this.panelDatabase.Location = new System.Drawing.Point(23, 25);
            this.panelDatabase.Name = "panelDatabase";
            this.panelDatabase.Size = new System.Drawing.Size(380, 110);
            this.panelDatabase.TabIndex = 30;
            // 
            // dbmain
            // 
            this.dbmain.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dbmain.Location = new System.Drawing.Point(131, 27);
            this.dbmain.MinimumSize = new System.Drawing.Size(4, 25);
            this.dbmain.Name = "dbmain";
            this.dbmain.Size = new System.Drawing.Size(244, 25);
            this.dbmain.TabIndex = 32;
            this.dbmain.Tag = "encrypted";
            // 
            // db_password
            // 
            this.db_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.db_password.Location = new System.Drawing.Point(131, 79);
            this.db_password.MinimumSize = new System.Drawing.Size(4, 25);
            this.db_password.Name = "db_password";
            this.db_password.Size = new System.Drawing.Size(244, 25);
            this.db_password.TabIndex = 34;
            this.db_password.Tag = "encrypted";
            this.db_password.UseSystemPasswordChar = true;
            // 
            // db_uid
            // 
            this.db_uid.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.db_uid.Location = new System.Drawing.Point(131, 53);
            this.db_uid.MinimumSize = new System.Drawing.Size(4, 25);
            this.db_uid.Name = "db_uid";
            this.db_uid.Size = new System.Drawing.Size(244, 25);
            this.db_uid.TabIndex = 33;
            this.db_uid.Tag = "encrypted";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(1, 79);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.MinimumSize = new System.Drawing.Size(0, 25);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label8.Size = new System.Drawing.Size(130, 25);
            this.label8.TabIndex = 31;
            this.label8.Text = "Password";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(1, 53);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.MinimumSize = new System.Drawing.Size(0, 25);
            this.label10.Name = "label10";
            this.label10.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label10.Size = new System.Drawing.Size(130, 25);
            this.label10.TabIndex = 29;
            this.label10.Text = "Username";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label11.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(1, 27);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.MinimumSize = new System.Drawing.Size(0, 25);
            this.label11.Name = "label11";
            this.label11.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label11.Size = new System.Drawing.Size(130, 25);
            this.label11.TabIndex = 27;
            this.label11.Text = "Database Name";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dbserver
            // 
            this.dbserver.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dbserver.Location = new System.Drawing.Point(131, 1);
            this.dbserver.MinimumSize = new System.Drawing.Size(4, 25);
            this.dbserver.Name = "dbserver";
            this.dbserver.Size = new System.Drawing.Size(244, 25);
            this.dbserver.TabIndex = 31;
            this.dbserver.Tag = "encrypted";
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(128)))), ((int)(((byte)(201)))));
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label12.Font = new System.Drawing.Font("Calibri", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(1, 1);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.MinimumSize = new System.Drawing.Size(0, 25);
            this.label12.Name = "label12";
            this.label12.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.label12.Size = new System.Drawing.Size(130, 25);
            this.label12.TabIndex = 25;
            this.label12.Text = "Server";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EntryForm_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(454, 413);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EntryForm_Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "EntryForm_Settings";
            this.menuPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panelGeneralSettings.ResumeLayout(false);
            this.panelGeneralSettings.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panelSchoolSettings.ResumeLayout(false);
            this.panelSchoolSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.semester)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.no_of_semesters)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panelDatabase.ResumeLayout(false);
            this.panelDatabase.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel menuPanel;
        private System.Windows.Forms.Label bClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button bConfirm;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panelGeneralSettings;
        private System.Windows.Forms.TextBox appcaption;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox syslocation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panelDatabase;
        private System.Windows.Forms.TextBox db_password;
        private System.Windows.Forms.TextBox db_uid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox dbserver;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox dbmain;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label testresult;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panelSchoolSettings;
        private System.Windows.Forms.TextBox club_logo;
        private System.Windows.Forms.TextBox school_logo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox club_name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox school_name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown no_of_semesters;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker school_year;
        private System.Windows.Forms.Button browseCL;
        private System.Windows.Forms.Button browseSL;
        private System.Windows.Forms.NumericUpDown semester;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button2;
    }
}