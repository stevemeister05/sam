﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class Control_Students : UserControl
    {
        private DatabaseTableView view;
        private MySQLDatabase db;
        public Control_Students(DatabaseTableView view)
        {
            InitializeComponent();
            new ListViewSorter(listView1);
            db = DatabaseManager.GetInstance();
            this.view = view;
            if(view != null)
            {
                control_StandardControls1.View = view;
            }
            control_StandardControls1.HideControl(Control_StandardControls.HideOptions.SearchDateControls);
            control_StandardControls1.TransactionName = "Students";
            control_StandardControls1.ListView = listView1;
            control_StandardControls1.SetButtonNewClick(buttonNew_Click);
            if (ActiveUser.type.ToLower() != "regular")
            {
                control_StandardControls1.SetButtonEditClick(buttonEdit_Click);
                control_StandardControls1.SetButtonDeleteClick(buttonDelete_Click);
            }
            status.SelectedIndex = 0;
            LoadListView(true);
        }

        private void LoadListView(bool firstLoad = false)
        {
            if (this.view != null)
            {
                List<SQLParameter> prms = null;
                if(status.SelectedIndex == 0)
                {
                    prms = new List<SQLParameter>();
                    prms.Add(new SQLParameter("status", "Active"));
                }
                else if(status.SelectedIndex == 1)
                {
                    prms = new List<SQLParameter>();
                    prms.Add(new SQLParameter("status", "Inactive"));
                }
                IDataReader reader = db.Select("SELECT * FROM " + this.view.Name, prms);
                ListViewManager.populateListView(listView1, reader, firstLoad);
            }
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            EntryForm_Student efs = new EntryForm_Student();
            if (efs.ShowDialog() == DialogResult.OK)
            {
                LoadListView();
            }
            efs.Dispose();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if(listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select an item to delete.");
                return;
            }
            string sid = listView1.SelectedItems[0].Name;
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("student_id", sid));
            if (DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT status FROM tbstudents", prms)) == 2)
            {
                if(ActiveUser.type.ToLower() != "creator")
                {
                    MessageBox.Show("You don't have the access to this Legend.");
                    return;
                }
            }
            int a = Convert.ToInt32(db.ScalarSelect("SELECT COUNT(*) FROM tbattendance", prms));
            prms.Clear();
            prms.Add(new SQLParameter("studentid", sid));
            int b = Convert.ToInt32(db.ScalarSelect("SELECT COUNT(*) FROM tbusers", prms));
            if(a > 0 || b > 0)
            {
                MessageBox.Show("You can't delete this student because he/she has existing attendance/user record.");
                return;
            }
            if(MessageBox.Show("Are you sure you want to delete this item?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                prms.Clear();
                prms.Add(new SQLParameter("student_id", sid));
                if(db.Delete("tbstudents", prms))
                {
                    string folderName = AppConfiguration.GetConfig("syslocation") + "Students\\" + sid;
                    clearStudentDetails();
                    MessageBox.Show("Naclear na");
                    if (FileManager.DeleteFolder(folderName, true))
                    {
                        LoadListView();
                    }
                }
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select an item to delete.");
                return;
            }
            string sid = listView1.SelectedItems[0].Name;
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("student_id", sid));
            if (DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT status FROM tbstudents", prms)) == 2)
            {
                if (ActiveUser.type.ToLower() != "creator")
                {
                    MessageBox.Show("You don't have the access to this Legend.");
                    return;
                }
            }
            EntryForm_Student efs = new EntryForm_Student(listView1.SelectedItems[0].Name);
            if(efs.ShowDialog() == DialogResult.OK)
            {
                LoadListView();
                clearStudentDetails();
            }
            efs.Dispose();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
            {
                return;
            }
            try
            {
                listView2.Items.Clear();
                pbStudent.Image = null;
                List<SQLParameter> prms = new List<SQLParameter>();
                prms.Add(new SQLParameter("student_id", listView1.SelectedItems[0].Name));
                Image img = Image.FromFile(DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT image FROM tbstudents", prms)));
                pbStudent.Image = new Bitmap((Image)img.Clone());
                img.Dispose();
                img = null;
                if (DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT status FROM tbstudents", prms)) != 1)
                {
                    return;
                }
                prms.Clear();
                prms.Add(new SQLParameter("status", "Cancelled", "!="));
                prms.Add(new SQLParameter("school_year", db.ScalarSelect("SELECT school_year FROM tbappdefaults")));
                prms.Add(new SQLParameter("semester", db.ScalarSelect("SELECT semester FROM tbappdefaults")));
                IDataReader reader = db.Select("SELECT event_id, title, type FROM tbevents", prms);
                DataTable dt = new DataTable();
                dt.Load(reader);
                reader.Close();
                prms.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    prms.Add(new SQLParameter("student_id", listView1.SelectedItems[0].Name, "=", "ID Number"));
                    string event_id = DataTypeManager.ParseDataToString(dt.Rows[i][0]);
                    string event_name = DataTypeManager.ParseDataToString(dt.Rows[i][1]);
                    string type = DataTypeManager.ParseDataToString(dt.Rows[i][2]);
                    prms.Add(new SQLParameter("event_id", event_id, "=", "Event ID"));
                    prms.Add(new SQLParameter("deleted", 0, "="));
                    int attd = DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT COUNT(*) FROM view_attendance", prms));
                    string attndnce = "Absent";
                    string[] item = { event_name, type };
                    ListViewItem lvi = new ListViewItem(item);
                    lvi.UseItemStyleForSubItems = false;
                    if (attd > 0)
                    {
                        attndnce = "Present";
                        lvi.SubItems.Add(attndnce, Color.Green, Color.White, new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold));
                        string rmrks = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT Remarks FROM view_attendance", prms));
                        lvi.SubItems.Add(rmrks, rmrks == "Ok" ? Color.Green : Color.Red, Color.White, new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold));
                        lvi.SubItems.Add(DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT Notes FROM view_attendance", prms)));
                    }
                    else
                    {
                        prms.Clear();
                        prms.Add(new SQLParameter("event_id", event_id));
                        prms.Add(new SQLParameter("student_id", listView1.SelectedItems[0].Name));
                        int isexemp = DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT COUNT(id) FROM tbevent_exemptions", prms));
                        if (isexemp > 0)
                        {
                            lvi.SubItems.Add("Exempted", Color.Green, Color.White, new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold));
                            lvi.SubItems.Add("Ok", Color.Green, Color.White, new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold));
                            lvi.SubItems.Add(DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT remarks FROM tbevent_exemptions", prms)));
                        }
                        else
                        {
                            lvi.SubItems.Add(attndnce, Color.Red, Color.White, new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold));
                        }
                    }
                    prms.Clear();
                    listView2.Items.Add(lvi);
                }
                listView2.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            catch(Exception err)
            {
                Console.WriteLine("Error message: " + err.Message + "\r\n\r\n" + err.StackTrace);
            }
            
        }

        private void clearStudentDetails()
        {
            Image iOld = this.pbStudent.Image;
            pbStudent.Image.Dispose();
            this.pbStudent.Image = null;
            if (iOld != null)
            {
                iOld.Dispose();
            }
            listView2.Items.Clear();
            clientname.Text = "";
        }

        private void bUpdate_Click(object sender, EventArgs e)
        {
            if(listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select student/s to update");
                return;
            }
            string sid = listView1.SelectedItems[0].Name;
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("student_id", sid));
            if (DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT status FROM tbstudents", prms)) == 2)
            {
                MessageBox.Show("You don't have the access to this Legend.");
                return;
            }
            UpdateForm_Student ufs = new UpdateForm_Student(listView1.SelectedItems);
            if(ufs.ShowDialog() == DialogResult.OK)
            {
                LoadListView();
            }
            ufs.Dispose();
        }

        private ColumnHeader SortingColumn = null;
        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ListViewManager.sortListview(listView1, ref SortingColumn, e);
        }

        private void status_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadListView();
        }
    }
}
