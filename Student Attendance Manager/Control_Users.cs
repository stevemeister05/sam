﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class Control_Users : UserControl
    {
        private DatabaseTableView view;
        private MySQLDatabase db;
        public Control_Users(DatabaseTableView view = null)
        {
            InitializeComponent();
            db = DatabaseManager.GetInstance();
            this.view = view;
            if (view != null)
            {
                control_StandardControls1.View = view;
            }

            control_StandardControls1.TransactionName = "Users";
            control_StandardControls1.ListView = listView1;
            control_StandardControls1.StartDate = new DateTime(2019, 10, 30);
            control_StandardControls1.EndDate = DateTime.Today;
            control_StandardControls1.SetButtonNewClick(bAdd_Click);
            control_StandardControls1.SetButtonDeleteClick(bDelete_Click);
            //control_StandardControls1.SetButtonEditClick(buttonEdit_Click);
            control_StandardControls1.loadListView(true);
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            EntryForm_User efu = new EntryForm_User();
            if(efu.ShowDialog() == DialogResult.OK)
            {
                control_StandardControls1.loadListView();
            }
            efu.Dispose();
        }
        private void bDelete_Click(object sender, EventArgs e)
        {
            if(listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select a user to delete");
                return;
            }
            if(MessageBox.Show("Are you sure you want to delete this user?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                List<SQLParameter> prms = new List<SQLParameter>();
                SQLParameter userid = new SQLParameter("user_id", listView1.SelectedItems[0].Name);
                prms.Add(userid);
                if (Convert.ToString(db.ScalarSelect("SELECT type FROM tbusers", prms)) == "Creator")
                {
                    MessageBox.Show("You don't have access to delete this user.");
                    return;
                }
                if(db.Delete("tbusers", prms))
                {
                    control_StandardControls1.loadListView();
                }
            }
        }
    }
}
