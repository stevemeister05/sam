﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DPUruNet;

namespace Student_Attendance_Manager
{
    public partial class Control_Attendance : UserControl
    {
        private MySQLDatabase db;
        private FPReader fpreader;
        public Control_Attendance()
        {
            InitializeComponent();
            init();
        }

        public void init()
        {
            db = DatabaseManager.GetInstance();
            populateEvents();
            fpreader = FPReaderManager.GetInstance();
            if (fpreader.CurrentReader == null && ReaderCollection.GetReaders().Count > 0)
            {
                fpreader.CurrentReader = ReaderCollection.GetReaders()[0];
            }
            if (fpreader.CurrentReader != null)
            {
                if (!fpreader.OpenReader())
                {
                    MessageBox.Show("Can't open reader. Please make sure the Fingerprint reader is attached.");
                }
                if (!fpreader.StartCaptureAsync(this.OnCaptured))
                {
                }
            }
            IDataReader reader = db.Select("SELECT `ID Number` AS sysID, name as Description FROM view_students WHERE status = 'Active'");
            FormManager.SetComboBoxDataSource(students, reader);
            students.AutoCompleteMode = AutoCompleteMode.Append;
            students.AutoCompleteSource = AutoCompleteSource.ListItems;
            students.SelectedIndex = -1;
        }

        private void populateEvents()
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("date", DateTime.Today.ToString("yyyy-MM-dd")));
            prms.Add(new SQLParameter("status", "Open"));
            prms.Add(new SQLParameter("semester", db.ScalarSelect("SELECT semester FROM tbappdefaults")));
            prms.Add(new SQLParameter("school_year", db.ScalarSelect("SELECT school_year FROM tbappdefaults")));
            IDataReader reader = db.Select("SELECT event_id AS sysID, CONCAT(title, ' (Semester ', semester, ', S.Y. ', school_year,')') AS Description FROM tbevents", prms);
            FormManager.SetComboBoxDataSource(events, reader);
            events.SelectedIndex = -1;
            if (events.Items.Count > 0)
            {
                events.SelectedIndex = 0;
                bCloseEvent.Enabled = true;
            }
            else
            {
                bCloseEvent.Enabled = true;
                if (fpreader != null && fpreader.CurrentReader != null)
                {
                    fpreader.CancelCaptureAndCloseReader();
                }
                students.DataSource = null;
                listView1.Clear();
            }
        }

        private void populateExemptions(string id)
        {
            IDataReader reader = db.Select("SELECT tbevent_exemptions.student_id AS student_id, view_students.Name as Name, tbevent_exemptions.remarks FROM tbevent_exemptions JOIN view_students on (view_students.`ID Number` = tbevent_exemptions.student_id)  WHERE tbevent_exemptions.event_id='" + id + "'");
            ListViewManager.populateListView(listView2, reader, true);
        }

        private void events_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("event_id", events.SelectedValue));
            IDataReader reader = db.Select("SELECT * FROM tbevents", prms);
            FormManager.populateFields(panelEventDetails, reader);
            string temp = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT addtime(time, sec_to_time(grace_period*60)) FROM tbevents", prms));
            temp = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT time FROM tbevents", prms)) + " - " + temp;
            tip.Text = temp;
            top.Text = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT log_out_time FROM tbevents", prms)) + " onwards";
            loadListView();
        }

        private void OnCaptured(CaptureResult captureResult)
        {
            try
            {
                if (!fpreader.CheckCaptureResult(captureResult)) return;
                foreach (Fid.Fiv fiv in captureResult.Data.Views)
                {
                    SendMessage(FPReader.Action.SendBitmap, fpreader.CreateBitmap(fiv.RawImage, fiv.Width, fiv.Height));
                }
                DataResult<Fmd> resultConversion = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ANSI);
                if (resultConversion.ResultCode != Constants.ResultCode.DP_SUCCESS)
                {
                    fpreader.Reset = true;
                    throw new Exception(resultConversion.ResultCode.ToString());
                }
                Fmd firstFinger = resultConversion.Data;
                IDataReader reader = db.Select("SELECT student_id, fingerprint FROM view_fingerprints WHERE status=1");
                if(reader != null)
                {
                    bool matched = false;
                    while(!matched && reader.Read())
                    {
                        Fmd secondFinger = Fmd.DeserializeXml(reader.GetString(1));
                        CompareResult compareResult = Comparison.Compare(firstFinger, 0, secondFinger, 0);
                        if (compareResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
                        {
                            fpreader.Reset = true;
                            reader.Close();
                            throw new Exception(compareResult.ResultCode.ToString());
                        }
                        if(compareResult.Score < (fpreader.PROBABILITY_ONE/ 100000))
                        {
                            matched = true;
                            object id = reader.GetValue(0);
                            reader.Close();
                            SendMessage(FPReader.Action.RegisterStudent, id);
                            //break;
                        }
                    }
                    reader.Close();
                    if(!matched)
                    {
                        SendMessage(FPReader.Action.RegisterStudent);
                    }
                }
            }
            catch (Exception ex)
            {
                SendMessage(FPReader.Action.SendMessage, "Error:  " + ex.Message);
            }
        }

        #region SendMessage

        private delegate void SendMessageCallback(FPReader.Action action, object payload = null);
        private void SendMessage(FPReader.Action action, object payload = null)
        {
            try
            {
                if (this.pbFingerprint.InvokeRequired)
                {
                    SendMessageCallback d = new SendMessageCallback(SendMessage);
                    this.Invoke(d, new object[] { action, payload });
                }
                else
                {
                    switch (action)
                    {
                        case FPReader.Action.SendMessage:
                            studentName.Text = Convert.ToString(payload);
                            break;
                        case FPReader.Action.SendBitmap:
                            pbFingerprint.Image = (Bitmap)payload;
                            pbFingerprint.Refresh();
                            break;
                        case FPReader.Action.RegisterStudent:
                            if(payload == null)
                            {
                                studentName.Text = "No match found.";
                                pbStudent.Image = null;
                                break;
                            }
                            string studentID = Convert.ToString(payload);
                            List<SQLParameter> prms = new List<SQLParameter>();
                            prms.Add(new SQLParameter("event_id", events.SelectedValue));
                            string sttsss = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT status FROM tbevents", prms));
                            prms.Clear();
                            if(sttsss != "Open")
                            {
                                events.SelectedIndex = -1;
                                return;
                            }
                            prms.Add(new SQLParameter("ID_Number", Convert.ToString(payload), "=", "ID Number"));
                            studentName.Text = Convert.ToString(db.ScalarSelect("SELECT Name FROM view_students", prms));
                            prms.Clear();
                            prms.Add(new SQLParameter("student_id", Convert.ToString(payload)));
                            try
                            {
                                pbStudent.Load(Convert.ToString(db.ScalarSelect("SELECT image FROM tbstudents", prms)));
                            }
                            catch(Exception err)
                            {
                                Console.WriteLine("Error message: " + err.Message + "\r\n\r\n" + err.StackTrace);
                                pbStudent.Image = null;
                            }
                            prms.Add(new SQLParameter("deleted", 0));
                            prms.Add(new SQLParameter("event_id", events.SelectedValue));
                            object idd = db.ScalarSelect("SELECT id FROM tbattendance", prms);
                            DateTime ts = DataTypeManager.ParseDataToTime(db.ScalarSelect("SELECT time FROM tbevents WHERE event_id='" + events.SelectedValue + "'"));
                            if (idd == null)
                            {
                                DateTime ti = DateTime.Now;
                                prms.Add(new SQLParameter("time_in", ti.ToString("HH:mm:ss")));
                                db.Insert("tbattendance", prms);

                            }
                            else
                            {
                                string remarkss = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT remarks FROM tbattendance", prms));
                                object to = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT time_out FROM tbattendance", prms));
                                SQLParameter[] prmss = { new SQLParameter("id", idd) };
                                if (to == null)
                                {
                                    prms.Clear();
                                    DateTime too = DateTime.Now;
                                    prms.Add(new SQLParameter("time_out", too.ToString("HH:mm:ss")));
                                    db.Update("tbattendance", prms, prmss);
                                }
                                else
                                {
                                    MessageBox.Show("Error! Can't record this log because this student has already completed his/her attendance for this event.");
                                }
                            }
                            loadListView();
                            break;
                        case FPReader.Action.CloseReader:
                            fpreader.CurrentReader.Reset();
                            break;
                    }
                }
            }
            catch (Exception err)
            {
                Console.WriteLine(err.StackTrace);
                MessageBox.Show(err.Message);
            }
        }
        #endregion

        private void Control_Attendance_ParentChanged(object sender, EventArgs e)
        {
            if(this.Parent == null)
            {
                FPReaderManager.GetInstance().CancelCaptureAndCloseReader(this.OnCaptured);
            }
        }

        private void loadListView()
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("eid", events.SelectedValue, "=", "Event ID"));
            prms.Add(new SQLParameter("deleted", 0));
            IDataReader reader = db.Select("SELECT `Attendance ID` AS `Record Number`, `ID Number`, Name, time_in, time_out, remarks, Notes FROM view_attendance1", prms);
            ListViewManager.populateListView(listView1, reader, true);
            populateExemptions(DataTypeManager.ParseDataToString(events.SelectedValue));
        }

        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    if(listView1.SelectedItems.Count > 0)
                    {
                        if (listView1.FocusedItem.Bounds.Contains(e.Location))
                        {
                            contextMenuStrip1.Show(Cursor.Position);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void addNoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DatabaseTableView dtv = new DatabaseTableView("tbattendance", "remarks");
            SQLParameter id = new SQLParameter("id", listView1.SelectedItems[0].Name);
            EntryForm_AddNote ean = new EntryForm_AddNote(dtv, id);
            if(ean.ShowDialog() == DialogResult.OK)
            {
                loadListView();
            }
            ean.Dispose();
        }

        private void students_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                try
                {
                    SendMessage(FPReader.Action.RegisterStudent, students.SelectedValue);
                }
                catch (Exception err)
                {
                    Console.WriteLine("Error Message: " + err.Message + "\r\n\r\n" + err.StackTrace);
                    MessageBox.Show(err.Message);
                }
                students.SelectedIndex = -1;
            }
        }

        private void bCloseEvent_Click(object sender, EventArgs e)
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("event_id", events.SelectedValue));
            string s = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT status from tbevents", prms));
            if (s == "Cancelled")
            {
                MessageBox.Show("You can't close the attendance registration of a cancelled event.");
                return;
            }
            if (s == "Unbegun")
            {
                MessageBox.Show("You can't close the attendance registration of an unstarted event.");
                return;
            }
            if (s == "Closed")
            {
                MessageBox.Show("You can't close the attendance registration for this event. It is already closed.");
                return;
            }
            string prompt = "Are you sure you want to close the attendance registration for this event? You will no longer be able to register after closing it.";
            if (MessageBox.Show(prompt, "Close Event", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                prms.Clear();
                prms.Add(new SQLParameter("status", "Closed"));
                SQLParameter[] prmss = { new SQLParameter("event_id", events.SelectedValue) };
                if (db.Update("tbevents", prms, prmss))
                {
                    populateEvents();
                }
                else
                {
                    MessageBox.Show("Something went wrong while closing the event.");
                }
            }
        }
    }
}
