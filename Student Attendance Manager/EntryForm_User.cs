﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class EntryForm_User : Form
    {
        private MySQLDatabase db;

        public EntryForm_User()
        {
            InitializeComponent();
            db = DatabaseManager.GetInstance();
            IDataReader reader = db.Select("SELECT `ID Number` AS sysID, Name as Description FROM view_students WHERE status='Active'");
            FormManager.SetComboBoxDataSource(studentid, reader);
            type.SelectedIndex = 1;
            designation.SelectedIndex = 0;
        }

        #region form mouse events
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();
        private void menuPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void Label5_MouseHover(object sender, EventArgs e)
        {
            bClose.BackColor = Color.Firebrick;
        }

        private void BClose_MouseLeave(object sender, EventArgs e)
        {
            bClose.BackColor = Color.FromArgb(64, 64, 64);
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (designation.SelectedIndex == 0)
            {
                panelStudent.Visible = true;
                panelOthers.Visible = false;
                panelPhoto.Visible = false;
                this.Height = 350;
            }
            else
            {
                this.Height = 547;
                panelStudent.Visible = false;
                panelOthers.Visible = true;
                panelPhoto.Visible = true;
            }
        }

        private void bConfirm_Click(object sender, EventArgs e)
        {
            if(username.TextLength < 1)
            {
                MessageBox.Show("Please enter a username");
                return;
            }
            if(unameStatus.Text != "ok")
            {
                MessageBox.Show("There is an existing account for this username");
                username.Focus();
            }
            List<SQLParameter> prms = FormManager.getParameters(panelUserInfo1);
            string folderName = AppConfiguration.GetConfig("syslocation") + "Users\\Photo\\";
            string photoname = null;
            if (designation.SelectedIndex != 0)
            {
                if(name.TextLength < 1)
                {
                    MessageBox.Show("Please enter a name");
                    return;
                }
                prms.AddRange(FormManager.getParameters(panelOthers));
                if (pbUser.Tag != null)
                {
                    photoname = System.IO.Path.GetFileName(pbUser.Tag.ToString());
                    prms.Add(new SQLParameter("photo", folderName + photoname));
                }
            }
            else
            {
                prms.AddRange(FormManager.getParameters(panelStudent));
            }
            prms.Add(new SQLParameter("date_created", DateTime.Today));
            prms.Add(new SQLParameter("created_by", ActiveUser.userid));
            int userid;
            if ((userid = db.InsertScalar("tbusers", prms)) > 0)
            {
                
                bool flag = true;
                if (pbUser.Tag != null)
                {
                    flag = FileManager.SaveImageToLocalFolder(folderName, photoname, pbUser.Image);
                }
                if (flag)
                {
                    DialogResult = DialogResult.OK;
                    Close();
                }
            }
        }

        private void username_TextChanged(object sender, EventArgs e)
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("username", username.Text));
            if(DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT COUNT(*) FROM tbusers", prms)) > 0)
            {
                unameStatus.Text = "!";
                unameStatus.ForeColor = Color.Firebrick;
            }
            else
            {
                unameStatus.Text = "ok";
                unameStatus.ForeColor = Color.Green;
            }
        }

        private void bBrowse_Click(object sender, EventArgs e)
        {
            string photoDirectory = FileManager.SelectFile("Select a photo", "Image files|*.png;*.jpg;*.jpeg");
            if (photoDirectory != null)
            {
                pbUser.Load(photoDirectory);
                pbUser.Tag = photoDirectory;
            }
        }

        private void bCapture_Click(object sender, EventArgs e)
        {
            Form_CameraCapture fcc = new Form_CameraCapture(pbUser);
            if (fcc.ShowDialog() == DialogResult.OK)
            {
                pbUser.Image.Tag = "new";
            }
            fcc.Dispose();
        }
    }
}
