﻿using System;
using System.Globalization;

namespace Student_Attendance_Manager
{
    class DataTypeManager
    {
        private static TextInfo txtInfo = new CultureInfo("en-us", false).TextInfo;
        public static string ParseDataToString(object data, string header = null)
        {
            if (data == null)
            {
                return ("");
            }
            if (data.ToString() == "")
            {
                return null;
            }
            if (data is TimeSpan)
            {
                TimeSpan ts = TimeSpan.Parse(data.ToString());
                DateTime time = DateTime.Today.Add(ts);
                Console.WriteLine(data.ToString() + " is TimeSpan");
                return time.ToString("hh:mm tt");
            }
            if (data is DateTime)
            {
                Console.WriteLine(data.ToString() + " is DateTime");
                return (Convert.ToDateTime(data).ToString("MMM. dd, yyyy"));
            }
            if (data is Decimal)
            {
                return (Convert.ToDecimal(data).ToString("#,##0.00"));
            }
            if (data is Boolean)
            {
                return (((Boolean)data).ToString());
            }
            return (data.ToString());
        }

        public static int ParseDataToInt(object data)
        {
            try
            {
                return(Convert.ToInt32(data));
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return -1;
            }
        }

        public static DateTime ParseDataToDateTime(object data)
        {
            try
            {
                return Convert.ToDateTime(data);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return new DateTime(1, 1, 1);
            }
        }

        public static DateTime ParseDataToTime(object data)
        {
            try
            {
                TimeSpan ts = TimeSpan.Parse(data.ToString());
                return DateTime.Today.Add(ts);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return new DateTime(1, 1, 1);
            }
        }
        public static DateTime ParseDataToYear(object data)
        {
            try
            {
                return new DateTime(Convert.ToInt32(data), 1, 1);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return new DateTime(1, 1, 1);
            }
        }

        public static bool ParseDataToBool(object data)
        {
            try
            {
                return Convert.ToBoolean(data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }

        public static Decimal ParseDataToDecimal(object data)
        {
            try
            {
                return Convert.ToDecimal(data);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return -1;
            }
        }

        public static string ToTitleCase(string words)
        {
            return (txtInfo.ToTitleCase(words));
        }
    }
}
