﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Student_Attendance_Manager
{
    class FormatManager
    {
        public static bool isValidMobileNumber(string number)
        {
            if(!number.StartsWith("+639"))
            {
                return false;
            }
            if(number.Length != 13)
            {
                return false;
            }
            return true;
        }

        public static bool isValidEmailAddress(string emailAddress)
        {
            Regex regex = new Regex(@"^\s*[\w\-\+_']+(\.[\w\-\+_']+)*\@[A-Za-z0-9]([\w\.-]*[A-Za-z0-9])?\.[A-Za-z][A-Za-z\.]*[A-Za-z]$");
            return regex.IsMatch(emailAddress);
            /*try
            {
                if(emailAddress.Contains(" "))
                {
                    return false;
                }
                string[] parts = emailAddress.Split('@');
                if(parts[0].Length < 1)
                {
                    return false;
                }
                string[] domainParts = parts[1].Split('.');
                if(domainParts[0].Length < 1)
                {
                    return false;
                }
                if(domainParts[1].Length < 1)
                {
                    return false;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
            return true;*/
        }

        private static bool isAlphaNumbericOnly(string inputString)
        {
            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
            return regexItem.IsMatch(inputString);
        }
    }
}
