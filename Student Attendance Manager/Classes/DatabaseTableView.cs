﻿namespace Student_Attendance_Manager
{
    public class DatabaseTableView
    {
        private string name;
        private string searchColumn;
        private string dateColumn;

        public string Name
        {
            get { return name; }
        }

        public string SearchColumn
        {
            get { return searchColumn; }
        }

        public string DateColumn
        {
            get { return dateColumn; }
        }

        public DatabaseTableView(string name, string searchColumn, string dateColumn = null)
        {
            this.name = name;
            this.searchColumn = searchColumn;
            this.dateColumn = dateColumn;
        }
    }
}
