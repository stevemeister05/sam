﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public interface SubForm
    {
        ListView Listview
        {
            set;
        }

        void ClearData();
    }
}
