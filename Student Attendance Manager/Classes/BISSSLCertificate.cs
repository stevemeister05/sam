﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Attendance_Manager
{
    class Student_Attendance_ManagerSSLCertificate
    {
        private string certificateFilePath, certificatePassword;

        public Student_Attendance_ManagerSSLCertificate(string certficateFilePath, string certificatePassword=null)
        {
            certficateFilePath = this.certificateFilePath;
            certificatePassword = this.certificatePassword;
        }

        public string CertificateFilePath
        {
            get { return certificateFilePath; }
        }

        public string CertificatePassword
        {
            get { return certificatePassword; }
        }
    }
}
