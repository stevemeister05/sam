﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Attendance_Manager
{
    class FPReaderManager
    {
        private static FPReader reader;
        
        public static FPReader GetInstance(bool newInstance = false)
        {
            if(reader == null || newInstance)
            {
                reader = new FPReader();
            }
            return reader;
        }
    }
}
