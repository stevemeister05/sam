﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Attendance_Manager
{
    class ActiveUser
    {
        //change
        public static int userid = -1;
        public static string name;
        public static string type;
        public static string photo;

        public static void Logout()
        {
            userid = -1;
            name = null;
            photo = null;
            type = null;
        }

        public static bool IsLogged()
        {
            if(userid > -1)
            {
                return true;
            }
            return false;
        }
    }
}
