﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;

namespace Student_Attendance_Manager
{
    class FileManager
    {
        /*public static async void uploadImageAsync(string imagePath, string imageName)
        {
            FileStream fileStream = null;
            try
            {
                string projectName = "afccor";
                var clientSecrets = new ClientSecrets();
                clientSecrets.ClientId = "968167537497-2foju9o90852mk3c2gbrpnmdep67l71r.apps.googleusercontent.com";
                clientSecrets.ClientSecret = "nWf1mpkq4FDOQor5Dv10Ffvf";
                var scopes = new[] { @"https://www.googleapis.com/auth/devstorage.full_control" };
                var cts = new CancellationTokenSource();
                var userCredential = await GoogleWebAuthorizationBroker.AuthorizeAsync(clientSecrets, scopes, "thakiraitsolutions@gmail.com", cts.Token);
                await userCredential.RefreshTokenAsync(cts.Token);
                var service = new Google.Apis.Storage.v1.StorageService();
                var bucketsQuery = service.Buckets.List(projectName);
                bucketsQuery.OauthToken = userCredential.Token.AccessToken;
                var buckets = bucketsQuery.Execute();
                var bucketToUpload = buckets.Items.FirstOrDefault().Name;
                var newObject = new Google.Apis.Storage.v1.Data.Object()
                {
                    Bucket = bucketToUpload,
                    Name = "Photo/" + imageName
                };
            
                fileStream = new FileStream(imagePath, FileMode.Open);
                var uploadRequest = new Google.Apis.Storage.v1.ObjectsResource.InsertMediaUpload(service, newObject,
                bucketToUpload, fileStream, "image/jpg");
                uploadRequest.OauthToken = userCredential.Token.AccessToken;
                await uploadRequest.UploadAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("Something went wrong while uploading photo. A default photo will be set as this customer's photo. To upload a new photo, edit this customer.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                if (fileStream != null)
                {
                    fileStream.Dispose();
                }
            }
        }*/

        public static string SelectFile(string title, string filter, bool fileNameOnly = false)
        {
            try
            {

                OpenFileDialog theDialog = new OpenFileDialog();
                theDialog.Title = title;
                theDialog.Filter = filter;
                if (theDialog.ShowDialog() == DialogResult.OK)
                {
                    if (!fileNameOnly)
                    {
                        return (theDialog.FileName.ToString());
                    }
                    return System.IO.Path.GetFileName(theDialog.FileName);
                }
            }
            catch (Exception err)
            {
                Console.Write("Error message: " + err.Message);
                Console.Write(err.StackTrace);
            }

            return null;
        }

        public static string SelectedFolder(string title)
        {
            try
            {
                FolderBrowserDialog theDialog = new FolderBrowserDialog();
                theDialog.Description = title;
                if (theDialog.ShowDialog() == DialogResult.OK)
                {
                    return theDialog.SelectedPath;
                }
            }
            catch (Exception err)
            {
                Console.Write("Error message: " + err.Message);
                Console.Write(err.StackTrace);
            }

            return null;
        }

        public static string ReadFromFile(string filePath)
        {
            try
            {
                return File.ReadAllText(filePath);
            }
            catch (Exception err)
            {
                Console.WriteLine(err.StackTrace);
                return null;
            }
        }

        public static bool DeleteFile(string filePath)
        {
            try
            {
                File.Delete(filePath);
                return true;
            }
            catch (Exception err)
            {
                Console.WriteLine(err.StackTrace);
                return false;
            }
        }

        public static bool DeleteFolder(string folderPath, bool deleteFiles = false)
        {
            try
            {
                Directory.Delete(folderPath, true);
                return true;
            }
            catch(Exception e)
            {
                Console.WriteLine("Error Message: " + e.Message + "\r\n\r\n" + e.StackTrace);
                return false;
            }
        }

        public static bool WriteToFile(string folderPath, string fileName, string text = null)
        {
            try
            {
                if (folderPath == null)
                {
                    folderPath = "D:\\";
                }
                string filePath = folderPath + (folderPath.ElementAt(folderPath.Length - 1) == '\\' ? "" : "\\") + fileName;
                System.IO.Directory.CreateDirectory(folderPath);
                if (text != null)
                {
                    File.WriteAllText(filePath, text);
                }
                return true;
            }
            catch (Exception err)
            {
                Console.WriteLine(err.StackTrace);
                MessageBox.Show("Something went wrong while creating the file.\r\n\r\nError message:\t" + err.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static bool SaveImageToLocalFolder(string folderPath, string fileName, Image image)
        {
            string filePath = null;
            try
            {
                if (folderPath == null)
                {
                    folderPath = "D:\\";
                }
                filePath = folderPath + (folderPath.ElementAt(folderPath.Length - 1) == '\\' ? "" : "\\") + fileName;
                System.IO.Directory.CreateDirectory(folderPath);
                if (image != null)
                {
                    image.Save(filePath);
                }
                return true;
            }
            catch (Exception err)
            {
                Console.WriteLine("File Path: " + filePath);
                Console.WriteLine("Error Message: " + err.Message);
                Console.WriteLine(err.StackTrace);
                MessageBox.Show("Something went wrong while saving the image.\r\n\r\nError message:\t" + err.Message, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public static bool EmptyDirectory(string directoryPath)
        {
            DirectoryInfo directory;
            try
            {
                directory = new DirectoryInfo(directoryPath);
                foreach (System.IO.FileInfo file in directory.GetFiles()) file.Delete();
                foreach (System.IO.DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
            }
            catch (Exception err)
            {
                Console.WriteLine("Error message: " + err.Message);
                Console.WriteLine(err.StackTrace);
                return false;
            }
            return true;
        }

        public static bool CopyFile(string filePath, string destinationFilePath)
        {
            try
            {
                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(destinationFilePath));
                if(File.Exists(destinationFilePath))
                {
                    File.Delete(destinationFilePath);
                }
                File.Copy(filePath, destinationFilePath);
            }
            catch (Exception err)
            {
                Console.WriteLine("Error message: " + err.Message);
                Console.WriteLine(err.StackTrace);
                return false;
            }
            return true;
        }

        public static void LoadImageToPictureBox(string photoPath, PictureBox pb)
        {
            try
            {
                pb.Load(photoPath);
                pb.Image.Tag = null;
                pb.Tag = photoPath;
            }
            catch (Exception err)
            {
                Console.WriteLine("Error message: " + err.Message);
                Console.WriteLine(err.StackTrace);
                pb.Tag = null;
            }
        }

        public static void exportToExcelFile(string filename, bool open, params DataTable[] datatables)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filename));
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(filename, SpreadsheetDocumentType.Workbook))
                {
                    // Add a WorkbookPart to the document.
                    WorkbookPart workbookpart = spreadsheetDocument.AddWorkbookPart();
                    workbookpart.Workbook = new Workbook();
                    WorkbookStylesPart stylesheet = AddStyleSheet(spreadsheetDocument);
                    // Add a WorksheetPart to the WorkbookPart.
                    Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());
                    UInt32Value id = 1;
                    foreach (DataTable dataTable in datatables)
                    {
                        // Add Sheets to the Workbook.
                        // Append a new worksheet and associate it with the workbook.
                        WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                        SheetData sheetData = new SheetData();
                        worksheetPart.Worksheet = new Worksheet();
                        Columns columns = new Columns();
                        Column column = new Column() { Min = 1, Max = 20, Width = 30, CustomWidth = true };
                        Column column1 = new Column() { Min = 1, Max = 30, Width = 50, CustomWidth = true };
                        Column column2 = new Column() { Min = 1, Max = 30, Width = 20, CustomWidth = true };
                        columns.Append(column);
                        columns.Append(column1);
                        columns.Append(column2);
                        SheetFormatProperties sheetFormatProperties = new SheetFormatProperties()
                        {
                            DefaultColumnWidth = 15,
                            DefaultRowHeight = 15D,
                        };
                        worksheetPart.Worksheet.Append(sheetFormatProperties);
                        worksheetPart.Worksheet.Append(columns);
                        worksheetPart.Worksheet.Append(sheetData);
                        Sheet sheet = new Sheet()
                        {
                            Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
                            SheetId = id++,
                            Name = dataTable.TableName
                        };
                        UInt32Value x = 1;
                        Row r = new Row() { RowIndex = x };
                        char chr = 'A';
                        for (int i = 0; i < dataTable.Columns.Count; i++)
                        {
                            string tmp = (chr++) + x.ToString();
                            Cell headerr = new Cell() { CellReference = tmp, CellValue = new CellValue(dataTable.Columns[i].ColumnName), DataType = CellValues.String, StyleIndex = Convert.ToUInt32(1) };
                            r.Append(headerr);
                        }
                        sheetData.Append(r);
                        x = x + 1;
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            Row row = new Row() { RowIndex = x };
                            chr = 'A';
                            for (int j = 0; j < dataTable.Columns.Count; j++)
                            {
                                string tmp = (chr++) + x.ToString();
                                bool isNum = false;
                                Cell headerr;
                                try
                                {
                                    Convert.ToDecimal(dataTable.Rows[i][j]);
                                    isNum = true;
                                }
                                catch(Exception e)
                                {
                                    Console.WriteLine(e.StackTrace);
                                }
                                string val = DataTypeManager.ParseDataToString(dataTable.Rows[i][j]);
                                if (isNum)
                                {
                                    headerr = new Cell() { CellReference = tmp, CellValue = new CellValue(val), DataType = CellValues.String, StyleIndex = Convert.ToUInt32(1) };
                                }
                                else
                                {
                                    headerr = new Cell() { CellReference = tmp, CellValue = new CellValue(val), DataType = CellValues.String };
                                }
                                row.Append(headerr);
                            }
                            sheetData.Append(row);
                            x = x + 1;
                        }
                        sheets.Append(sheet);
                    }

                    workbookpart.Workbook.Save();
                    spreadsheetDocument.Close();
                }
                if (open)
                {
                    System.Diagnostics.Process.Start(filename);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show(e.Message, "File Manager Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private static WorkbookStylesPart AddStyleSheet(SpreadsheetDocument spreadsheet)
        {
            WorkbookStylesPart stylesheet = spreadsheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
            Stylesheet workbookstylesheet = new Stylesheet();
            // <Fonts>
            DocumentFormat.OpenXml.Spreadsheet.Font font0 = new DocumentFormat.OpenXml.Spreadsheet.Font();            // Default font
            DocumentFormat.OpenXml.Spreadsheet.Font font1 = new DocumentFormat.OpenXml.Spreadsheet.Font();            // Default font
            Fonts fonts = new Fonts();          // <APPENDING Fonts>
            fonts.Append(font0);
            // <Fills>
            Fill fill0 = new Fill();            // Default fill
            Fills fills = new Fills();          // <APPENDING Fills>
            fills.Append(fill0);
            // <Borders>
            Border border0 = new Border();      // Defualt border
            Borders borders = new Borders();    // <APPENDING Borders>
            borders.Append(border0);
            // <CellFormats>
            CellFormat cellformat0 = new CellFormat()   // Default style : Mandatory
            {
                FontId = 0,
                FillId = 0,
                BorderId = 0,
                Alignment = new Alignment() { WrapText = true, Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Center }
            };
            // <APPENDING CellFormats>
            CellFormat cellformat1 = new CellFormat(new Alignment() { WrapText = true, Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center });
            CellFormat cellformat2 = new CellFormat(new Alignment() { WrapText = true, Horizontal = HorizontalAlignmentValues.Right, Vertical = VerticalAlignmentValues.Center });
            // Style with textwrap set
            CellFormats cellformats = new CellFormats();
            cellformats.Append(cellformat0);
            cellformats.Append(cellformat1);
            cellformats.Append(cellformat2);
            // Append FONTS, FILLS , BORDERS & CellFormats to stylesheet <Preserve the ORDER>
            workbookstylesheet.Append(fonts);
            workbookstylesheet.Append(fills);
            workbookstylesheet.Append(borders);
            workbookstylesheet.Append(cellformats);
            // Finalize
            stylesheet.Stylesheet = workbookstylesheet;
            stylesheet.Stylesheet.Save();
            return stylesheet;
        }
    }
}
