﻿using DPUruNet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Attendance_Manager
{
    public class Fingerprint
    {
        private int position;
        private Fmd fingerPrint;
        private Image image;
        private Fid fid;

        public int Position
        {
            get { return position; }
        }

        public Fid Fid
        {
            set
            {
                fid = value;
                image = SetImage(fid);
            }
        }

        public Fmd FMDValue
        {
            get { return fingerPrint; }
        }

        public Image Image
        {
            get
            {
                return image;
            }
        }

        public Fingerprint(int position, Fmd fingerPrint, Fid fid = null)
        {
            this.position = position;
            this.fingerPrint = fingerPrint;
            this.fid = fid;
            image = SetImage(fid);
        }

        public Fingerprint(int position, Fmd fingerPrint, Image image)
        {
            this.position = position;
            this.fingerPrint = fingerPrint;
            this.image = image;
        }

        public Image SetImage(Fid fpImageView)
        {
            Image img = null;
            if(fpImageView != null)
            {
                foreach (Fid.Fiv fiv in fid.Views)
                {
                    img = FPReaderManager.GetInstance().CreateBitmap(fiv.RawImage, fiv.Width, fiv.Height);
                }
            }
            return img;
        }

        public string ToXML()
        {
            return (Fmd.SerializeXml(fingerPrint));
        }

        public bool Matches(Fmd fingerPrint)
        {
            CompareResult compareResult = Comparison.Compare(this.fingerPrint, 0, fingerPrint, 0);
            if (compareResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
            {
                return false;
            }
            return compareResult.Score < (0x7fffffff / 100000) ? true : false;
        }

        public bool Matches(Fmd[] fingerPrints)
        {
            int thresholdScore = 0x7fffffff * 1 / 100000;
            IdentifyResult identifyResult = Comparison.Identify(fingerPrint, 0, fingerPrints, thresholdScore, 2);
            if (identifyResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
            {
                return false;
            }
            return identifyResult.Indexes.Length > 0 ? true : false;
        }

        public Image SetImageFromFile(string filePath)
        {
            try
            {
                return (image = Image.FromFile(filePath));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            return null;
        }

        public bool Matches(string fingerPrintXML)
        {
            try
            {
                return Matches(Fmd.DeserializeXml(fingerPrintXML));
            }
            catch (Exception e)
            {
                Console.Write(e.StackTrace);
            }
            return false;
        }

        public string FingerName
        {
            get
            {
                string fname = null;
                switch(position)
                {
                    case 0:
                        fname = "Right Thumb";
                        break;
                    case 1:
                        fname = "Right Index Finger";
                        break;
                    case 2:
                        fname = "Right Middle Finger";
                        break;
                    case 3:
                        fname = "Right Ring Finger";
                        break;
                    case 4:
                        fname = "Right Little Finger";
                        break;
                    case 5:
                        fname = "Left Thumb";
                        break;
                    case 6:
                        fname = "Left Index Finger";
                        break;
                    case 7:
                        fname = "Left Middle Finger";
                        break;
                    case 8:
                        fname = "Left Ring Finger";
                        break;
                    case 9:
                        fname = "Left Little Finger";
                        break;
                }
                return fname;
            }
        }
        
        public string FidToXML()
        {
            return (fid == null ? "" : Fid.SerializeXml(fid));
        }
    }
}
