﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Drawing;

namespace Student_Attendance_Manager
{
    class ListViewManager
    {
        public static void populateListView(ListView listView1, IDataReader reader, bool firstLoad)
        {
            try
            {
                if (reader != null)
                {
                    listView1.ListViewItemSorter = null;
                    if (firstLoad)
                    {
                        listView1.Clear();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string hd = reader.GetName(i);
                            string hddis = hd;
                            if(hd.Contains('_'))
                            {
                                hddis = hd.Replace("_", " ");
                            }
                            listView1.Columns.Add(hd, DataTypeManager.ToTitleCase(hddis));
                        }
                    }
                    else
                    {
                        listView1.Items.Clear();
                    }
                    int x = 0;
                    while (reader.Read())
                    {
                        listView1.Items.Add(DataTypeManager.ParseDataToString(reader.GetValue(0)));
                        listView1.Items[x].Name = reader.GetString(0);
                        for (int i = 1; i < reader.FieldCount; i++)
                        {
                            listView1.Items[x].SubItems.Add(DataTypeManager.ParseDataToString(reader.GetValue(i)));
                        }
                        /*ColumnHeader ch;
                        if ((ch = listView1.Columns["deleted"]) != null || (ch = listView1.Columns["cancelled"]) != null)
                        {
                            if(Convert.ToBoolean(listView1.Items[x].SubItems[ch.Index].Text))
                            {
                                listView1.Items[x].ForeColor = Color.IndianRed;
                            }
                        }*/
                        x++;
                    }
                    reader.Close();
                    listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                    /*int width = 0;
                    foreach(ColumnHeader ch in listView1.Columns)
                    {
                        width += ch.Width;
                    }
                    listView1.MaximumSize = new Size(width, 5000);*/
                }
            }
            catch(Exception e)
            {
                reader.Close();
                Console.WriteLine(e.StackTrace);
                MessageBox.Show(e.Message);
            }
        }

        public static bool onRightClick(ListView lw, ContextMenuStrip cms, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    if (lw.FocusedItem.Bounds.Contains(e.Location))
                    {
                        cms.Show(Cursor.Position);
                        return true;
                    }
                }
                return false;
            }
            catch(Exception err)
            {
                MessageBox.Show(err.Message);
                return false;
            }
        }

        public static void sortListview(ListView listView1, ref ColumnHeader SortingColumn, ColumnClickEventArgs e)
        {
            ColumnHeader new_sorting_column = listView1.Columns[e.Column];
            // Figure out the new sorting order.
            System.Windows.Forms.SortOrder sort_order;
            if (SortingColumn == null)
            {
                // New column. Sort ascending.
                sort_order = SortOrder.Ascending;
            }
            else
            {
                // See if this is the same column.
                if (new_sorting_column == SortingColumn)
                {
                    // Same column. Switch the sort order.
                    if (SortingColumn.Text.StartsWith("> "))
                    {
                        sort_order = SortOrder.Descending;
                    }
                    else
                    {
                        sort_order = SortOrder.Ascending;
                    }
                }
                else
                {
                    // New column. Sort ascending.
                    sort_order = SortOrder.Ascending;
                    
                }
                SortingColumn.Text = SortingColumn.Text.Replace("> ", "").Replace("< ", "");

                // Remove the old sort indicator.
            }

            // Display the new sort order.
            SortingColumn = new_sorting_column;
            if (sort_order == SortOrder.Ascending)
            {
                SortingColumn.Text = "> " + SortingColumn.Text;
            }
            else
            {
                SortingColumn.Text = "< " + SortingColumn.Text;
            }

            // Create a comparer.
            listView1.ListViewItemSorter =
                new ListViewComparer(e.Column, sort_order);

            // Sort.
            listView1.Sort();
        }

        public static string getColumnSubTotal(ListView listview, int columnIndex)
        {
            Decimal val = 0;
            Decimal temp;
            int errCount = 0;
            bool flg = false;
            for(int i = 0; i < listview.Items.Count; i++)
            {
                if(Decimal.TryParse(listview.Items[i].SubItems[columnIndex].Text, out temp))
                {
                    val += temp;
                }
                else
                {
                    errCount++;
                }
            }
            if(errCount > 0 && val == 0)
            {
                flg = true;
            }
            return flg ? "" : DataTypeManager.ParseDataToString(val);
        }
    }
}
