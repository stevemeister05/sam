﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    class ListViewSorter
    {
        private ListView listView;
        private ColumnHeader SortingColumn = null;

        public ListViewSorter(ListView listView)
        {
            this.listView = listView;
            this.listView.ColumnClick += listView1_ColumnClick;
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ListViewManager.sortListview(listView, ref SortingColumn, e);
        }
    }
}
