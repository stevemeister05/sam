﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class EntryForm_Settings : Form
    {
        private MySQLDatabase db;
        public EntryForm_Settings(int index = 0, bool showSchoolSettings = true)
        {
            InitializeComponent();
            PopulateFromConfigFile(panelDatabase);
            PopulateFromConfigFile(panelGeneralSettings);
            db = DatabaseManager.GetInstance();
            if (showSchoolSettings)
            {
                IDataReader reader = db.Select("SELECT * FROM tbappdefaults");
                FormManager.populateFields(panelSchoolSettings, reader);
            }
            else
            {
                panelSchoolSettings.Visible = false;
                tabControl1.Controls.Remove(tabPage3);
            }
            tabControl1.SelectedIndex = index;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            MySQLDatabase testdb = new MySQLDatabase(dbserver.Text, dbmain.Text, db_uid.Text, db_password.Text);
            if (testdb.OpenConnection())
            {
                testresult.Text = "Ok";
                testresult.ForeColor = Color.Green;
            }
            else
            {
                testresult.Text = "Failed";
                testresult.ForeColor = Color.Firebrick;
            }
            this.Cursor = Cursors.Default;
        }

        public void PopulateFromConfigFile(Control control)
        {
            string encryptionkey = AppConfiguration.GetConfig("encryptionkey");
            foreach (Control c in control.Controls)
            {
                if(c is TextBox)
                {
                    string setting = AppConfiguration.GetConfig(c.Name);
                    if (c.Tag != null && c.Tag.ToString() == "encrypted")
                    {
                        c.Text = HashComputer.Encryptor.DecryptData(setting, encryptionkey);
                    }
                    else
                    {
                        c.Text = setting;
                    }
                }
            }
        }

        public void SaveToConfigFile(Control control)
        {
            string encryptionkey = AppConfiguration.GetConfig("encryptionkey");
            foreach (Control c in control.Controls)
            {
                if (c is TextBox)
                {
                    string setting = c.Text;
                    if (c.Tag != null && c.Tag.ToString() == "encrypted")
                    {
                        setting = HashComputer.Encryptor.EncryptData(setting, encryptionkey);
                    }
                    else
                    {
                        c.Text = setting;
                    }
                    AppConfiguration.SetConfig(c.Name, setting);
                }
            }
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

       

        private void bConfirm_Click(object sender, EventArgs e)
        {
            SaveToConfigFile(panelDatabase);
            SaveToConfigFile(panelGeneralSettings);
            db = DatabaseManager.GetInstance(true);
            List<SQLParameter> prms = FormManager.getParameters(panelSchoolSettings);
            SQLParameter[] prms1 = { new SQLParameter("id", 1) };
            bool flag = true;
            if(panelSchoolSettings.Visible == true)
            {
                if (school_logo.Tag != null)
                {
                    flag = FileManager.CopyFile(school_logo.Tag.ToString(), AppConfiguration.GetConfig("syslocation") + @"defaults\" + school_logo.Text);
                }
                if (flag)
                {
                    if (club_logo.Tag != null)
                    {
                        flag = FileManager.CopyFile(club_logo.Tag.ToString(), AppConfiguration.GetConfig("syslocation") + @"defaults\" + club_logo.Text);
                        if (!flag)
                        {
                            MessageBox.Show("Something went wrong while saving club logo");
                            club_logo.Focus();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Something went wrong while saving school logo.");
                    school_logo.Focus();
                }
                if (flag && db.Update("tbappdefaults", prms, prms1))
                {
                    DialogResult = DialogResult.OK;
                    Close();
                }
            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }
        #region ControlBoxEvents
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();
        private void menuPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void Label5_MouseHover(object sender, EventArgs e)
        {
            bClose.BackColor = Color.Firebrick;
        }

        private void BClose_MouseLeave(object sender, EventArgs e)
        {
            bClose.BackColor = Color.FromArgb(64, 64, 64);
        }
        #endregion

        private void no_of_semesters_ValueChanged(object sender, EventArgs e)
        {
            semester.Maximum = no_of_semesters.Value;
        }

        private void browseSL_Click(object sender, EventArgs e)
        {
            string temp = FileManager.SelectFile("Select School Logo", "Images|*.jpg;*.png;*.jpeg");
            school_logo.Text = temp != null ? System.IO.Path.GetFileName(temp) : school_logo.Text;
            school_logo.Tag = temp;
        }

        private void browseCL_Click(object sender, EventArgs e)
        {
            string temp = FileManager.SelectFile("Select Club Logo", "Images|*.jpg;*.png;*.jpeg");
            club_logo.Text = temp != null ? System.IO.Path.GetFileName(temp) : club_logo.Text;
            club_logo.Tag = temp;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string temp = FileManager.SelectedFolder("Select App Files Location");
            syslocation.Text = temp != null ? temp : syslocation.Text;
            syslocation.Tag = temp;
        }
    }
}
