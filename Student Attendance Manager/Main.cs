﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class Main : Form
    {
        MySQLDatabase db;
        public Main()
        {
            InitializeComponent();
            init();
        }

        private void init()
        {
            db = DatabaseManager.GetInstance();
            try
            {
                bUser.Text = ActiveUser.name;
                applabel.Text = AppConfiguration.GetConfig("appcaption");
                clubName.Text = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT club_name FROM tbappdefaults"));
                schoolName.Text = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT school_name FROM tbappdefaults"));
                schoolyear.Text = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT school_year FROM tbappdefaults"));
                schoolyear.Text = "S.Y. " + DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT school_year FROM tbappdefaults"));
                sem.Text = formatNumber(DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT semester FROM tbappdefaults"))) + " Sem";
                clublogo.Load(AppConfiguration.GetConfig("syslocation") + "defaults\\" + DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT club_logo FROM tbappdefaults")));
                bUser.Image = OvalImage(Image.FromFile(ActiveUser.photo));
            }
            catch(Exception err)
            {
                Console.WriteLine("Error message: " + err.Message + "\r\n\r\n" + err.StackTrace);
            }
            if(ActiveUser.type.ToLower() == "regular")
            {
                settingsToolStripMenuItem.Visible = false;
                bUsers.Visible = false;
            }
        }

        public static Image OvalImage(Image img)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height);
            using (GraphicsPath gp = new GraphicsPath())
            {
                gp.AddEllipse(0, 0, img.Width, img.Height);
                using (Graphics gr = Graphics.FromImage(bmp))
                {
                    gr.SetClip(gp);
                    gr.DrawImage(img, Point.Empty);
                }
            }
            return bmp;
        }

        private string formatNumber(int num)
        {
            switch(num)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }
        }

        #region Move GUI
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private void Form_Main_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        #endregion

        private void bExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            foreach (Control child in content.Controls)
            {
                content.Controls.Remove(child);
                child.Dispose();
            }
            ToolStripMenuItem item = (ToolStripMenuItem)e.ClickedItem;
            this.Cursor = Cursors.WaitCursor;
            UserControl uc = null;
            switch (e.ClickedItem.Name)
            {
                case "bAttendance":
                    HighlightMenu(item);
                    uc = new Control_Attendance();
                    break;
                case "bStudents":
                    HighlightMenu(item);
                    uc = new Control_Students(new DatabaseTableView("view_students", "Name"));
                    break;
                case "bEvents":
                    uc = new Control_Events(new DatabaseTableView("tbevents", "title", "date"));
                    HighlightMenu(item);
                    break;
                case "bOfficers":
                    //uc = new Control_Users(new DatabaseTableView("view_users", "Name", "Date Created"));
                    HighlightMenu(item);
                    break;
                case "bUsers":
                    uc = new Control_Users(new DatabaseTableView("view_users", "Name", "Date Created"));
                    HighlightMenu(item);
                    break;
                case "bExit":
                    Close();
                    break;
                default:
                    HighlightMenu(item);
                    break;
            }
            if (uc != null)
            {
                uc.Dock = DockStyle.Fill;
                content.Controls.Add(uc);
            }
            this.Cursor = Cursors.Default;
        }

        private void HighlightMenu(ToolStripMenuItem menuitem)
        {
            menuitem.BackColor = Color.FromArgb(61, 84, 118);
            foreach (ToolStripMenuItem c in menuStrip1.Items)
            {
                if (c != menuitem)
                {
                    c.BackColor = Color.FromArgb(53, 64, 82);
                }
            }
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EntryForm_Settings es = new EntryForm_Settings();
            if(es.ShowDialog() == DialogResult.OK)
            {
                init();
            }
            es.Dispose();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FPReaderManager.GetInstance().CancelCaptureAndCloseReader();
            ActiveUser.Logout();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void bChangePassword_Click(object sender, EventArgs e)
        {
            EntryForm_ChangePassword efcp = new EntryForm_ChangePassword();
            efcp.ShowDialog();
            efcp.Dispose();
        }
    }
}
