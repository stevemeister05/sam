﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class Control_Events : UserControl
    {
        private DatabaseTableView view;
        private MySQLDatabase db;
        public Control_Events(DatabaseTableView view = null)
        {
            InitializeComponent();
            new ListViewSorter(listView1);
            new ListViewSorter(listView2);
            new ListViewSorter(listView3);
            db = DatabaseManager.GetInstance();
            this.view = view;
            if (this.view != null)
            {
                control_StandardControls1.View = view;
            }
            control_StandardControls1.TransactionName = "Events";
            control_StandardControls1.ListView = listView1;
            control_StandardControls1.SetButtonNewClick(buttonNew_Click);
            control_StandardControls1.SetButtonPrintClick(bPrint_Click);
            control_StandardControls1.StartDate = new DateTime(2019, 10, 1);
            if (ActiveUser.type.ToLower() != "regular")
            {
                control_StandardControls1.SetButtonEditClick(buttonEdit_Click);
                control_StandardControls1.SetButtonDeleteClick(buttonDelete_Click);
            }
            else
            {
                control_StandardControls1.EditButtonVisible = false;
                control_StandardControls1.DeleteButtonVisible = false;
            }
            control_StandardControls1.SetButtonExportClick(buttonExport_Click);
            loadComboBoxes();
        }

        public void loadComboBoxes()
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("status", "Cancelled", "!="));
            IDataReader reader = db.Select("SELECT school_year AS sysID, school_year AS Description FROM tbevents", prms, "GROUP BY school_year ORDER BY school_year DESC");
            FormManager.SetComboBoxDataSource(sy, reader);
            string syear = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT school_year FROM tbappdefaults"));
            sy.SelectedIndex = -1;
            for (int i = 0; i < sy.Items.Count; i++)
            {
                try
                {
                    DataRowView row = (DataRowView)(sy.Items[i]);
                    Console.WriteLine(syear + " - " + row[0]);
                    if (syear == DataTypeManager.ParseDataToString(row[0]))
                    {
                        sy.SelectedIndex = i;
                        break;
                    }
                }
                catch (Exception err)
                {
                    Console.WriteLine("Error Message: " + err.Message + "\r\n\r\n" + err.StackTrace);
                }
            }
            if(sy.Items.Count == 0)
            {
                sem.DataSource = null;
            }
            else
            {
                try
                {
                    control_StandardControls1.EndDate = DataTypeManager.ParseDataToDateTime(db.ScalarSelect("SELECT MAX(date) FROM tbevents WHERE status<>'Cancelled'"));
                }
                catch (Exception err)
                {
                    control_StandardControls1.EndDate = DateTime.Now;
                    Console.WriteLine("Error message: " + err.Message + "\r\n\r\n" + err.StackTrace);
                }
            }
        }

        private void loadListView()
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("semester", sem.Text));
            prms.Add(new SQLParameter("school_year", sy.Text));
            control_StandardControls1.loadListView(true, prms);
        }

        private bool isEventInPresentSemAndYear(string event_id)
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("event_id", event_id));
            string a = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT school_year FROM tbevents", prms));
            int b = DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT semester FROM tbevents", prms));
            if(a != DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT school_year FROM tbappdefaults")))
            {
                return false;
            }
            if(b != DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT semester FROM tbappdefaults")))
            {
                return false;
            }
            return true;
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            EntryForm_Event efs = new EntryForm_Event();
            if (efs.ShowDialog() == DialogResult.OK)
            {
                loadComboBoxes();
                //loadListView();
            }
            efs.Dispose();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select an event to edit");
                return;
            }
            if(!isEventInPresentSemAndYear(listView1.SelectedItems[0].Name))
            {
                MessageBox.Show("Can't modify this event because its school year and semester is not the same with the currently set school year and semester");
                return;
            }
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("event_id", listView1.SelectedItems[0].Name));
            prms.Add(new SQLParameter("deleted", 0));
            if (DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT COUNT(*) FROM tbattendance", prms)) > 0)
            {
                MessageBox.Show("Can't edit this event because it has started recording attendance.");
                return;
            }
            EntryForm_Event efs = new EntryForm_Event(listView1.SelectedItems[0].Name);
            if (efs.ShowDialog() == DialogResult.OK)
            {
                loadComboBoxes();
                //loadListView();
            }
            efs.Dispose();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select an item to delete.");
                return;
            }
            string eid = listView1.SelectedItems[0].Name;
            if (!isEventInPresentSemAndYear(listView1.SelectedItems[0].Name))
            {
                MessageBox.Show("Can't delete this event because its school year and semester is not the same with the currently set school year and semester");
                return;
            }
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("event_id", eid));
            int a = DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT COUNT(*) FROM tbattendance", prms));
            if (a != 0)
            {
                MessageBox.Show("You can't delete this event because it has existing attendance record/s.");
                return;
            }
            if (MessageBox.Show("Are you sure you want to delete this event?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (db.Delete("tbevents", prms))
                {
                    loadComboBoxes();
                    //loadListView();
                }
            }
        }
        private void buttonExport_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select an item to delete.");
                return;
            }
            string eid = listView1.SelectedItems[0].Name;
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("event_id", eid, "=", "Event ID"));
            prms.Add(new SQLParameter("deleted", 0));
            IDataReader reader = db.Select("SELECT `ID Number`, Name, time_in AS `Time in`, time_out AS `Time out`, Remarks FROM view_attendance", prms);
            DataTable dt = new DataTable();
            dt.Load(reader);
            dt.TableName = "Attendance";
            reader.Close();
            FileManager.exportToExcelFile(AppConfiguration.GetConfig("syslocation") + "Attendance\\Attendance-" + eid + ".xlsx", true, dt);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                bAddExemption.Enabled = true;
                try
                {
                    List<SQLParameter> prms = new List<SQLParameter>();
                    prms.Add(new SQLParameter("event_id", listView1.SelectedItems[0].Name));
                    string s = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT status from tbevents", prms));
                    if (s == "Closed")
                    {
                        bOpenEvent.Text = "Reopen Registration";
                        bOpenEvent.Enabled = true;
                        bCloseEvent.Enabled = false;
                    }
                    else if (s == "Cancelled")
                    {
                        bOpenEvent.Text = "Open Registration";
                        bOpenEvent.Enabled = false;
                        bCloseEvent.Enabled = false;
                    }
                    else if (s == "Open")
                    {
                        bOpenEvent.Text = "Open Registration";
                        bOpenEvent.Enabled = false;
                        bCloseEvent.Enabled = true;
                    }
                    else
                    {
                        bOpenEvent.Text = "Open Registration";
                        bOpenEvent.Enabled = true;
                        bCloseEvent.Enabled = false;
                    }
                    PopulateEventDetails(listView1.SelectedItems[0].Name);
                }
                catch (Exception err)
                {
                    PopulateEventDetails();
                    Console.WriteLine("Error Message: " + err.Message + "\r\n\r\n" + err.StackTrace);
                }
            }
            else
            {
                bAddExemption.Enabled = false;
                listView3.Items.Clear();
                listView2.Items.Clear();
                enrolledStudents.Clear();
                attendees.Clear();
                PopulateEventDetails();
            }
        }

        private void PopulateEventDetails(string id = null)
        {
            if (id != null)
            {
                List<SQLParameter> prms = new List<SQLParameter>();
                prms.Add(new SQLParameter("event_id", id));
                string statuss = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT status FROM tbevents", prms));
                if(statuss != "Cancelled")
                {
                    int es = DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT enrolled_students FROM tbevents", prms));
                    int exemptedd = DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT COUNT(student_id) FROM tbevent_exemptions", prms));
                    int expected = es - exemptedd;
                    Console.WriteLine(exemptedd.ToString());
                    if (statuss == "Closed")
                    {
                        stts.ForeColor = Color.Green;
                        prms.Add(new SQLParameter("deleted", 0));
                        int ps = DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT COUNT(student_id) FROM tbattendance", prms));
                        prms.Clear();
                        SQLParameter rparams = new SQLParameter("Remarks", "Late", "LIKE");
                        prms.Add(new SQLParameter("eid", id, "=", "Event ID"));
                        prms.Add(rparams);
                        int lcomers = DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT COUNT(`ID Number`) FROM view_attendance", prms));
                        prms.Remove(rparams);
                        prms.Add(new SQLParameter("remarks", "Early Out", "LIKE"));
                        int eouters = DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT COUNT(`ID Number`) FROM view_attendance", prms));
                        enrolledStudents.Text = (expected).ToString();
                        exempted.Text = exemptedd.ToString();
                        attendees.Text = ps.ToString();
                        absentees.Text = (expected - ps).ToString();
                        attendancerate.Text = DataTypeManager.ParseDataToString((Convert.ToDecimal((ps)) / expected * 100)) + " %";
                        latecomers.Text = lcomers.ToString();
                        earlyouters.Text = eouters.ToString();
                        if(ps <= 0)
                        {
                            punctualrate.Text = "n/a";
                        }
                        else
                        {
                            punctualrate.Text = DataTypeManager.ParseDataToString((Convert.ToDecimal(ps - lcomers) / ps * 100)) + " %";
                        }
                    }
                    else
                    {
                        ClearEventDetails("n/a");
                        enrolledStudents.Text = (expected).ToString();
                        exempted.Text = exemptedd.ToString();
                    }
                    prms.Clear();
                    prms.Add(new SQLParameter("eid", id, "=", "Event ID"));
                    prms.Add(new SQLParameter("deleted", 0));
                    IDataReader reader = db.Select("SELECT `Attendance ID` AS `Record Number`, `ID Number`, Name, time_in, time_out, remarks, Notes FROM view_attendance", prms);
                    ListViewManager.populateListView(listView2, reader, true);
                    prms.Clear();
                    populateExemptions(id);
                }
                else
                {
                    stts.ForeColor = Color.Firebrick;
                    bAddExemption.Enabled = false;
                    bRemoveExemptions.Enabled = false;
                    ClearEventDetails();
                }
                switch (statuss)
                {
                    case "Closed":
                        stts.ForeColor = Color.SteelBlue;
                        break;
                    case "Open":
                        stts.ForeColor = Color.Green;
                        break;
                    case "Unbegan":
                        stts.ForeColor = Color.Gray;
                        break;
                    default:
                        stts.ForeColor = Color.Firebrick;
                        break;
                }
                stts.Text = statuss.ToUpper();
            }
            else
            {
                ClearEventDetails();
            }
        }

        private void ClearEventDetails(string text = null)
        {
            enrolledStudents.Text = text;
            exempted.Text = text;
            attendees.Text = text;
            absentees.Text = text;
            attendancerate.Text = text;
            latecomers.Text = text;
            earlyouters.Text = text;
            punctualrate.Text = text;
        }

        private void populateExemptions(string id)
        {
            IDataReader reader = db.Select("SELECT tbevent_exemptions.student_id AS student_id, view_students.Name as Name, tbevent_exemptions.remarks FROM tbevent_exemptions JOIN view_students on (view_students.`ID Number` = tbevent_exemptions.student_id)  WHERE tbevent_exemptions.event_id='" + id + "'");
            ListViewManager.populateListView(listView3, reader, true);
        }

        private void bOpenEvent_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select an event to open");
                return;
            }
            if (!isEventInPresentSemAndYear(listView1.SelectedItems[0].Name))
            {
                MessageBox.Show("Can't modify this event because its school year and semester is not the same with the currently set school year and semester");
                return;
            }
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("event_id", listView1.SelectedItems[0].Name));
            string s = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT status from tbevents", prms));
            if (s == "Cancelled")
            {
                MessageBox.Show("You can't open the attendance registration for this event. This is already cancelled.");
                return;
            }
            string prompt = s == "Closed" ? "Are you sure you want to reopen the attendance registration for this event?" : "Are you sure you want to open the attendance registration this event?";
            if (MessageBox.Show(prompt, bOpenEvent.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                prms.Clear();
                prms.Add(new SQLParameter("status", "Open"));
                SQLParameter[] prmss = { new SQLParameter("event_id", listView1.SelectedItems[0].Name) };
                if (db.Update("tbevents", prms, prmss))
                {
                    loadListView();
                }
                else
                {
                    MessageBox.Show("Something went wrong while opening the attendance registration for this event.");
                }
            }
        }

        private void bCloseEvent_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select an event to close");
                return;
            }
            if (!isEventInPresentSemAndYear(listView1.SelectedItems[0].Name))
            {
                MessageBox.Show("Can't modify this event because its school year and semester is not the same with the currently set school year and semester");
                return;
            }
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("event_id", listView1.SelectedItems[0].Name));
            string s = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT status from tbevents", prms));
            //string s = listView1.SelectedItems[0].SubItems[listView1.Columns["status"].Index].Text;
            if (s == "Cancelled")
            {
                MessageBox.Show("You can't open the attendance registration of a cancelled event.");
                return;
            }
            if (s == "Unbegun")
            {
                MessageBox.Show("You can't close the attendance registration of an unstarted event.");
                return;
            }
            if (s == "Closed")
            {
                MessageBox.Show("You can't close the attendance registration for this event. This is already closed.");
                return;
            }
            string prompt = "Are you sure you want to close the attendance registration for this event? You will no longer be able to register after closing it.";
            if (MessageBox.Show(prompt, bOpenEvent.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                prms.Clear();
                prms.Add(new SQLParameter("status", "Closed"));
                SQLParameter[] prmss = { new SQLParameter("event_id", listView1.SelectedItems[0].Name) };
                if (db.Update("tbevents", prms, prmss))
                {
                    loadListView();
                }
                else
                {
                    MessageBox.Show("Something went wrong while closing the attendance registration for this event.");
                }
            }
        }

        private void bCancelEvent_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select an event to open");
                return;
            }
            if (!isEventInPresentSemAndYear(listView1.SelectedItems[0].Name))
            {
                MessageBox.Show("Can't modify this event because its school year and semester is not the same with the currently set school year and semester");
                return;
            }
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("event_id", listView1.SelectedItems[0].Name));
            string s = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT status from tbevents", prms));
            if (s == "Cancelled")
            {
                MessageBox.Show("You can't cancel an already cancelled event");
                return;
            }
            string prompt = "Are you sure you want to cancel this event? You will no longer be able to use this after closing.";
            if (MessageBox.Show(prompt, bOpenEvent.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                prms.Clear();
                prms.Add(new SQLParameter("status", "Cancelled"));
                SQLParameter[] prmss = { new SQLParameter("event_id", listView1.SelectedItems[0].Name) };
                if (db.Update("tbevents", prms, prmss))
                {
                    loadListView();
                    MessageBox.Show("Event was cancelled successfully.\r\n\r\n");
                }
                else
                {
                    MessageBox.Show("Something went wrong while closing the event.");
                }
            }
        }


        

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.TextLength > 0)
            {
                string[] tmp = tb.Text.Split('%');
                decimal rt = DataTypeManager.ParseDataToDecimal(tmp[0]);
                if (rt >= 80)
                {
                    tb.ForeColor = Color.Green;
                }
                else
                {
                    tb.ForeColor = Color.Red;
                }
                if(rt < 0)
                {
                    tb.ForeColor = Color.Black;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select an event.");
                return;
            }
            string id = listView1.SelectedItems[0].Name;
            if (!isEventInPresentSemAndYear(id))
            {
                MessageBox.Show("Can't modify this event because its school year and semester is not the same with the currently set school year and semester");
                return;
            }
            EntryForm_Exemption efe = new EntryForm_Exemption(Convert.ToInt32(id));
            if(efe.ShowDialog() == DialogResult.OK)
            {
                PopulateEventDetails(id);
            }
            efe.Dispose();
        }

        private void listView3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listView3.SelectedItems.Count < 1)
            {
                bRemoveExemptions.Enabled = false;
            }
            else
            {
                bRemoveExemptions.Enabled = true;
            }
        }

        private void bRemoveExemptions_Click(object sender, EventArgs e)
        {
            if(listView1.SelectedItems.Count < 1)
            {
                return;
            }
            int eid = Convert.ToInt32(listView1.SelectedItems[0].Name);
            if (!isEventInPresentSemAndYear(listView1.SelectedItems[0].Name))
            {
                MessageBox.Show("Can't modify this event because its school year and semester is not the same with the currently set school year and semester");
                return;
            }
            if (listView3.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select a student to remove from the exemption list");
                return;
            }
            if(MessageBox.Show("Are you sure you want to remove this student from the exemption list?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                foreach (ListViewItem lvi in listView3.SelectedItems)
                {
                    List<SQLParameter> prms = new List<SQLParameter>();
                    prms.Add(new SQLParameter("event_id", eid));
                    prms.Add(new SQLParameter("student_id", lvi.Name));
                    db.Delete("tbevent_exemptions", prms);
                }
                PopulateEventDetails(eid.ToString());
            }
        }

        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    if (listView3.FocusedItem.Bounds.Contains(e.Location))
                    {
                        contextMenuStrip1.Show(Cursor.Position);
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void addNoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(listView1.SelectedItems.Count < 1)
            {
                return;
            }
            if(!isEventInPresentSemAndYear(listView1.SelectedItems[0].Name))
            {
                MessageBox.Show("Can't modify this event because its school year and semester is not the same with the currently set school year and semester");
                return;
            }
            DatabaseTableView dtv = new DatabaseTableView("tbevent_exemptions", "remarks");
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("event_id", listView1.SelectedItems[0].Name));
            string statuss = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT status FROM tbevents", prms));
            if(statuss != "Cancelled")
            {
                prms.Add(new SQLParameter("student_id", listView3.SelectedItems[0].Name));
                SQLParameter exempid = new SQLParameter("id", db.ScalarSelect("SELECT id FROM tbevent_exemptions", prms));
                EntryForm_AddNote ean = new EntryForm_AddNote(dtv, exempid);
                if (ean.ShowDialog() == DialogResult.OK)
                {
                    populateExemptions(listView1.SelectedItems[0].Name);
                }
                ean.Dispose();
            }
        }

        private void sy_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("status", "Cancelled", "!="));
            prms.Add(new SQLParameter("school_year", sy.SelectedValue));
            IDataReader reader = db.Select("SELECT semester AS sysID, semester AS Description FROM tbevents", prms, "GROUP BY semester ORDER BY semester DESC");
            FormManager.SetComboBoxDataSource(sem, reader);
            string semester = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT semester FROM tbappdefaults"));
            sem.SelectedIndex = -1;
            for(int i = 0; i < sem.Items.Count; i++)
            {
                try
                {
                    DataRowView row = (DataRowView)(sem.Items[i]);
                    if (semester == DataTypeManager.ParseDataToString(row[0]))
                    {
                        sem.SelectedIndex = i;
                        break;
                    }
                }
                catch (Exception err)
                {
                    Console.WriteLine("Error Message: " + err.Message + "\r\n\r\n" + err.StackTrace);
                }
            }
        }

        private void sem_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadListView();
        }

        #region PRINTING
        private int cnt = 1;
        private IDataReader reader;
        private int totalPages = 1;
        private int currentPage = 1;
        PaperSize paperSize = new PaperSize();

        private void bPrint_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("Please select an event to print");
                return;
            }
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("event_id", listView1.SelectedItems[0].Name));
            string s = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT status from tbevents", prms));
            if (s == "Cancelled")
            {
                MessageBox.Show("You can't print a cancelled event's attendance record.");
                return;
            }
            if (s != "Closed")
            {
                MessageBox.Show("You have to close the event's attendance registration first in order to print.");
                return;
            }
            PrinterSettings ps = new PrinterSettings();
            IEnumerable<PaperSize> paperSizes = ps.PaperSizes.Cast<PaperSize>();
            paperSize = paperSizes.First<PaperSize>(size => size.Kind == PaperKind.Letter);
            printDocument1.DefaultPageSettings.PaperSize = paperSize;
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.ShowDialog();
        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            float currentY = 70;
            Font bfont = new Font(DefaultFont.FontFamily, 10, FontStyle.Bold);
            Font nfont = new Font(DefaultFont.FontFamily, 10);
            Pen pen = new Pen(Color.Black, 1);
            if (reader == null)
            {
                string clubName = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT club_name FROM tbappdefaults"));
                string schoolName = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT school_name FROM tbappdefaults"));
                string eid = listView1.SelectedItems[0].Name;
                List<SQLParameter> prms = new List<SQLParameter>();
                prms.Add(new SQLParameter("event_id", eid));
                string date = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT date FROM tbevents", prms));
                string time = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT time FROM tbevents", prms));
                string eventName = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT title FROM tbevents", prms));
                string venue = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT venue FROM tbevents", prms));

                //Event Name
                e.Graphics.DrawString("Event Name: ", bfont, Brushes.Black, 60, currentY);
                e.Graphics.DrawString(eventName, nfont, Brushes.Black, 170, currentY);
                //Date
                e.Graphics.DrawString("Date: ", bfont, Brushes.Black, 550, currentY);
                e.Graphics.DrawString(date, nfont, Brushes.Black, 615, currentY);
                currentY += 20;
                e.Graphics.DrawLine(pen, 160, currentY, 500, currentY);
                e.Graphics.DrawLine(pen, 605, currentY, paperSize.Width - 60, currentY);
                currentY += 10;

                //Venue
                e.Graphics.DrawString("Venue: ", bfont, Brushes.Black, 60, currentY);
                e.Graphics.DrawString(venue, nfont, Brushes.Black, 170, currentY);
                //Time
                e.Graphics.DrawString("Time: ", bfont, Brushes.Black, 550, currentY);
                e.Graphics.DrawString(time, nfont, Brushes.Black, 615, currentY);
                currentY += 20;
                e.Graphics.DrawLine(pen, 160, currentY, 500, currentY);
                e.Graphics.DrawLine(pen, 605, currentY, paperSize.Width - 60, currentY);
                currentY += 10;

                //School Name
                e.Graphics.DrawString("School Name: ", bfont, Brushes.Black, 60, currentY);
                e.Graphics.DrawString(schoolName, nfont, Brushes.Black, 170, currentY);
                currentY += 20;
                e.Graphics.DrawLine(pen, 160, currentY, paperSize.Width - 60, currentY);
                currentY += 10;

                //Club Name
                e.Graphics.DrawString("Club Name: ", bfont, Brushes.Black, 60, currentY);
                e.Graphics.DrawString(clubName, nfont, Brushes.Black, 170, currentY);
                currentY += 20;
                e.Graphics.DrawLine(pen, 160, currentY, paperSize.Width - 60, currentY);
                currentY += 50;

                prms.Clear();
                prms.Add(new SQLParameter("event_id", eid, "=", "Event ID"));
                reader = db.Select("SELECT `ID Number`, Name, time_in, time_out, remarks FROM view_attendance1", prms);
            }
            e.Graphics.DrawRectangle(pen, 65, currentY - 5, 30, 30);
            e.Graphics.DrawRectangle(pen, 95, currentY - 5, 100, 30);
            e.Graphics.DrawRectangle(pen, 195, currentY - 5, 300, 30);
            e.Graphics.DrawRectangle(pen, 495, currentY - 5, 80, 30);
            e.Graphics.DrawRectangle(pen, 575, currentY - 5, 80, 30);
            e.Graphics.DrawRectangle(pen, 655, currentY - 5, paperSize.Width - 60 - 660, 30);
            e.Graphics.DrawString("ID Number", bfont, Brushes.Black, 100, currentY);
            e.Graphics.DrawString("Student Name", bfont, Brushes.Black, 200, currentY);
            e.Graphics.DrawString("Time in", bfont, Brushes.Black, 500, currentY);
            e.Graphics.DrawString("Time out", bfont, Brushes.Black, 580, currentY);
            e.Graphics.DrawString("Remarks", bfont, Brushes.Black, 660, currentY);
            currentY += 30;
            if (reader!= null)
            {
                while(reader.Read())
                {
                    string idnum = reader.GetString(0);
                    string name = reader.GetString(1);
                    string timein = DataTypeManager.ParseDataToString(reader.GetValue(2));
                    string timeout = DataTypeManager.ParseDataToString(reader.GetValue(3));
                    string remarks = DataTypeManager.ParseDataToString(reader.GetValue(4));
                    e.Graphics.DrawRectangle(pen, 65, currentY - 5, 30, 30);
                    e.Graphics.DrawRectangle(pen, 95, currentY - 5, 100, 30);
                    e.Graphics.DrawRectangle(pen, 195, currentY - 5, 300, 30);
                    e.Graphics.DrawRectangle(pen, 495, currentY - 5, 80, 30);
                    e.Graphics.DrawRectangle(pen, 575, currentY - 5, 80, 30);
                    e.Graphics.DrawRectangle(pen, 655, currentY - 5, paperSize.Width - 60 - 660, 30);
                    e.Graphics.DrawString(cnt++.ToString(), nfont, Brushes.Black, 70, currentY);
                    e.Graphics.DrawString(idnum, nfont, Brushes.Black, 100, currentY);
                    e.Graphics.DrawString(name, (e.Graphics.MeasureString(name, nfont).Width >= 300) ? new Font(nfont.FontFamily, e.Graphics.MeasureString(name,DefaultFont).Width/(name.Length + 5)) : nfont, Brushes.Black, 200, currentY);
                    e.Graphics.DrawString(timein, nfont, Brushes.Black, 500, currentY);
                    e.Graphics.DrawString(timeout, nfont, Brushes.Black, 580, currentY);
                    e.Graphics.DrawString(remarks, nfont, Brushes.Black, 660, currentY);
                    currentY += 30;
                    if (currentY <= paperSize.Height - 70)
                    {
                        e.HasMorePages = false;
                    }
                    else
                    {
                        string pp = "Page " + currentPage++ + " of " + ++totalPages;
                        SizeF size = e.Graphics.MeasureString(pp, DefaultFont);
                        e.Graphics.DrawString(pp, DefaultFont, Brushes.Black, (paperSize.Width/2) - (size.Width/2), paperSize.Height - 37);
                        e.HasMorePages = true;
                        return;
                    }
                }
                string ppp = "Page " + currentPage + " of " + totalPages;
                SizeF sizep = e.Graphics.MeasureString(ppp, DefaultFont);
                e.Graphics.DrawString(ppp, DefaultFont, Brushes.Black, (paperSize.Width / 2) - (sizep.Width / 2), paperSize.Height - 37);
                currentPage = 1;
                totalPages = 1;
                reader.Close();
                reader.Dispose();
                reader = null;
            }
        }
        #endregion
    }
}
