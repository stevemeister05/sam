﻿namespace Student_Attendance_Manager
{
    partial class Control_FPimageBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fpImageBox = new System.Windows.Forms.PictureBox();
            this.fingerName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fpImageBox)).BeginInit();
            this.SuspendLayout();
            // 
            // fpImageBox
            // 
            this.fpImageBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.fpImageBox.Location = new System.Drawing.Point(0, 0);
            this.fpImageBox.Name = "fpImageBox";
            this.fpImageBox.Size = new System.Drawing.Size(200, 200);
            this.fpImageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fpImageBox.TabIndex = 1;
            this.fpImageBox.TabStop = false;
            // 
            // fingerName
            // 
            this.fingerName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fingerName.Location = new System.Drawing.Point(0, 200);
            this.fingerName.Name = "fingerName";
            this.fingerName.Size = new System.Drawing.Size(200, 40);
            this.fingerName.TabIndex = 2;
            this.fingerName.Text = "Finger Name";
            this.fingerName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Control_FPimageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.fingerName);
            this.Controls.Add(this.fpImageBox);
            this.Name = "Control_FPimageBox";
            this.Size = new System.Drawing.Size(200, 240);
            this.Resize += new System.EventHandler(this.Control_FPimageBox_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.fpImageBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox fpImageBox;
        private System.Windows.Forms.Label fingerName;
    }
}
