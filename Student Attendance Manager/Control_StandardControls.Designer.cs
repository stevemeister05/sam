﻿namespace Student_Attendance_Manager
{
    partial class Control_StandardControls
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Control_StandardControls));
            this.formLabel = new System.Windows.Forms.Label();
            this.panelSearch = new System.Windows.Forms.Panel();
            this.panelSearchDate = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.dateEnd = new System.Windows.Forms.DateTimePicker();
            this.dateBeg = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.buttonsMenu = new System.Windows.Forms.MenuStrip();
            this.bNew = new System.Windows.Forms.ToolStripMenuItem();
            this.bEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.bDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.bPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bPrint_printer = new System.Windows.Forms.ToolStripMenuItem();
            this.bPrint_screen = new System.Windows.Forms.ToolStripMenuItem();
            this.bPrint_PDF = new System.Windows.Forms.ToolStripMenuItem();
            this.bExport = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.panelSearch.SuspendLayout();
            this.panelSearchDate.SuspendLayout();
            this.panel1.SuspendLayout();
            this.buttonsMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // formLabel
            // 
            this.formLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.formLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.formLabel.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formLabel.ForeColor = System.Drawing.Color.White;
            this.formLabel.Location = new System.Drawing.Point(0, 0);
            this.formLabel.Name = "formLabel";
            this.formLabel.Size = new System.Drawing.Size(188, 70);
            this.formLabel.TabIndex = 1;
            this.formLabel.Text = "Transaction Name";
            this.formLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelSearch
            // 
            this.panelSearch.Controls.Add(this.panelSearchDate);
            this.panelSearch.Controls.Add(this.panel1);
            this.panelSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSearch.Location = new System.Drawing.Point(0, 70);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(188, 198);
            this.panelSearch.TabIndex = 2;
            this.panelSearch.VisibleChanged += new System.EventHandler(this.panelSearch_VisibleChanged);
            // 
            // panelSearchDate
            // 
            this.panelSearchDate.Controls.Add(this.label2);
            this.panelSearchDate.Controls.Add(this.dateEnd);
            this.panelSearchDate.Controls.Add(this.dateBeg);
            this.panelSearchDate.Controls.Add(this.label1);
            this.panelSearchDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSearchDate.Location = new System.Drawing.Point(0, 66);
            this.panelSearchDate.Name = "panelSearchDate";
            this.panelSearchDate.Size = new System.Drawing.Size(188, 132);
            this.panelSearchDate.TabIndex = 8;
            this.panelSearchDate.VisibleChanged += new System.EventHandler(this.panelSearchDate_VisibleChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.SteelBlue;
            this.label2.Location = new System.Drawing.Point(6, 66);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.MinimumSize = new System.Drawing.Size(84, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 26);
            this.label2.TabIndex = 10;
            this.label2.Text = "End Date:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dateEnd
            // 
            this.dateEnd.CustomFormat = "MMMM dd, yyyy";
            this.dateEnd.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dateEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateEnd.Location = new System.Drawing.Point(28, 94);
            this.dateEnd.Margin = new System.Windows.Forms.Padding(2);
            this.dateEnd.MinimumSize = new System.Drawing.Size(4, 26);
            this.dateEnd.Name = "dateEnd";
            this.dateEnd.ShowUpDown = true;
            this.dateEnd.Size = new System.Drawing.Size(138, 26);
            this.dateEnd.TabIndex = 11;
            this.dateEnd.ValueChanged += new System.EventHandler(this.date_ValueChanged);
            // 
            // dateBeg
            // 
            this.dateBeg.CustomFormat = "MMMM dd, yyyy";
            this.dateBeg.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dateBeg.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateBeg.Location = new System.Drawing.Point(28, 28);
            this.dateBeg.Margin = new System.Windows.Forms.Padding(2);
            this.dateBeg.MinimumSize = new System.Drawing.Size(4, 26);
            this.dateBeg.Name = "dateBeg";
            this.dateBeg.ShowUpDown = true;
            this.dateBeg.Size = new System.Drawing.Size(138, 26);
            this.dateBeg.TabIndex = 9;
            this.dateBeg.ValueChanged += new System.EventHandler(this.date_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.SteelBlue;
            this.label1.Location = new System.Drawing.Point(6, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.MinimumSize = new System.Drawing.Size(84, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 26);
            this.label1.TabIndex = 8;
            this.label1.Text = "Start Date:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.searchBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 66);
            this.panel1.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.SteelBlue;
            this.label4.Location = new System.Drawing.Point(6, 11);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.MinimumSize = new System.Drawing.Size(84, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 26);
            this.label4.TabIndex = 7;
            this.label4.Text = "Search:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // searchBox
            // 
            this.searchBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchBox.Location = new System.Drawing.Point(9, 38);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(170, 21);
            this.searchBox.TabIndex = 0;
            this.searchBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchBox_KeyDown);
            // 
            // buttonsMenu
            // 
            this.buttonsMenu.AutoSize = false;
            this.buttonsMenu.BackColor = System.Drawing.Color.Transparent;
            this.buttonsMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonsMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.buttonsMenu.ImageScalingSize = new System.Drawing.Size(25, 25);
            this.buttonsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bNew,
            this.bEdit,
            this.bDelete,
            this.bPrint,
            this.bExport});
            this.buttonsMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.buttonsMenu.Location = new System.Drawing.Point(0, 268);
            this.buttonsMenu.Name = "buttonsMenu";
            this.buttonsMenu.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.buttonsMenu.ShowItemToolTips = true;
            this.buttonsMenu.Size = new System.Drawing.Size(188, 258);
            this.buttonsMenu.TabIndex = 5;
            this.buttonsMenu.Text = "menuStrip3";
            // 
            // bNew
            // 
            this.bNew.AutoSize = false;
            this.bNew.ForeColor = System.Drawing.Color.SteelBlue;
            this.bNew.Image = ((System.Drawing.Image)(resources.GetObject("bNew.Image")));
            this.bNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bNew.Name = "bNew";
            this.bNew.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
            this.bNew.Size = new System.Drawing.Size(170, 45);
            this.bNew.Text = "New";
            this.bNew.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bNew.Visible = false;
            // 
            // bEdit
            // 
            this.bEdit.AutoSize = false;
            this.bEdit.ForeColor = System.Drawing.Color.SteelBlue;
            this.bEdit.Image = ((System.Drawing.Image)(resources.GetObject("bEdit.Image")));
            this.bEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bEdit.Name = "bEdit";
            this.bEdit.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
            this.bEdit.Size = new System.Drawing.Size(170, 45);
            this.bEdit.Text = "Edit";
            this.bEdit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bEdit.Visible = false;
            // 
            // bDelete
            // 
            this.bDelete.AutoSize = false;
            this.bDelete.ForeColor = System.Drawing.Color.SteelBlue;
            this.bDelete.Image = ((System.Drawing.Image)(resources.GetObject("bDelete.Image")));
            this.bDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bDelete.Name = "bDelete";
            this.bDelete.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
            this.bDelete.Size = new System.Drawing.Size(170, 45);
            this.bDelete.Text = "Delete";
            this.bDelete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bDelete.Visible = false;
            // 
            // bPrint
            // 
            this.bPrint.AutoSize = false;
            this.bPrint.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator2,
            this.bPrint_printer,
            this.bPrint_screen,
            this.bPrint_PDF});
            this.bPrint.ForeColor = System.Drawing.Color.SteelBlue;
            this.bPrint.Image = ((System.Drawing.Image)(resources.GetObject("bPrint.Image")));
            this.bPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bPrint.Name = "bPrint";
            this.bPrint.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
            this.bPrint.Size = new System.Drawing.Size(170, 45);
            this.bPrint.Text = "Print";
            this.bPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bPrint.Visible = false;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(106, 6);
            // 
            // bPrint_printer
            // 
            this.bPrint_printer.Name = "bPrint_printer";
            this.bPrint_printer.Size = new System.Drawing.Size(109, 22);
            this.bPrint_printer.Text = "Printer";
            this.bPrint_printer.Visible = false;
            // 
            // bPrint_screen
            // 
            this.bPrint_screen.Name = "bPrint_screen";
            this.bPrint_screen.Size = new System.Drawing.Size(109, 22);
            this.bPrint_screen.Text = "Screen";
            this.bPrint_screen.Visible = false;
            // 
            // bPrint_PDF
            // 
            this.bPrint_PDF.Name = "bPrint_PDF";
            this.bPrint_PDF.Size = new System.Drawing.Size(109, 22);
            this.bPrint_PDF.Text = "PDF";
            this.bPrint_PDF.Visible = false;
            // 
            // bExport
            // 
            this.bExport.AutoSize = false;
            this.bExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.bExport.ForeColor = System.Drawing.Color.SteelBlue;
            this.bExport.Image = global::Student_Attendance_Manager.Properties.Resources.excell;
            this.bExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bExport.Name = "bExport";
            this.bExport.Padding = new System.Windows.Forms.Padding(10, 0, 4, 0);
            this.bExport.Size = new System.Drawing.Size(170, 45);
            this.bExport.Text = "Export to Excel";
            this.bExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bExport.Visible = false;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(106, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem2.Text = "Printer";
            this.toolStripMenuItem2.Visible = false;
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem3.Text = "Screen";
            this.toolStripMenuItem3.Visible = false;
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem4.Text = "PDF";
            this.toolStripMenuItem4.Visible = false;
            // 
            // Control_StandardControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.buttonsMenu);
            this.Controls.Add(this.panelSearch);
            this.Controls.Add(this.formLabel);
            this.Name = "Control_StandardControls";
            this.Size = new System.Drawing.Size(188, 526);
            this.panelSearch.ResumeLayout(false);
            this.panelSearchDate.ResumeLayout(false);
            this.panelSearchDate.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.buttonsMenu.ResumeLayout(false);
            this.buttonsMenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label formLabel;
        private System.Windows.Forms.Panel panelSearch;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelSearchDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateBeg;
        private System.Windows.Forms.DateTimePicker dateEnd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip buttonsMenu;
        private System.Windows.Forms.ToolStripMenuItem bNew;
        private System.Windows.Forms.ToolStripMenuItem bEdit;
        private System.Windows.Forms.ToolStripMenuItem bDelete;
        private System.Windows.Forms.ToolStripMenuItem bPrint;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem bPrint_printer;
        private System.Windows.Forms.ToolStripMenuItem bPrint_screen;
        private System.Windows.Forms.ToolStripMenuItem bPrint_PDF;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem bExport;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
    }
}
