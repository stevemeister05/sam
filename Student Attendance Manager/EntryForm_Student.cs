﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DPUruNet;
using DPCtlUruNet;
using System.IO;

namespace Student_Attendance_Manager
{
    public partial class EntryForm_Student : Form
    {
        private FPReader reader = null;
        private EnrollmentControl enrollmentControl;
        private MySQLDatabase db;
        private List<Fingerprint> fingerprints;
        private Fid fpImageView;
        private string clientID;
        private bool hasRegisteredFp = false;
        private bool hasRegisteredFp1 = false;
        public EntryForm_Student(string clientID = null)
        {
            InitializeComponent();
            this.clientID = clientID;
            reader = FPReaderManager.GetInstance();
            if (ReaderCollection.GetReaders().Count > 0)
            {
                reader.CurrentReader = ReaderCollection.GetReaders()[0];
            }
            init();
            sex.SelectedIndex = 0;
            if (this.clientID != null)
            {
                List<SQLParameter> prmsss = new List<SQLParameter>();
                prmsss.Add(new SQLParameter("student_id", clientID));
                string photoPath = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT image FROM tbstudents", prmsss));
                this.student_id.ReadOnly = true;
                if (photoPath != null && photoPath.Length > 0)
                {
                    try
                    {
                        residentPhoto.Load(photoPath);
                        residentPhoto.Image.Tag = null;
                        residentPhoto.Tag = photoPath;
                    }
                    catch (Exception err)
                    {
                        Console.WriteLine("Error message: " + err.Message);
                        Console.WriteLine(err.StackTrace);
                        residentPhoto.Tag = null;
                    }
                }
                IDataReader readerr = db.Select("SELECT * FROM tbstudents", prmsss);
                DataTable dt = new DataTable();
                dt.Load(readerr);
                readerr.Close();
                FormManager.populateFields(panelClientName, dt);
                FormManager.populateFields(panelStudentInformation, dt);
                readerr = db.Select("SELECT position, fingerprint, FPPath FROM tbfingerprints", prmsss);
                if (readerr != null)
                {
                    while (readerr.Read())
                    {
                        hasRegisteredFp = true;
                        try
                        {
                            Fmd fpFmd = Fmd.DeserializeXml(DataTypeManager.ParseDataToString(readerr.GetValue(1)));
                            Fid fpFid = Fid.DeserializeXml(FileManager.ReadFromFile(readerr.GetString(2) + ".xml"));
                            Fingerprint fprint = new Fingerprint(readerr.GetInt32(0), fpFmd, fpFid);
                            fingerprints.Add(fprint);
                            Control_FPimageBox fpib = new Control_FPimageBox();
                            panelFPs.Controls.Add(fpib);
                            //fpib.BringToFront();
                            fpib.Dock = DockStyle.Left;
                            fpib.Fingerprint = fprint;
                        }
                        catch (Exception err)
                        {
                            Console.WriteLine(err.StackTrace);
                            MessageBox.Show(err.Message);

                        }

                    }
                    readerr.Close();
                }
            }
        }

        private void init()
        {
            fingerprints = new List<Fingerprint>();
            reader = FPReaderManager.GetInstance();
            db = DatabaseManager.GetInstance();
            if (enrollmentControl != null)
            {
                enrollmentControl.Reader = reader.CurrentReader;
            }
            else
            {
                enrollmentControl = new EnrollmentControl(reader.CurrentReader, Constants.CapturePriority.DP_PRIORITY_COOPERATIVE);
                enrollmentControl.BackColor = System.Drawing.SystemColors.Window;
                enrollmentControl.Location = new System.Drawing.Point(2, 10);
                enrollmentControl.Name = "ctlEnrollmentControl";
                enrollmentControl.Size = new System.Drawing.Size(482, 346);
                enrollmentControl.TabIndex = 0;
                enrollmentControl.OnCancel += new EnrollmentControl.CancelEnrollment(this.enrollment_OnCancel);
                enrollmentControl.OnCaptured += new EnrollmentControl.FingerprintCaptured(this.enrollment_OnCaptured);
                enrollmentControl.OnDelete += new EnrollmentControl.DeleteEnrollment(this.enrollment_OnDelete);
                enrollmentControl.OnEnroll += new EnrollmentControl.FinishEnrollment(this.enrollment_OnEnroll);
                enrollmentControl.OnStartEnroll += new EnrollmentControl.StartEnrollment(this.enrollment_OnStartEnroll);
            }
            tabPage3.Controls.Add(enrollmentControl);
        }

        private void bNext_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex < tabControl1.TabCount - 1)
            {
                tabControl1.SelectedIndex++;
            }
            else
            {
                List<string> exmptions = new List<string>();
                exmptions.Add("txtMessage");
                if (!FormManager.isFieldsFilled(this, exmptions))
                {
                    return;
                }
                List<SQLParameter> prms = FormManager.getParameters(panelStudentInformation);
                prms.AddRange(FormManager.getParameters(panelClientName));
                int cid = 0;
                SQLParameter clientidParams = null;
                bool flagg = false;
                SQLParameter[] prmss = { null };
                if (clientID == null)
                {
                    flagg = db.Insert("tbstudents", prms);
                    clientidParams = new SQLParameter("clientidd", student_id.Text, "=", "student_id");
                    cid = DataTypeManager.ParseDataToInt(student_id.Text);
                }
                else
                {
                    cid = Convert.ToInt32(clientID);
                    clientidParams = new SQLParameter("clientiddd", cid, "=", "student_id");
                    prmss[0] = clientidParams;
                    flagg = db.Update("tbstudents", prms, prmss);
                }
                if (flagg)
                {
                    bool flag = true;
                    string folderName = AppConfiguration.GetConfig("syslocation") + "Students\\" + cid;
                    string fpFolderName = folderName + "\\Fingerprints\\";
                    bool insertFp = true;
                    if (residentPhoto.Tag != null)
                    {
                        if (Convert.ToString(residentPhoto.Image.Tag) == "new")
                        {
                            SQLParameter[] photoPrms = { clientidParams };
                            string fileXtnsion = ".png";
                            try
                            {
                                fileXtnsion = Path.GetExtension(Convert.ToString(residentPhoto.Tag));
                            }
                            catch (Exception err)
                            {
                                Console.WriteLine("Error Message: " + err.Message);
                                Console.WriteLine(err.StackTrace);
                            }
                            string photoName = "Photo-" + cid + fileXtnsion;
                            string newPhotoPath = folderName + "\\Photo\\" + photoName;
                            List<SQLParameter> photoPrms1 = new List<SQLParameter>();
                            photoPrms1.Add(new SQLParameter("image", newPhotoPath));
                            if (db.Update("tbstudents", photoPrms1, photoPrms))
                            {
                                FileManager.SaveImageToLocalFolder(folderName + "\\Photo\\", photoName, residentPhoto.Image);
                            }
                        }
                    }
                    if (hasRegisteredFp)
                    {
                        if (hasRegisteredFp1)
                        {
                            List<SQLParameter> deletefpprms = new List<SQLParameter>();
                            deletefpprms.Add(clientidParams);
                            db.Delete("tbfingerprints", deletefpprms);
                            FileManager.EmptyDirectory(fpFolderName);
                        }
                        insertFp = hasRegisteredFp1;
                    }
                    if (insertFp)
                    {
                        foreach (Fingerprint fp in fingerprints)
                        {
                            string fileLoc = fpFolderName + fp.FingerName;
                            if (!FileManager.WriteToFile(fpFolderName, fp.FingerName + ".xml", fp.FidToXML()) || !FileManager.SaveImageToLocalFolder(fpFolderName, fp.FingerName + "-" + cid + ".png", fp.Image))
                            {
                                flag = false;
                                break;
                            }
                            List<SQLParameter> fpparams = new List<SQLParameter>();
                            fpparams.Add(clientidParams);
                            fpparams.Add(new SQLParameter("position", fp.Position));
                            fpparams.Add(new SQLParameter("fingerprint", fp.ToXML()));
                            fpparams.Add(new SQLParameter("FPPath", fileLoc));
                            if (!db.Insert("tbfingerprints", fpparams))
                            {
                                flag = false;
                                break;
                            }
                        }
                    }
                    if (flag)
                    {
                        DialogResult = DialogResult.OK;
                        Close();
                    }
                    else
                    {
                        //rollback code in case magka error sa pag create ng xml at image file ng fingerprint
                    }
                }
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == tabControl1.TabCount - 1)
            {
                bNext.Text = "Finish";
            }
            else
            {
                bNext.Text = "Next";
            }
        }

        #region Enrollment Control Events
        private void enrollment_OnCancel(EnrollmentControl enrollmentControl, Constants.ResultCode result, int fingerPosition)
        {
            if (enrollmentControl.Reader != null)
            {
                SendMessage("OnCancel:  " + enrollmentControl.Reader.Description.Name + ", finger " + fingerPosition);
            }
            else
            {
                SendMessage("OnCancel:  No Reader Connected, finger " + fingerPosition);
            }

            btnCancel.Enabled = false;
        }
        private void enrollment_OnCaptured(EnrollmentControl enrollmentControl, CaptureResult captureResult, int fingerPosition)
        {
            if (enrollmentControl.Reader != null)
            {
                SendMessage("OnCaptured:  " + enrollmentControl.Reader.Description.Name + ", finger " + fingerPosition + ", quality " + captureResult.Quality.ToString());
            }
            else
            {
                SendMessage("OnCaptured:  No Reader Connected, finger " + fingerPosition);
            }

            if (captureResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
            {
                if (reader.CurrentReader != null)
                {
                    reader.CurrentReader.Dispose();
                    reader.CurrentReader = null;
                }

                enrollmentControl.Reader = null;

                MessageBox.Show("Error:  " + captureResult.ResultCode);
                btnCancel.Enabled = false;
            }
            else
            {
                if (captureResult.Data != null)
                {
                    foreach (Fid.Fiv fiv in captureResult.Data.Views)
                    {
                        pbFingerprint.Image = reader.CreateBitmap(fiv.RawImage, fiv.Width, fiv.Height);
                    }
                    fpImageView = captureResult.Data;
                }
            }
        }
        private void enrollment_OnDelete(EnrollmentControl enrollmentControl, Constants.ResultCode result, int fingerPosition)
        {
            if (enrollmentControl.Reader != null)
            {
                SendMessage("OnDelete:  " + enrollmentControl.Reader.Description.Name + ", finger " + fingerPosition);
                SendMessage("Enrolled Finger Mask: " + enrollmentControl.EnrolledFingerMask);
                SendMessage("Disabled Finger Mask: " + enrollmentControl.DisabledFingerMask);
            }
            else
            {
                SendMessage("OnDelete:  No Reader Connected, finger " + fingerPosition);
            }

            reader.Fmds.Remove(fingerPosition);
            for (int i = 0; i < fingerprints.Count; i++)
            {
                if (fingerPosition == fingerprints[i].Position)
                {
                    fingerprints.RemoveAt(i);
                    panelFPs.Controls.RemoveAt(i);
                }
            }
            if (reader.Fmds.Count == 0)
            {
            }
        }
        private void enrollment_OnEnroll(EnrollmentControl enrollmentControl, DataResult<Fmd> result, int fingerPosition)
        {
            if (enrollmentControl.Reader != null)
            {
                SendMessage("OnEnroll:  " + enrollmentControl.Reader.Description.Name + ", finger " + fingerPosition);
                SendMessage("Enrolled Finger Mask: " + enrollmentControl.EnrolledFingerMask);
                SendMessage("Disabled Finger Mask: " + enrollmentControl.DisabledFingerMask);
            }
            else
            {
                SendMessage("OnEnroll:  No Reader Connected, finger " + fingerPosition);
            }
            if (result != null && result.Data != null)
            {
                //reader.Fmds.Add(fingerPosition, result.Data);
                Fingerprint fp = new Fingerprint(fingerPosition, result.Data, fpImageView);
                fingerprints.Add(fp);
                Control_FPimageBox ib = new Control_FPimageBox();
                ib.Dock = DockStyle.Left;
                panelFPs.Controls.Add(ib);
                ib.BringToFront();
                ib.Fingerprint = fp;
            }
            btnCancel.Enabled = false;
        }

        private void enrollment_OnStartEnroll(EnrollmentControl enrollmentControl, Constants.ResultCode result, int fingerPosition)
        {
            if (enrollmentControl.Reader != null)
            {
                if (hasRegisteredFp && !hasRegisteredFp1)
                {
                    DialogResult rs = MessageBox.Show(
                        "Resident has already registered his/her fingerprints. Continuing this will reset his fingerprint registration.\r\n\r\nAre you sure you want to proceed?",
                        "Attention",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Warning);
                    if (rs == DialogResult.Yes)
                    {
                        fingerprints.Clear();
                        panelFPs.Controls.Clear();
                        hasRegisteredFp1 = true;
                        SendMessage("OnStartEnroll: " + enrollmentControl.Reader.Description.Name + ", finger " + fingerPosition);
                    }
                    else
                    {
                        enrollmentControl.Cancel();
                    }
                }
            }
            else
            {
                SendMessage("OnStartEnroll:  No Reader Connected, finger " + fingerPosition);
            }
            btnCancel.Enabled = true;
        }
        #endregion
        private void SendMessage(string message)
        {
            txtMessage.Text += message + "\r\n\r\n";
            txtMessage.SelectionStart = txtMessage.TextLength;
            txtMessage.ScrollToCaret();
        }
        private void bClose_Click(object sender, EventArgs e)
        {
            enrollmentControl.Cancel(); 
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void Label5_MouseHover(object sender, EventArgs e)
        {
            bClose.BackColor = Color.Firebrick;
        }

        private void BClose_MouseLeave(object sender, EventArgs e)
        {
            bClose.BackColor = Color.FromArgb(64, 64, 64);
        }

        private void bFP_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
        }

        private void bPDetails_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form_CameraCapture fcc = new Form_CameraCapture(residentPhoto);
            if(fcc.ShowDialog() == DialogResult.OK)
            {
                residentPhoto.Image.Tag = "new";
            }
            fcc.Dispose();
        }

        private void bBrowsePhoto_Click(object sender, EventArgs e)
        {
            string photoDirectory = FileManager.SelectFile("Select a photo", "Image files|*.png;*.jpg;*.jpeg");
            if (photoDirectory != null)
            {
                residentPhoto.Load(photoDirectory);
                residentPhoto.Tag = photoDirectory;
                residentPhoto.Image.Tag = "new";
            }
        }

        private void tabPage1_Enter(object sender, EventArgs e)
        {
            HighlightButton(bPDetails);
        }

        private void tabPage3_Enter(object sender, EventArgs e)
        {
            HighlightButton(bFP);
        }

        private void HighlightButton(Control control)
        {
            control.BackColor = Color.White;
            control.ForeColor = Color.FromArgb(64, 64, 64);
            foreach (Control c in menuPanel.Controls)
            {
                if (c is Button)
                {
                    if (c != control)
                    {
                        c.BackColor = Color.FromArgb(19, 128, 201);
                        c.ForeColor = Color.FromArgb(123, 200, 252);
                    }
                }
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();
        private void menuPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void student_id_TextChanged(object sender, EventArgs e)
        {
            int parsedValue;
            TextBox t = (TextBox)sender;
            if (!int.TryParse(t.Text, out parsedValue))
            {
                t.Text = "";
            }
        }
    }
}
