﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class UpdateForm_Student : Form
    {
        private MySQLDatabase db;
        private SQLParameter[] prms1;
        public UpdateForm_Student(ListView.SelectedListViewItemCollection listViewItems)
        {
            InitializeComponent();
            db = DatabaseManager.GetInstance();
            if(listViewItems != null)
            {
                prms1 = new SQLParameter[listViewItems.Count];
                int x = 0;
                try
                {
                    foreach (ListViewItem lvi in listViewItems)
                    {
                        string idnum = lvi.SubItems[0].Text;
                        prms1[x++] = new SQLParameter(idnum, idnum, "=", "student_id", "OR");
                        string[] lvitem = { idnum, lvi.SubItems[1].Text };
                        ListViewItem newlvi = new ListViewItem(lvitem);
                        newlvi.Name = lvi.Name;
                        listView1.Items.Add(newlvi);
                    }
                    year_level.Value = Convert.ToInt32(listViewItems[0].SubItems[2].Text);
                    status.Items.Add(new KeyValuePair<int, string>(1, "Active"));
                    status.Items.Add(new KeyValuePair<int, string>(0, "Inactive"));
                    status.ValueMember = "Key";
                    status.DisplayMember = "Value";
                    status.SelectedIndex = 0;
                }
                catch(Exception err)
                {
                    year_level.Value = 1;
                    Console.WriteLine("Error Message: " + err.Message + "\r\n\r\n" + err.StackTrace);
                    MessageBox.Show(err.Message);
                }
            }
        }

        #region ControlBoxEvents
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();
        private void menuPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void Label5_MouseHover(object sender, EventArgs e)
        {
            bClose.BackColor = Color.Firebrick;
        }

        private void BClose_MouseLeave(object sender, EventArgs e)
        {
            bClose.BackColor = Color.FromArgb(64, 64, 64);
        }
        #endregion

        private void bClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void status_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(((KeyValuePair<int, string>)status.SelectedItem).Key == 0)
            {
                panelYearLevel.Visible = false;
            }
            else
            {
                panelYearLevel.Visible = true;
            }
        }

        private void bConfirm_Click(object sender, EventArgs e)
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            int stts = ((KeyValuePair<int, string>)status.SelectedItem).Key;
            prms.Add(new SQLParameter("status", stts));
            prms.Add(new SQLParameter("remarks", remarks.Text));
            if (stts != 0)
            {
                prms.AddRange(FormManager.getParameters(panelYearLevel));
            }
            if(db.Update("tbstudents", prms, prms1))
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}
