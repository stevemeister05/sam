﻿using DPUruNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class Form_Login : Form
    {
        private MySQLDatabase db;
        private FPReader fpreader;
        public Form_Login()
        {
            InitializeComponent();
            db = DatabaseManager.GetInstance();
            try
            {
                Image img = Image.FromFile(DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT school_logo FROM tbappdefaults")));
                pictureBox1.Image = (Image)(img.Clone());
                img.Dispose();
                img = null;
            }
            catch(Exception err)
            {
                Console.WriteLine("Error message: " + err.Message + "\r\n\r\n" + err.StackTrace);
                pictureBox1.Image = Student_Attendance_Manager.Properties.Resources.NDDUseal;
            }

            fpreader = FPReaderManager.GetInstance();
            initializeFpReader();
        }

        private void initializeFpReader()
        {
            if (fpreader.CurrentReader == null)
            {
                if(ReaderCollection.GetReaders().Count > 0)
                {
                    fpreader.CurrentReader = ReaderCollection.GetReaders()[0];
                }
                else
                {
                    SendMessage(FPReader.Action.SendMessage, "No fingerprint reader attached.");
                }
            }
            if (fpreader.CurrentReader != null)
            {
                if (!fpreader.OpenReader())
                {
                    SendMessage(FPReader.Action.SendMessage, "Can't open fingerprint reader.");
                }
                if (!fpreader.StartCaptureAsync(this.OnCaptured))
                {
                    SendMessage(FPReader.Action.SendMessage, "Can't open fingerprint reader.");
                }
            }
        }

        private void bLogin_Click(object sender, EventArgs e)
        {
            if(username.TextLength < 0)
            {
                MessageBox.Show("Please enter a username");
                username.Focus();
                return;
            }
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("username", username.Text));
            prms.Add(new SQLParameter("password", HashComputer.ComputeSha256Hash(password.Text)));
            int uid = DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT user_id FROM tbusers", prms));
            if(uid > 0)
            {
                loginUser(uid);
            }
            else
            {
                MessageBox.Show("Incorrect username and/or password");
                password.Clear();
                username.Focus();
                username.Select(0, username.TextLength);
            }
        }

        private void loginUser(int uid)
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            ActiveUser.userid = uid;
            prms.Add(new SQLParameter("user_id", uid, "=", "User ID"));
            ActiveUser.name = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT Name FROM view_users", prms));
            ActiveUser.type = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT type FROM view_users", prms));
            prms.Clear();
            prms.Add(new SQLParameter("user_id", uid));
            ActiveUser.photo = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT photo FROM view_userphoto", prms));
            //ActiveUser.photo = new Image(DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT Name FROM view_users", prms)));
            fpreader.CancelCaptureAndCloseReader(this.OnCaptured);
            Main main = new Main();
            Hide();
            if (main.ShowDialog() == DialogResult.Cancel)
            {
                main.Dispose();
                this.Close();
            }
            else
            {
                main.Dispose();
                message.Text = "";
                password.Clear();
                username.Focus();
                initializeFpReader();
                Show();
            }
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void OnCaptured(CaptureResult captureResult)
        {
            try
            {
                SendMessage(FPReader.Action.SendMessage, "Reading fingerprint");
                if (!fpreader.CheckCaptureResult(captureResult)) return;
                DataResult<Fmd> resultConversion = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ANSI);
                if (resultConversion.ResultCode != Constants.ResultCode.DP_SUCCESS)
                {
                    fpreader.Reset = true;
                    throw new Exception(resultConversion.ResultCode.ToString());
                }
                Fmd firstFinger = resultConversion.Data;
                IDataReader reader = db.Select("SELECT student_id, fingerprint FROM view_fingerprints WHERE status<>0");
                if (reader != null)
                {
                    bool matched = false;
                    while (!matched && reader.Read())
                    {
                        Fmd secondFinger = Fmd.DeserializeXml(reader.GetString(1));
                        CompareResult compareResult = Comparison.Compare(firstFinger, 0, secondFinger, 0);
                        if (compareResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
                        {
                            fpreader.Reset = true;
                            reader.Close();
                            throw new Exception(compareResult.ResultCode.ToString());
                        }
                        if (compareResult.Score < (fpreader.PROBABILITY_ONE / 100000))
                        {
                            matched = true;
                            object id = reader.GetValue(0);
                            reader.Close();
                            SendMessage(FPReader.Action.Login, id);
                        }
                    }
                    reader.Close();
                    if (!matched)
                    {
                        SendMessage(FPReader.Action.SendMessage, "No user account matched with the fingerprint");
                    }
                }
            }
            catch (Exception ex)
            {
                SendMessage(FPReader.Action.SendMessage, "Please place your finger on the reader properly.");
                Console.WriteLine("Error message: " + ex.Message + "\r\n\r\n" + ex.StackTrace);
            }
        }

        #region SendMessage
        private delegate void SendMessageCallback(FPReader.Action action, object payload = null);
        private void SendMessage(FPReader.Action action, object payload = null)
        {
            try
            {
                if (this.InvokeRequired)
                {
                    SendMessageCallback d = new SendMessageCallback(SendMessage);
                    this.Invoke(d, new object[] { action, payload });
                }
                else
                {
                    switch (action)
                    {
                        case FPReader.Action.SendMessage:
                            message.Text = Convert.ToString(payload);
                            break;
                        case FPReader.Action.Login:
                            if(payload != null)
                            {
                                List<SQLParameter> prms = new List<SQLParameter>();
                                prms.Add(new SQLParameter("studentid", Convert.ToString(payload)));
                                int uid = DataTypeManager.ParseDataToInt(db.ScalarSelect("SELECT user_id FROM tbusers", prms));
                                message.Text = "User account found.";
                                if(uid > 0)
                                {
                                    loginUser(uid);
                                }
                                else
                                {
                                    message.Text = "No user account matched with the fingerprint";
                                }
                            }
                            break;
                        case FPReader.Action.CloseReader:
                            fpreader.CurrentReader.Reset();
                            break;
                    }
                }
            }
            catch (Exception err)
            {
                Console.WriteLine(err.StackTrace);
                MessageBox.Show(err.Message);
            }
        }
        #endregion

        private void bSettings_MouseHover(object sender, EventArgs e)
        {
            bSettings.Height = 26;
            bSettings.Width = 26;
            bSettings.Location = new Point(bSettings.Location.X - 3, bSettings.Location.Y - 3);
        }

        private void bSettings_MouseLeave(object sender, EventArgs e)
        {
            bSettings.Height = 20;
            bSettings.Width = 20;
            bSettings.Location = new Point(bSettings.Location.X + 3, bSettings.Location.Y + 3);
        }

        private void bSettings_MouseClick(object sender, MouseEventArgs e)
        {
            EntryForm_Settings efs = new EntryForm_Settings(1, false);
            efs.ShowDialog();
            efs.Dispose();
            db = DatabaseManager.GetInstance();
        }
    }
}
