﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class EntryForm_ChangePassword : Form
    {
        private MySQLDatabase db;
        public EntryForm_ChangePassword()
        {
            InitializeComponent();
            db = DatabaseManager.GetInstance();
            setPasswordFlag();
        }

        private bool isCorrectPassword(string password)
        {
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("user_id", ActiveUser.userid));
            string currPw = DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT password FROM tbusers", prms));
            if(currPw == HashComputer.ComputeSha256Hash(password))
            {
                return true;
            }
            return false;
        }

        private void setPasswordFlag()
        {
            if(isCorrectPassword(currentPassword.Text))
            {
                labelyea.Text = "ok";
                labelyea.ForeColor = Color.Green;
                bConfirm.Enabled = true;
                panelNewPassword.Enabled = true;
            }
            else
            {
                labelyea.Text = "!";
                labelyea.ForeColor = Color.Firebrick;
                bConfirm.Enabled = false;
                panelNewPassword.Enabled = false;
            }
        }

        private void currentPassword_TextChanged(object sender, EventArgs e)
        {
            setPasswordFlag();
        }

        #region ControlBoxEvents
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();
        private void menuPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void Label5_MouseHover(object sender, EventArgs e)
        {
            bClose.BackColor = Color.Firebrick;
        }

        private void BClose_MouseLeave(object sender, EventArgs e)
        {
            bClose.BackColor = Color.FromArgb(64, 64, 64);
        }
        #endregion

        private bool doesNewPasswordMatch()
        {
            if (newPassword.Text != cNewPassword.Text)
            {
                return false;
            }
            return true;
        }
        private void bConfirm_Click(object sender, EventArgs e)
        {
            if(!isCorrectPassword(currentPassword.Text))
            {
                MessageBox.Show("The current password you entered is incorrect");
                return;
            }
            if(!doesNewPasswordMatch())
            {
                MessageBox.Show("The new password and the confirmation you entered don't match");
                return;
            }
            List<SQLParameter> prms = new List<SQLParameter>();
            prms.Add(new SQLParameter("password", HashComputer.ComputeSha256Hash(newPassword.Text)));
            SQLParameter[] prms1 = { new SQLParameter("user_id", ActiveUser.userid) };
            if(db.Update("tbusers", prms, prms1))
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
