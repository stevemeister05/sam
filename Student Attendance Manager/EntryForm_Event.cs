﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class EntryForm_Event : Form
    {
        private string eventID;
        private MySQLDatabase db;
        public EntryForm_Event(string eventID = null)
        {
            InitializeComponent();
            this.eventID = eventID;
            db = DatabaseManager.GetInstance();
            date.Value = DateTime.Today;
            int temp = date.Value.Year;
            if (this.eventID == null)
            {
                type.SelectedIndex = 0;
            }
            else
            {
                List<SQLParameter> prms = new List<SQLParameter>();
                prms.Add(new SQLParameter("event_id", eventID));
                IDataReader reader = db.Select("SELECT * FROM tbevents", prms);
                FormManager.populateFields(panelContent, reader);
            }
        }

        private void bConfirm_Click(object sender, EventArgs e)
        {
            if(!FormManager.isFieldsFilled(this, null))
            {
                return;
            }
            if(log_out_time.Value.TimeOfDay.CompareTo(time.Value.TimeOfDay) <= 0)
            {
                MessageBox.Show("Start time of Log out can't happen before the event");
                log_out_time.Focus();
                return;
            }
            List<SQLParameter> prms = FormManager.getParameters(this);
            prms.Add(new SQLParameter("school_year", DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT school_year FROM tbappdefaults"))));
            prms.Add(new SQLParameter("semester", DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT semester FROM tbappdefaults"))));
            prms.Add(new SQLParameter("enrolled_students", DataTypeManager.ParseDataToString(db.ScalarSelect("SELECT COUNT(student_id) FROM tbstudents WHERE status=1"))));
            bool flag;
            if(eventID == null)
            {
                flag = db.Insert("tbevents", prms);
            }
            else
            {
                SQLParameter[] prmmss = { new SQLParameter("event_id", eventID) };
                flag = db.Update("tbevents", prms, prmmss);
            }
            if(flag)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();
        private void menuPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void Label5_MouseHover(object sender, EventArgs e)
        {
            bClose.BackColor = Color.Firebrick;
        }

        private void BClose_MouseLeave(object sender, EventArgs e)
        {
            bClose.BackColor = Color.FromArgb(64, 64, 64);
        }

        private void time_ValueChanged(object sender, EventArgs e)
        {
            log_out_time.Value = time.Value;
            log_out_time.Value.TimeOfDay.Add(new TimeSpan(1, 0, 0));
        }
    }
}
