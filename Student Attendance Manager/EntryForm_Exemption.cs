﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class EntryForm_Exemption : Form
    {
        private MySQLDatabase db;
        private int eventid;
        public EntryForm_Exemption(int eventid)
        {
            InitializeComponent();
            this.eventid = eventid;
            db = DatabaseManager.GetInstance();
            IDataReader reader = db.Select("SELECT `ID Number`, Name FROM view_students WHERE status='Active' AND `ID Number` NOT IN (SELECT student_id FROM tbevent_exemptions WHERE event_id='" + eventid + "')");
            ListViewManager.populateListView(listView1, reader, true);
        }

        private void bcancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach(ListViewItem lvi in listView1.CheckedItems)
            {
                List <SQLParameter> prms = new List<SQLParameter>();
                prms.Add(new SQLParameter("event_id", eventid));
                prms.Add(new SQLParameter("student_id", lvi.Name));
                db.Insert("tbevent_exemptions", prms);
            }
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
