﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace Student_Attendance_Manager
{
    public partial class Control_StandardControls : UserControl
    {
        public enum HideOptions
        {
            AllSearchControls = 0,
            SearchDateControls = 1,
            SearchBar = 2,
            EntryPrintControls = 3,
            AllEntryControls = 4,
            None = 9
        }

        private MySQLDatabase db;
        private DatabaseTableView view;
        private ListView listView;

        public bool NewButtonVisible
        {
            get { return bNew.Visible; }
            set { bNew.Visible = value; }
        }
        public bool EditButtonVisible
        {
            get { return bEdit.Visible; }
            set { bEdit.Visible = value; }
        }
        public bool DeleteButtonVisible
        {
            get { return bDelete.Visible; }
            set { bDelete.Visible = value; }
        }
        public bool PrintButtonVisible
        {
            get { return bPrint.Visible; }
            set { bPrint.Visible = value; }
        }
        public bool ExportButtonVisible
        {
            get { return bExport.Visible; }
            set { bExport.Visible = value; }
        }
        public DatabaseTableView View
        {
            set { view = value; }
        }
        public DateTime StartDate
        {
            get { return dateBeg.Value; }
            set { dateBeg.Value = value; }
        }

        public DateTime EndDate
        {
            get { return dateEnd.Value; }
            set { dateEnd.Value = value; }
        }

        public ListView ListView
        {
            set 
            {
                listView = value;
                listView.SelectedIndexChanged += ListView_SelectedIndexChanged;
                SetMenuButtonsStatus(listView.SelectedItems.Count < 1 ? false : true);
            }
        }

        public MenuStrip ButtonsMenu
        {
            get { return buttonsMenu; }
        }

        public void HideControl(HideOptions option = HideOptions.None)
        {
            switch(option)
            {
                case HideOptions.AllSearchControls:
                    panelSearch.Visible = false;
                    break;
                case HideOptions.SearchDateControls:
                    panelSearchDate.Visible = false;
                    break;
                case HideOptions.SearchBar:
                    label4.Visible = false;
                    searchBox.Visible = false;
                    break;
                case HideOptions.AllEntryControls:
                    buttonsMenu.Visible = false;
                    break;
                case HideOptions.EntryPrintControls:
                    bPrint.Visible = false;
                    break;
                case HideOptions.None:
                    panelSearch.Visible = true;
                    buttonsMenu.Visible = true;
                    bPrint.Visible = true;
                    break;
            }
        }

        public string TransactionName
        {
            set { formLabel.Text = value; }
        }

        #region Methods for setting buttons click events
        public void SetButtonNewClick(EventHandler click)
        {
            bNew.Visible = true;
            bNew.Click += click;
        }

        public void SetButtonEditClick(EventHandler click)
        {
            bEdit.Visible = true;
            bEdit.Click += click;
        }

        public void SetButtonDeleteClick(EventHandler click)
        {
            bDelete.Visible = true;
            bDelete.Click += click;
        }

        public void SetButtonPrintClick(EventHandler click)
        {
            bPrint.Visible = true;
            bPrint.Click += click;
        }

        public void SetButtonExportClick(EventHandler click)
        {
            bExport.Visible = true;
            bExport.Click += click;
        }

        #endregion
        public Control_StandardControls()
        {
            InitializeComponent();
        }

        private void ListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetMenuButtonsStatus(listView.SelectedItems.Count < 1 ? false : true);
        }

        private void SetMenuButtonsStatus(bool status)
        {
            bEdit.Enabled = status;
            bDelete.Enabled = status;
            bPrint_PDF.Enabled = status;
            bPrint_printer.Enabled = status;
            bPrint_screen.Enabled = status;
            bExport.Enabled = status;
        }

        private void searchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                loadListView();
            }
        }

        public void loadListView(bool firstLoad = false, List<SQLParameter> prms = null)
        {
            if(listView != null)
            {
                if (view != null)
                {
                    try
                    {
                        if(prms == null)
                        {
                            prms = new List<SQLParameter>();
                        }
                        db = DatabaseManager.GetInstance();
                        if (searchBox.TextLength > 0)
                        {
                            prms.Add(new SQLParameter("search", searchBox.Text, "LIKE", view.SearchColumn));
                        }
                        if (panelSearchDate.Visible == true)
                        {
                            if (view.DateColumn != null)
                            {
                                prms.Add(new SQLParameter("start", dateBeg.Value.ToString("yyyy-MM-dd"), ">=", view.DateColumn));
                                prms.Add(new SQLParameter("end", dateEnd.Value.ToString("yyyy-MM-dd"), "<=", view.DateColumn));
                            }
                        }
                    }
                    catch(Exception err)
                    {
                        Console.WriteLine("Error message: " + err.Message + "\r\n\r\n" + err.StackTrace);
                        MessageBox.Show(err.Message);
                    }
                    
                    IDataReader reader = db.Select("SELECT * FROM " + view.Name, prms);
                    ListViewManager.populateListView(listView, reader, firstLoad);
                }
            }
        }

        private void panelSearch_VisibleChanged(object sender, EventArgs e)
        {
            if(panelSearch.Visible == false)
            {
                label4.Visible = false;
                searchBox.Visible = false;
                panelSearchDate.Visible = false;
            }
            else
            {
                label4.Visible = false;
                searchBox.Visible = true;
                panelSearchDate.Visible = true;
            }
        }

        private void panelSearchDate_VisibleChanged(object sender, EventArgs e)
        {
            if (panelSearchDate.Visible == false)
            {
                dateBeg.Visible = false;
                dateEnd.Visible = false;
                panelSearch.Height = panel1.Height;
            }
            else
            {
                panelSearch.Height = panelSearchDate.Height + panel1.Height;
                dateBeg.Visible = true;
                dateEnd.Visible = true;
            }
        }

        private void date_ValueChanged(object sender, EventArgs e)
        {
            loadListView();
        }
    }
}
