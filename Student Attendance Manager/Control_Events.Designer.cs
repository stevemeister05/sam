﻿namespace Student_Attendance_Manager
{
    partial class Control_Events
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Control_Events));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.sem = new System.Windows.Forms.ComboBox();
            this.sy = new System.Windows.Forms.ComboBox();
            this.bCancelEvent = new System.Windows.Forms.Button();
            this.bCloseEvent = new System.Windows.Forms.Button();
            this.bOpenEvent = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.stts = new System.Windows.Forms.Label();
            this.punctualrate = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.earlyouters = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.latecomers = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.attendancerate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.absentees = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.attendees = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.enrolledStudents = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.exempted = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listView2 = new System.Windows.Forms.ListView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.bRemoveExemptions = new System.Windows.Forms.Button();
            this.bAddExemption = new System.Windows.Forms.Button();
            this.listView3 = new System.Windows.Forms.ListView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addNoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.control_StandardControls1 = new Student_Attendance_Manager.Control_StandardControls();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(712, 589);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.listView1);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(10, 10);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10);
            this.panel2.Size = new System.Drawing.Size(692, 281);
            this.panel2.TabIndex = 0;
            // 
            // listView1
            // 
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(10, 51);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(672, 220);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.sem);
            this.panel5.Controls.Add(this.sy);
            this.panel5.Controls.Add(this.bCancelEvent);
            this.panel5.Controls.Add(this.bCloseEvent);
            this.panel5.Controls.Add(this.bOpenEvent);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(10, 10);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(672, 41);
            this.panel5.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SteelBlue;
            this.label2.Location = new System.Drawing.Point(-3, -4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 15);
            this.label2.TabIndex = 9;
            this.label2.Text = "School year";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.SteelBlue;
            this.label11.Location = new System.Drawing.Point(160, -4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 15);
            this.label11.TabIndex = 10;
            this.label11.Text = "Semester";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // sem
            // 
            this.sem.BackColor = System.Drawing.Color.White;
            this.sem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sem.ForeColor = System.Drawing.Color.SteelBlue;
            this.sem.FormattingEnabled = true;
            this.sem.Location = new System.Drawing.Point(164, 12);
            this.sem.Name = "sem";
            this.sem.Size = new System.Drawing.Size(67, 23);
            this.sem.TabIndex = 5;
            this.sem.SelectedIndexChanged += new System.EventHandler(this.sem_SelectedIndexChanged);
            // 
            // sy
            // 
            this.sy.BackColor = System.Drawing.Color.White;
            this.sy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sy.ForeColor = System.Drawing.Color.SteelBlue;
            this.sy.FormattingEnabled = true;
            this.sy.Location = new System.Drawing.Point(0, 12);
            this.sy.Name = "sy";
            this.sy.Size = new System.Drawing.Size(158, 23);
            this.sy.TabIndex = 4;
            this.sy.SelectedIndexChanged += new System.EventHandler(this.sy_SelectedIndexChanged);
            // 
            // bCancelEvent
            // 
            this.bCancelEvent.BackColor = System.Drawing.Color.Brown;
            this.bCancelEvent.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bCancelEvent.ForeColor = System.Drawing.Color.White;
            this.bCancelEvent.Location = new System.Drawing.Point(567, 0);
            this.bCancelEvent.Name = "bCancelEvent";
            this.bCancelEvent.Size = new System.Drawing.Size(105, 32);
            this.bCancelEvent.TabIndex = 2;
            this.bCancelEvent.Text = "Cancel Event";
            this.bCancelEvent.UseVisualStyleBackColor = false;
            this.bCancelEvent.Click += new System.EventHandler(this.bCancelEvent_Click);
            // 
            // bCloseEvent
            // 
            this.bCloseEvent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.bCloseEvent.Enabled = false;
            this.bCloseEvent.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bCloseEvent.ForeColor = System.Drawing.Color.White;
            this.bCloseEvent.Location = new System.Drawing.Point(426, 0);
            this.bCloseEvent.Name = "bCloseEvent";
            this.bCloseEvent.Size = new System.Drawing.Size(115, 32);
            this.bCloseEvent.TabIndex = 1;
            this.bCloseEvent.Text = "Close Registration";
            this.bCloseEvent.UseVisualStyleBackColor = false;
            this.bCloseEvent.Click += new System.EventHandler(this.bCloseEvent_Click);
            // 
            // bOpenEvent
            // 
            this.bOpenEvent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.bOpenEvent.Enabled = false;
            this.bOpenEvent.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bOpenEvent.ForeColor = System.Drawing.Color.White;
            this.bOpenEvent.Location = new System.Drawing.Point(305, 0);
            this.bOpenEvent.Name = "bOpenEvent";
            this.bOpenEvent.Size = new System.Drawing.Size(115, 32);
            this.bOpenEvent.TabIndex = 0;
            this.bOpenEvent.Text = "Open Registration";
            this.bOpenEvent.UseVisualStyleBackColor = false;
            this.bOpenEvent.Click += new System.EventHandler(this.bOpenEvent_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(10, 291);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.panel3.Size = new System.Drawing.Size(692, 288);
            this.panel3.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 10);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(692, 278);
            this.panel4.TabIndex = 2;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.Controls.Add(this.stts);
            this.panel7.Controls.Add(this.punctualrate);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.earlyouters);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.latecomers);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.attendancerate);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.absentees);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.attendees);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.enrolledStudents);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.exempted);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(471, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(221, 278);
            this.panel7.TabIndex = 1;
            // 
            // stts
            // 
            this.stts.AutoSize = true;
            this.stts.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stts.ForeColor = System.Drawing.Color.SteelBlue;
            this.stts.Location = new System.Drawing.Point(97, 10);
            this.stts.Name = "stts";
            this.stts.Size = new System.Drawing.Size(48, 18);
            this.stts.TabIndex = 31;
            this.stts.Text = "Open";
            // 
            // punctualrate
            // 
            this.punctualrate.BackColor = System.Drawing.Color.White;
            this.punctualrate.Cursor = System.Windows.Forms.Cursors.Default;
            this.punctualrate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.punctualrate.Location = new System.Drawing.Point(133, 207);
            this.punctualrate.Name = "punctualrate";
            this.punctualrate.ReadOnly = true;
            this.punctualrate.Size = new System.Drawing.Size(81, 21);
            this.punctualrate.TabIndex = 30;
            this.punctualrate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.punctualrate.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.SteelBlue;
            this.label10.Location = new System.Drawing.Point(18, 210);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 15);
            this.label10.TabIndex = 29;
            this.label10.Text = "Punctuality Rate:";
            // 
            // earlyouters
            // 
            this.earlyouters.BackColor = System.Drawing.Color.White;
            this.earlyouters.Cursor = System.Windows.Forms.Cursors.Default;
            this.earlyouters.Location = new System.Drawing.Point(175, 171);
            this.earlyouters.Name = "earlyouters";
            this.earlyouters.ReadOnly = true;
            this.earlyouters.Size = new System.Drawing.Size(39, 20);
            this.earlyouters.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.SteelBlue;
            this.label8.Location = new System.Drawing.Point(91, 172);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 15);
            this.label8.TabIndex = 25;
            this.label8.Text = "Early Outers:";
            // 
            // latecomers
            // 
            this.latecomers.BackColor = System.Drawing.Color.White;
            this.latecomers.Cursor = System.Windows.Forms.Cursors.Default;
            this.latecomers.Location = new System.Drawing.Point(175, 145);
            this.latecomers.Name = "latecomers";
            this.latecomers.ReadOnly = true;
            this.latecomers.Size = new System.Drawing.Size(39, 20);
            this.latecomers.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.SteelBlue;
            this.label7.Location = new System.Drawing.Point(87, 146);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 15);
            this.label7.TabIndex = 23;
            this.label7.Text = "Late Comers:";
            // 
            // attendancerate
            // 
            this.attendancerate.BackColor = System.Drawing.Color.White;
            this.attendancerate.Cursor = System.Windows.Forms.Cursors.Default;
            this.attendancerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendancerate.Location = new System.Drawing.Point(133, 234);
            this.attendancerate.Name = "attendancerate";
            this.attendancerate.ReadOnly = true;
            this.attendancerate.Size = new System.Drawing.Size(81, 21);
            this.attendancerate.TabIndex = 22;
            this.attendancerate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.attendancerate.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.SteelBlue;
            this.label6.Location = new System.Drawing.Point(18, 237);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 15);
            this.label6.TabIndex = 21;
            this.label6.Text = "Attendance Rate:";
            // 
            // absentees
            // 
            this.absentees.BackColor = System.Drawing.Color.White;
            this.absentees.Cursor = System.Windows.Forms.Cursors.Default;
            this.absentees.Location = new System.Drawing.Point(175, 119);
            this.absentees.Name = "absentees";
            this.absentees.ReadOnly = true;
            this.absentees.Size = new System.Drawing.Size(39, 20);
            this.absentees.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.SteelBlue;
            this.label4.Location = new System.Drawing.Point(100, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 15);
            this.label4.TabIndex = 19;
            this.label4.Text = "Absentees:";
            // 
            // attendees
            // 
            this.attendees.BackColor = System.Drawing.Color.White;
            this.attendees.Cursor = System.Windows.Forms.Cursors.Default;
            this.attendees.Location = new System.Drawing.Point(175, 93);
            this.attendees.Name = "attendees";
            this.attendees.ReadOnly = true;
            this.attendees.Size = new System.Drawing.Size(39, 20);
            this.attendees.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.SteelBlue;
            this.label3.Location = new System.Drawing.Point(103, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 15);
            this.label3.TabIndex = 17;
            this.label3.Text = "Attendees:";
            // 
            // enrolledStudents
            // 
            this.enrolledStudents.BackColor = System.Drawing.Color.White;
            this.enrolledStudents.Cursor = System.Windows.Forms.Cursors.Default;
            this.enrolledStudents.Location = new System.Drawing.Point(175, 41);
            this.enrolledStudents.Name = "enrolledStudents";
            this.enrolledStudents.ReadOnly = true;
            this.enrolledStudents.Size = new System.Drawing.Size(39, 20);
            this.enrolledStudents.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.SteelBlue;
            this.label5.Location = new System.Drawing.Point(56, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 15);
            this.label5.TabIndex = 7;
            this.label5.Text = "Expected Students:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SteelBlue;
            this.label1.Location = new System.Drawing.Point(6, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "Event Status:";
            // 
            // exempted
            // 
            this.exempted.BackColor = System.Drawing.Color.White;
            this.exempted.Cursor = System.Windows.Forms.Cursors.Default;
            this.exempted.Location = new System.Drawing.Point(175, 67);
            this.exempted.Name = "exempted";
            this.exempted.ReadOnly = true;
            this.exempted.Size = new System.Drawing.Size(39, 20);
            this.exempted.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.SteelBlue;
            this.label9.Location = new System.Drawing.Point(100, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 15);
            this.label9.TabIndex = 27;
            this.label9.Text = "Exempted:";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel8);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.panel6.Size = new System.Drawing.Size(471, 278);
            this.panel6.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.Controls.Add(this.tabControl1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(10);
            this.panel8.Size = new System.Drawing.Size(461, 278);
            this.panel8.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.ItemSize = new System.Drawing.Size(120, 18);
            this.tabControl1.Location = new System.Drawing.Point(10, 10);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(441, 258);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.listView2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(433, 232);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Attendance";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // listView2
            // 
            this.listView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView2.FullRowSelect = true;
            this.listView2.GridLines = true;
            this.listView2.HideSelection = false;
            this.listView2.Location = new System.Drawing.Point(3, 3);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(427, 226);
            this.listView2.TabIndex = 4;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.bRemoveExemptions);
            this.tabPage2.Controls.Add(this.bAddExemption);
            this.tabPage2.Controls.Add(this.listView3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(433, 232);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Exempted Students";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // bRemoveExemptions
            // 
            this.bRemoveExemptions.BackColor = System.Drawing.Color.Firebrick;
            this.bRemoveExemptions.Enabled = false;
            this.bRemoveExemptions.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bRemoveExemptions.ForeColor = System.Drawing.Color.White;
            this.bRemoveExemptions.Location = new System.Drawing.Point(107, 199);
            this.bRemoveExemptions.Name = "bRemoveExemptions";
            this.bRemoveExemptions.Size = new System.Drawing.Size(95, 28);
            this.bRemoveExemptions.TabIndex = 6;
            this.bRemoveExemptions.Text = "Remove";
            this.bRemoveExemptions.UseVisualStyleBackColor = false;
            this.bRemoveExemptions.Click += new System.EventHandler(this.bRemoveExemptions_Click);
            // 
            // bAddExemption
            // 
            this.bAddExemption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.bAddExemption.Enabled = false;
            this.bAddExemption.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bAddExemption.ForeColor = System.Drawing.Color.White;
            this.bAddExemption.Location = new System.Drawing.Point(6, 199);
            this.bAddExemption.Name = "bAddExemption";
            this.bAddExemption.Size = new System.Drawing.Size(95, 28);
            this.bAddExemption.TabIndex = 3;
            this.bAddExemption.Text = "Add";
            this.bAddExemption.UseVisualStyleBackColor = false;
            this.bAddExemption.Click += new System.EventHandler(this.button1_Click);
            // 
            // listView3
            // 
            this.listView3.Dock = System.Windows.Forms.DockStyle.Top;
            this.listView3.FullRowSelect = true;
            this.listView3.GridLines = true;
            this.listView3.HideSelection = false;
            this.listView3.Location = new System.Drawing.Point(3, 3);
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(427, 190);
            this.listView3.TabIndex = 5;
            this.listView3.UseCompatibleStateImageBehavior = false;
            this.listView3.View = System.Windows.Forms.View.Details;
            this.listView3.SelectedIndexChanged += new System.EventHandler(this.listView3_SelectedIndexChanged);
            this.listView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNoteToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(145, 26);
            // 
            // addNoteToolStripMenuItem
            // 
            this.addNoteToolStripMenuItem.Name = "addNoteToolStripMenuItem";
            this.addNoteToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.addNoteToolStripMenuItem.Text = "Add Remarks";
            this.addNoteToolStripMenuItem.Click += new System.EventHandler(this.addNoteToolStripMenuItem_Click);
            // 
            // control_StandardControls1
            // 
            this.control_StandardControls1.BackColor = System.Drawing.Color.White;
            this.control_StandardControls1.DeleteButtonVisible = true;
            this.control_StandardControls1.Dock = System.Windows.Forms.DockStyle.Right;
            this.control_StandardControls1.EditButtonVisible = true;
            this.control_StandardControls1.EndDate = new System.DateTime(2019, 11, 4, 10, 49, 37, 545);
            this.control_StandardControls1.ExportButtonVisible = true;
            this.control_StandardControls1.Location = new System.Drawing.Point(712, 0);
            this.control_StandardControls1.Name = "control_StandardControls1";
            this.control_StandardControls1.NewButtonVisible = true;
            this.control_StandardControls1.PrintButtonVisible = true;
            this.control_StandardControls1.Size = new System.Drawing.Size(188, 589);
            this.control_StandardControls1.StartDate = new System.DateTime(2019, 11, 4, 10, 49, 37, 548);
            this.control_StandardControls1.TabIndex = 2;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // Control_Events
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(239)))), ((int)(((byte)(243)))));
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.control_StandardControls1);
            this.Name = "Control_Events";
            this.Size = new System.Drawing.Size(900, 589);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button bOpenEvent;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button bCloseEvent;
        private System.Windows.Forms.Button bCancelEvent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox enrolledStudents;
        private System.Windows.Forms.TextBox attendancerate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox absentees;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox attendees;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox exempted;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox earlyouters;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox latecomers;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox punctualrate;
        private System.Windows.Forms.Label label10;
        private Control_StandardControls control_StandardControls1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView listView3;
        private System.Windows.Forms.Button bAddExemption;
        private System.Windows.Forms.Button bRemoveExemptions;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addNoteToolStripMenuItem;
        private System.Windows.Forms.ComboBox sy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox sem;
        private System.Windows.Forms.Label stts;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
    }
}
